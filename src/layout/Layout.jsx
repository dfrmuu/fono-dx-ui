import {useState,useEffect} from 'react'
import { Outlet, useNavigate} from 'react-router-dom'
import {DashboardComplete} from '../components/dashboard/DashBoard'
import { DateTime } from 'luxon';
import { BeatLoader } from 'react-spinners';
import { ToastContainer } from 'react-toastify';
import Logo from '../assets/Logo_FonoDX.png'
import Cookies from 'universal-cookie';
import axios from 'axios'
import Navbar from '.././components/layout/Navbar'
import CryptoJS from "crypto-js";
import 'react-responsive-modal/styles.css';
import SpinnerHeart from '../components/SpinnerHeart';
import MiUsuario from '../components/MiUsuario';
import CambioClave from '../components/CambioClave';

//El layout es la estructura que se va a compartir a lo largo del aplicativo, esto para que el aplicativo se vea consistente en el diseño.
const Layout = () => {

    //Hook para redirección
    const navigate = useNavigate();
    //Acceso a cookies del navegador
    const cookies = new Cookies();

    //Variable datos usuario
    const [user,setUser] = useState("");
    const [cargando,setCargando] = useState(false)
    const [permisos,setPermisos] = useState([])
    const [rol,setRol] = useState("");

    //Modales para edición de usuario y cambio de contraseña
    const [miUsuario,setMiUsuario] = useState(false)
    const [cambio,setCambio] = useState(false)

    //API
    const VITE_API_LINK = import.meta.env.VITE_API_LINK
    
    //Este use effect hace que se ejecute el traer el usuario apenas se carge esta ventana.
    useEffect(() => {
        traerUsuario() 
    }, [])

    const traerUsuario = async () => {

        //Recupera token acceso
        const tokenAcceso = cookies.get("token")

        //Pone el spinner de carga
        setCargando(true)

        //¿El token existe?
        if(cookies.get('token')){

            //Hace petición a ruta para la revisión de datos
            axios.get(VITE_API_LINK + "auth/revision",{
                headers: {
                    Authorization: `Bearer ${tokenAcceso}`,
                    ClientAuth : import.meta.env.VITE_APIKEY
                },
                timeout : 10000
            }).then((response) => {

                var datosUsuario

                if(response.data.Rol == 1){
                    datosUsuario = {
                        Nombre_Completo : response.data.Nombre_Completo,
                        Nombre_Usuario : response.data.Nombre_Usuario,
                        Familiar : response.data.Familiar,
                        FamiliarTelefono : response.data.FamiliarTelefono
                    }
                }else{

                    //Construye objeto con los datos del usuario
                    datosUsuario = {
                        Nombre_Completo : response.data.Nombre_Completo,
                        Nombre_Usuario : response.data.Nombre_Usuario,
                        Cargo : response.data.Cargo,
                        Area : response.data.Area
                    }
                }

                //Cambia el estado de las variables con los datos del api.
                setUser(datosUsuario)
                setRol(response.data.Rol)
                setPermisos(response.data.Permisos)            
                      
            }).catch((error) => {
                console.log(error)
            }).finally(() => {
                setCargando(false)
            })
        } else {
            navigate("/login")
        }
    }    

    //Handle para el control del modal de mi usuario (abierto-cerrado)
    const handleUsuario = () => {
        setMiUsuario(!miUsuario)
    }

     //Handle para el control del modal de cambio de clave (abierto-cerrado)
    const handleCambio = () => {
        setCambio(!cambio)
    }

    return (
        <>
            {miUsuario == true ? (
                <MiUsuario  abierto={miUsuario} handleCerrar={handleUsuario}/>
            ) : (
                null
            )}

            {cambio == true ? (
                <CambioClave abierto={cambio} handleCerrar={handleCambio}/>
            ) : (
                null
            )}

            {cargando == true ? (
                <SpinnerHeart height={500} width={500}/>
            ) : (
                <>
                <div className="offcanvas offcanvas-start light-grey-handbook overflow-hidden" tabIndex="-1" id="offcanvasFono" aria-labelledby="offcanvasFono">

                    <div className="offcanvas-header">
                        <div className="offcanvas-title text-center fw-bold text-center w-100 mt-2" id="offcanvasFono">
                            <div className='rounded-pill shadow  w-100'>
                                <img src={Logo} className='img-fluid' width="270px" height="90px"/>
                            </div>
                        </div>
                    </div>

                    <div className="offcanvas-body">
                        <DashboardComplete rolUsuario={rol}/>
                        <div className='rounded-pill shadow p-4 w-100 text-center mt-3 sidebar-btn-handbook' data-bs-dismiss="offcanvas" aria-label="Close">
                            <a className='fs-6 a-sidebar'>Ocultar</a>
                        </div>
                    </div>
                </div>

                <ToastContainer position='top-right' autoClose={3000}/>

                <Navbar user={user} handleUsuario={handleUsuario} handleCambio={handleCambio}/>

                <div className='px-3 mb-4 shadow-lg'>
                    <div className='mt-5'>
                        <Outlet context={[user,rol,permisos,setPermisos]}/>
                    </div>
                </div>
                </>
            )}

        </>
    )
}

export default Layout