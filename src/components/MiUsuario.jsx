import { useState, useEffect} from 'react'
import { toast } from 'react-toastify';
import { FaEdit, FaSearch } from 'react-icons/fa'
import { Formik,Form,Field, ErrorMessage } from 'formik';
import Modal from 'react-responsive-modal'
import Cookies from 'universal-cookie';
import CryptoJS from 'crypto-js'
import * as Yup from 'yup'
import axios from 'axios'
import SpinnerHeart from './SpinnerHeart';

//Componente para visualizar y editar datos del usuario propio
const MiUsuario = ({abierto,handleCerrar}) => {

    //Variable spinner de carga
    const [cargando,setCargando] = useState(false)

    //Variable para almacenar el usuario consultado
    const [usuario,setUsuario] = useState([])

    //Variables datos del usuario
    const [nombres,setNombres] = useState("")
    const [apellidos,setApellidos] = useState("") 
    const [correo,setCorreo] = useState("")
    const [numero,setNumero] = useState("")
    const [ciudad,setCiudad] = useState("")
    const [direccion,setDireccion] = useState("")
    const [ci,setCI] = useState("")
    const [ciudades,setCiudades] = useState([])


    //API 
    const cookies = new Cookies()
    const VITE_API_LINK = import.meta.env.VITE_API_LINK

    //Consulta las ciudades y el usuario
    useEffect(() => {
        consultaCiudades()
        miUsuario()
    }, [])

    //Según la consulta del usuario, llena las variables con los datos del usuario
    useEffect(() => {
        setNombres(usuario.name)
        setApellidos(usuario.last_name)
        setCorreo(usuario.email)
        setNumero(usuario.phone)
        setCiudad(usuario.city)
        setDireccion(usuario.address)
        setCI(usuario.ci)
    }, [usuario])
    

    //Esctructura valida para el formulario de usuario
    const miUsuarioScheme = Yup.object().shape({
        names : Yup.string()
                 .required("El nombre es requerido"),

        last_name : Yup.string()
                       .required("El apellido es requerido"),

        email : Yup.string()
                   .email("No es un correo valido")
                   .required("El correo es requerido"),

        city : Yup.string()
                    .required("La ciudad es requerida"),
        
        address : Yup.string()
                    .required("La dirección es requerida"),
        
        ci : Yup.string()
                    .required("El documento es requerido"),
                   
    })

    //Consulta las ciudades desde un API externa
    const consultaCiudades = async() => {
        try {
            
            await axios.get("https://www.datos.gov.co/resource/xdk5-pm3f.json")
                       .then((response) => {
                            const ciudades = response.data.map((ciudad) => ciudad.municipio)
                            setCiudades(ciudades)
                       }).catch((error) => {
                            console.log(error)
                       })

        } catch (error) {
            console.log(error)
        }
    }

    //Trae los datos del usuario propio
    const miUsuario = async() => {
        try {

            setCargando(true)

            const tokenAcceso = cookies.get("token")
    
            await axios.get(VITE_API_LINK + `usuarios/mi-usuario/info`, {
                headers: {
                    Authorization: `Bearer ${tokenAcceso}`,
                    ClientAuth : import.meta.env.VITE_APIKEY,
                    Resource: 'mi-usuario',
                    Action : 'listar'
                },
                timeout : 10000
            })
            .then((response) =>{
                setUsuario(response.data)
            }).catch((error) => {
                toast.error(error.response.data.Error)
            }).finally(() => {
                setInterval(() => {
                    setCargando(false)
                }, 500);
            })
        } catch (error) {
          console.log(error)
        }
    }

    //Envia los datos para realizar la edición del usuario
    const editarMiUsuario = async() => {
        try {

            const tokenAcceso = cookies.get("token")
    
            await axios.put(VITE_API_LINK + `usuarios/mi-usuario/editar`,{
                ci : ci,
                names : nombres,
                last_name : apellidos,
                email : correo,
                city : ciudad,
                address : direccion,
                phone : numero
            },{
                headers: {
                    Authorization: `Bearer ${tokenAcceso}`,
                    ClientAuth : import.meta.env.VITE_APIKEY,
                    Resource: 'mi-usuario',
                    Action : 'editar'
                },
                timeout : 10000
            })
            .then((response) =>{
                toast.info(response.data.Message)
                miUsuario()
            }).catch((error) => {
                toast.error(error.response.data.Error)
            })
        } catch (error) {
          console.log(error)
        }
    }

    //Handle para la digitación de datos
    const handleNombres = (e) => {
        setNombres(e.target.value)
    }

    const handleApellidos = (e) => {
        setApellidos(e.target.value)
    }

    const handleCorreo = (e) => {
        setCorreo(e.target.value)
    }

    const handleTelefono = (e) => {
        setNumero(e.target.value)
    }

    const handleCiudad = (e) => {
        setCiudad(e.target.value)
    }

    const handleDireccion = (e) => {
        setDireccion(e.target.value)
    }

    const handleDocumento = (e) => {
        setCI(e.target.value)
    }

    return (
        <Modal open={abierto}
            closeOnOverlayClick={false}
            classNames={{
                modal : `rounded w-75 light-grey-handbook`
            }} 
            animationDuration={400}
            center
            onClose={handleCerrar}>


            <div className="position-relative">

                <h3 className='text-center fw-bold mt-4'>
                    MI USUARIO
                </h3>

                <hr/>

                <div className='light-gray-bg rounded p-3'>
                    <div className='d-flex justify-content-center'>
                        <img src='https://img.freepik.com/free-icon/user_318-159711.jpg' width={100} height={100} className='img-fluid shadow rounded-circle'/>
                    </div>
                    <hr/>

                    {cargando == true ? (
                        <>
                           <br/>
                           <SpinnerHeart height={300} width={300}/>
                        </>
                    ) : (
                        
                        <Formik  onSubmit={async() => editarMiUsuario()}
                            validationSchema={miUsuarioScheme}
                            enableReinitialize = {true}
                            initialValues={{
                                    names : nombres,
                                    last_name : apellidos,
                                    email : correo,
                                    phone  : numero,
                                    city : ciudad,
                                    address : direccion,
                                    ci : ci
                            }}>
                                
                            <Form>
                                <div className='mt-3'>
                                    <div className='row'>
                                        <div className='col-md-6'>
                                            <label className='form-label fw-bold mb-2'> Nombres:</label>
                                            <Field type='text' className='form-control p-3 shadow-sm' placeholder='Escriba los nombres' name='names' id='names' onKeyUp={handleNombres}/>
                                            <ErrorMessage name='names' component='div' className='bg-danger p-2 text-white rounded text-center fw-bold'/>
                                        </div>
                                        <div className='col-md-6'>
                                            <label className='form-label fw-bold  mb-2'> Apellidos:</label>
                                            <Field type='text' className='form-control p-3 shadow-sm' placeholder='Escriba los apellidos' name='last_name' id='last_name' onKeyUp={handleApellidos}/>
                                            <ErrorMessage name='last_name' component='div' className='bg-danger p-2 text-white rounded text-center fw-bold'/>
                                        </div>
                                    </div>
                                    <br/>
                                    <div className='row'>
                                        <div className='col-md-6'>
                                            <label className='form-label fw-bold mb-2'> Correo:</label>
                                            <Field type='email' className='form-control p-3 shadow-sm' placeholder='Escriba el correo' name='email' id='email' onKeyUp={handleCorreo}/>
                                            <ErrorMessage name='email' component='div' className='bg-danger p-2 text-white rounded text-center fw-bold'/>
                                        </div>
                                        <div className='col-md-6'>
                                            <label className='form-label fw-bold  mb-2'> Número telefonico:</label>
                                            <Field type='text' className='form-control p-3 shadow-sm' placeholder='Escriba el número' name='phone' id='phone' onKeyUp={handleTelefono}/>
                                            <ErrorMessage name='phone' component='div' className='bg-danger p-2 text-white rounded text-center fw-bold'/>
                                        </div>
                                    </div>
                                    <br/>
                                    <div className='row'>
                                        <div className='col-md-6'>
                                            <label className='form-label fw-bold mb-2'> Ciudad:</label>
                                            <Field as="select" className='form-select p-3' value={ciudad} name='city' id='city' onChange={handleCiudad}>
                                                <option value="">Seleccione la ciudad..</option>
                                                {ciudades.map((ciudad) => (
                                                    <option value={ciudad}>
                                                        {ciudad}
                                                    </option>
                                                ))}
                                            </Field>
                                            <ErrorMessage name='city' component='div' className='bg-danger p-2 text-white rounded text-center fw-bold'/>
                                        </div>
                                        <div className='col-md-6'>
                                            <label className='form-label fw-bold  mb-2'> Dirección:</label>
                                            <Field type='text' className='form-control p-3 shadow-sm' placeholder='Escriba la dirección de residencia' name='address' id='address' onKeyUp={handleDireccion}/>
                                            <ErrorMessage name='address' component='div' className='bg-danger p-2 text-white rounded text-center fw-bold'/>
                                        </div>
                                    </div>
                                    <br/>
                                    <div className='row'>
                                        <div className='col-md-12'>
                                            <label className='form-label fw-bold mb-2'> Número documento:</label>
                                            <Field type='text' className='form-control p-3 shadow-sm' placeholder='Escriba el documento' name='ci' id='ci' onKeyUp={handleDocumento}/>
                                            <ErrorMessage name='ci' component='div' className='bg-danger p-2 text-white rounded text-center fw-bold'/>
                                        </div>
                                    </div>
                                    <br/>
                                    <div className='row'>
                                        <div className='col-md-12'>
                                            <button className='dark-red-handbook text-white p-3 mt-1 w-100 btn' type='submit'>
                                                <FaEdit/>
                                                <span className='px-2'>Actualizar mis datos.</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </Form>

                        </Formik>
                    )}
                </div>

            </div>

        </Modal>
    )
}

export default MiUsuario