import { useState } from 'react'
import { FaEye } from 'react-icons/fa'
import Modal from 'react-responsive-modal'
import Cookies from 'universal-cookie'
import SpinnerHeart from './SpinnerHeart'
import axios from 'axios'
import { toast } from 'react-toastify'

//Componente para cambio de clave
const CambioClave = ({abierto,handleCerrar}) => {

    //Variable spinner de carga
    const [cargando,setCargando] = useState(false)

    //Variables para datos de la clave
    const [clave,setClave] = useState("")
    const [nuevaClave,setNuevaClave] = useState("")

    //Variable para revisión de clave
    const [claveValida,setClaveValida] = useState(false)

    //Recupera cookies del navegador
    const cookies = new Cookies()

    //Datos del API
    const VITE_API_LINK = import.meta.env.VITE_API_LINK

    //Handle para la digitación de datos
    const handleClave = (e) => {
        setClave(e.target.value)
    }

    const handleNuevaClave = (e) => {
        setNuevaClave(e.target.value)
    }


    //Realiza la revisión de la clave, para ver si es correcta (le pertenece al usuario)
    const revisionClave = async() => {

        if(clave.length < 6 || clave == ""){
            toast.error("La clave debe tener al menos 6 digitos")
            return
        }

        try {
            setCargando(true)

            const tokenAcceso = cookies.get("token")
    
            await axios.post(VITE_API_LINK + `usuarios/revisarclave`,{
                clave : clave
            }, {
                headers: {
                    Authorization: `Bearer ${tokenAcceso}`,
                    ClientAuth : import.meta.env.VITE_APIKEY,
                    Resource: 'mi-usuario',
                    Action : 'listar'
                },
                timeout : 10000
            })
            .then((response) =>{
                if(response.data.valido == true){
                    setClaveValida(true)
                }
            }).catch((error) => {
                toast.error(error.response.data.Error)
            }).finally(() => {
                setInterval(() => {
                    setCargando(false)
                }, 500);
            })
        } catch (error) {
            console.log(error)
        }
    }


    //Envia los datos para el cambio de clave del usuario
    const cambiarClave = async() => {

        if(nuevaClave.length < 6 || nuevaClave == ""){
            toast.error("La clave debe tener al menos 6 digitos")
            return
        }

        try {
            setCargando(true)

            const tokenAcceso = cookies.get("token")
    
            await axios.post(VITE_API_LINK + `usuarios/cambiarclave`,{
                clave : nuevaClave
            }, {
                headers: {
                    Authorization: `Bearer ${tokenAcceso}`,
                    ClientAuth : import.meta.env.VITE_APIKEY,
                    Resource: 'mi-usuario',
                    Action : 'listar'
                },
                timeout : 10000
            })
            .then((response) =>{
                toast.info(response.data.Message)
            }).catch((error) => {
                toast.error(error.response.data.Error)
            }).finally(() => {
                setInterval(() => {
                    setCargando(false)
                    setNuevaClave("")
                    setClaveValida(false)
                    setClave("")
                }, 500);
            })
        } catch (error) {
            console.log(error)
        }
    }


    return (
        <Modal open={abierto}
            closeOnOverlayClick={false}
            classNames={{
                modal : `rounded w-75 light-grey-handbook`
            }} 
            animationDuration={400}
            center
            onClose={handleCerrar}>


            <div className="position-relative">

                <h3 className='text-center fw-bold mt-4'>
                    CAMBIO DE CLAVE 
                </h3>

                <hr/>

                <div className='dark-grey-handbook text-white p-2 mb-3 rounded shadow'>
                    <p className='text-center mt-2'> Digite su clave actual, a partir de esta podremos confirmar su identidad y darle acceso al cambio de contraseña </p>
                </div>

                <br/>

                {cargando == true ? (
                    <>
                        <br/>
                        <SpinnerHeart height={300} width={300}/>          
                    </>

                ) : (
                    <div className='mt-3'>

                        {claveValida == false ? (
                            <>
                               <div className='row'>
                                    <div className='col-md-12'>
                                        <label className='form-label fw-bold mb-2'> Por favor, escriba su clave:</label>
                                        <input type='text' className='form-control p-3 shadow-sm' placeholder='Escriba la clave' name='claveantigua' id='claveantigua' onKeyUp={handleClave}/>
                                    </div>
                                </div>

                                <br/>

                                <div className='row'>
                                    <div className='col-md-12'>
                                        <button className='dark-red-handbook text-white p-3 mt-1 w-100 btn' type='button' onClick={() => revisionClave()}>
                                            <FaEye/>
                                            <span className='px-2'>Revisar contraseña.</span>
                                        </button>
                                    </div>
                                </div>
                            </>
                        ) : (
                            <>
                                <div className='row'>
                                    <div className='col-md-12'>
                                        <label className='form-label fw-bold mb-2'> Por favor, escriba su NUEVA clave:</label>
                                        <input type='text' className='form-control p-3 shadow-sm' placeholder='Escriba la clave' name='clavenueva' id='clavenueva' onKeyUp={handleNuevaClave}/>
                                    </div>
                                </div>

                                <br/>

                                <div className='row'>
                                    <div className='col-md-12'>
                                        <button className='dark-red-handbook text-white p-3 mt-1 w-100 btn' type='button'  onClick={() => cambiarClave()}>
                                            <FaEye/>
                                            <span className='px-2'>Cambiar contraseña.</span>
                                        </button>
                                    </div>
                                </div>
                            </>
                        )}

                    </div>

                )}

            </div>
        </Modal>
    )
}

export default CambioClave