import axios from 'axios';
import React from 'react'
import { FaBars } from 'react-icons/fa';
import { useNavigate } from 'react-router-dom';
import { toast,ToastContainer } from 'react-toastify';
import Cookies from 'universal-cookie';


//Componente barra de navegación del aplicativo
const Navbar = ({user,handleUsuario,handleCambio}) => {

        //Recupera las cookies del navegador
        const cookies = new Cookies();

        //Función para redireccionar
        const navigate = useNavigate()

        //Función para cerrar sesión
        const CerrarSesion = () => {
                cookies.remove("token",{ path: '/' })         //Remueve el token
                navigate("/login")                            //Envia al login
        }


        return (
        <div className=''>
                <div className='mid-grey-handbook p-2 shadow'>
                        <div className='container-fluid'>
                        <div className='justify-content-end'>
                                <div className="d-flex justify-content-between">
                                        <button className="btn shadow ml-auto" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasFono" aria-controls="offcanvasFono">
                                                <FaBars className='text-black fs-2'/>
                                        </button>

                                        <div class="dropdown">
                                                <button className="btn  dropdown-toggle h-100" type="button" id='fonoDropdown' data-bs-toggle="dropdown" aria-expanded="false">  
                                                        <span className='fw-bold'>{user.Nombre_Usuario}</span>
                                                        <img src='https://img.freepik.com/free-icon/user_318-159711.jpg' className='p-1' width="30px" height="30px"/>
                                                </button>
                                                <ul className="dropdown-menu mid-grey-handbook" aria-labelledby="fonoDropdown">
                                                        <li onClick={() => handleUsuario()}><a className="dropdown-item"> Mi usuario</a></li>
                                                        <li onClick={() => handleCambio()}><a className="dropdown-item"> Cambiar contraseña</a></li>
                                                        <hr/>
                                                        <li><button className="dropdown-item" type='submit' onClick={CerrarSesion}>Cerrar sesión</button></li>
                                                </ul>
                                        </div>
                                </div>
                        </div>
                        </div>
                </div>
        </div>
        )
}

export default Navbar