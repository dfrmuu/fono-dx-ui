import React from 'react'
import Lottie from 'lottie-react'
import HeartAnim from '../assets/lottie-anim/Heart.json'

//Componente que renderiza un spinner de carga personalizado en la pantalla
const SpinnerHeart = ({width,height}) => {
    return (
      <div className='spinnerHeartContainer'>
          <Lottie animationData={HeartAnim}
                  style={{
                    width : `${width}px`,
                    height : `${height}px`
            }}/>
      </div>
    )
}

export default SpinnerHeart