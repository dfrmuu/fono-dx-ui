export {default as RegistroMedico} from './RegistroMedico'
export {default as RegistroAdministrativo} from './RegistroAdministrativo'
export {default as RegistroInvestigador} from './RegistroInvestigador'