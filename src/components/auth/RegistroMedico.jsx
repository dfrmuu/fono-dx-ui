import {useState,useEffect} from 'react'
import { Form, Field, Formik, ErrorMessage} from 'formik'
import { toast } from 'react-toastify'
import { useNavigate } from 'react-router-dom'
import * as Yup from 'yup'
import axios from 'axios'
import Cookies from 'universal-cookie'
import { FaEye, FaEyeSlash } from 'react-icons/fa'

//Componente para el registro de médico
const RegistroMedico = ({correoUsuario, invitacion}) => {

    //Función para redireccionar
    const navigate = useNavigate()

    //Datos del formulario
    const [ci,setCi] = useState("")
    const [correo,setCorreo] = useState("")
    const [nombre,setNombre] = useState("")
    const [apellido,setApellido] = useState("")
    const [celular,setCelular] = useState("")
    const [cargo,setCargo] = useState("")
    const [area,setArea] = useState("")
    const [ciudades,setCiudades] = useState([])
    const [ciudad,setCiudad] = useState("")
    const [direccion,setDireccion] = useState("")
    const [contrasena,setContrasena] = useState("")

    //Controla la visibilidad de la contraseña
    const [contrasenaVisible,setContrasenaVisible] = useState(false)
    
    //Recupera las cookies del navegador
    const cookies = new Cookies()

    //Datos del API
    const VITE_API_LINK = import.meta.env.VITE_API_LINK


    //Ejecuta esta función cuando se renderiza el componente
    useEffect(() => {
        consultaCiudades()
        setCorreo(correoUsuario)
    }, [])
    

    //Controla el envio del formulario
    const handleSubmit = async (values) => {

        //Recupera el token
        const tokenAcceso = cookies.get("token")

        //Recupera los datos del formulario
        const {ci,job_title,phone,email,name,last_name,city,address,area,password} = values

        //Siendo el rol médico
        const rol = 2;

        //Envia los datos de registro
        await axios.post(VITE_API_LINK + "auth/registro",{
            invitacion: invitacion,
            ci : ci,
            job_title : job_title,
            phone : phone,
            email : email,
            name : name,
            last_name : last_name,
            city : city,
            roleID : rol,
            address : address,
            area : area,
            password : password
        },{
            headers: {
                Authorization: `Bearer ${tokenAcceso}`,
                ClientAuth : import.meta.env.VITE_APIKEY,
            },
            timeout : 10000
        })
        .then((response) => {
            toast.info(response.data.Message)
        }).catch((error) => {
            console.log(error)
            toast.error(error.response.data.Error)
        })
  
    }
  

    //Recupera las ciudades desde un API
    const consultaCiudades = async() => {
        try {
            
            await axios.get("https://www.datos.gov.co/resource/xdk5-pm3f.json")
                       .then((response) => {
                            //Construye un array a partir del nombre de las ciudad
                            const ciudades = response.data.map((ciudad) => ciudad.municipio)
                            setCiudades(ciudades)
                       }).catch((error) => {
                            console.log(error)
                       })

        } catch (error) {
            console.log(error)
        }
    }

    //Estructura correcta del formulario de registro
    const RegisterScheme = Yup.object().shape({
        ci : Yup.string()
                .required("Digite  el documento"),


        email : Yup.string()
                    .email("Por favor escriba un correo valido")
                    .required("El correo es requerido"),

        name : Yup.string()
                    .required("El nombre es requerido"),

        last_name : Yup.string()
                    .required("El apellido es requerido"),

        phone : Yup.string()
                     .required("El celular es requerido"),

        city : Yup.string()
                  .required("La ciudad es requerida"),

        address : Yup.string()
                 .required("La dirección es requerida"),

        job_title : Yup.string()
                   .required("El cargo es requerido"),

        area : Yup.string()
                  .required("El area es requerida"),
            
        password : Yup.string()
                      .required("La contraseña es requerida")
                      .min(6, "Minimo 6 caracteres").max(12, "Máximo 12 caracteres")
        
    })
  
    //Handles para la digitación de valores del formulario

    const handleCi = (e) => {
        setCi(e.target.value)
    }

    const handleCorreo = (e) => {
        setCorreo(e.target.value)
    }

    const handleNombre = (e) => {
        setNombre(e.target.value)
    }

    const handleApellido = (e) => {
        setApellido(e.target.value)
    }


    const handleCelular = (e) => {
        setCelular(e.target.value)
    }

    const handleCiudad = (e) => {
        setCiudad(e.target.value)
    }

    const handleDireccion = (e) => {
        setDireccion(e.target.value)
    }

    const handleCargo = (e) => {
        setCargo(e.target.value)
    }

    const handleArea = (e) => {
        setArea(e.target.value)
    }

    const handleContrasena = (e) => {
        setContrasena(e.target.value)
    }

    //Handle para el control de la visualización de la contraseña
    const muestraContrasena = () => {
        setContrasenaVisible(!contrasenaVisible)
    }

    return (

        <Formik enableReinitialize ={true}
                onSubmit = {async (values) => {
                    handleSubmit(values)
                }}
                validationSchema={RegisterScheme}
                initialValues={{ 
                    ci : ci,
                    email : correo,
                    name : nombre,
                    last_name : apellido,
                    city : ciudad,
                    address : direccion,
                    phone : celular,
                    job_title : cargo,
                    area : area,
                    password : contrasena
                }}>


            <Form>

                <div className="form-outline mb-4">

                    <h2 className='text-center dark-red-handbook-text fw-bold'> REGISTRO MÉDICO </h2>

                    <hr/>
                    <br/>

                    <div className='row'>
                        <div className='col-md-12'>
                            <label className='form-label fw-bold'>Número de documento: </label>
                            <Field type="text" className='form-control shadow-sm p-3 mb-2' id="ci" name="ci" onKeyUp={handleCi} placeholder="Escriba su número de documento"/>
                            <ErrorMessage className='dark-grey-handbook mt-2 mb-2 p-2 text-white rounded text-center fw-bold' component="div" id="ci" name="ci"/>
                        </div>
                    </div>

                    <br/>


                    <div className='row'>
                        <div className='col-lg-4 col-md-6'>
                            <label className='form-label fw-bold'>Nombres: </label>
                            <Field type="text" className='form-control shadow-sm p-3 mb-2' id="name" name="name" onKeyUp={handleNombre} placeholder="Escriba el/los nombres"/>
                            <ErrorMessage className='dark-grey-handbook mt-2 mb-2 p-2 text-white rounded text-center fw-bold' component="div" id="name" name="name"/>
                        </div>
                        <div className='col-lg-4 col-md-6'>
                            <label className='form-label fw-bold'>Apellidos: </label>
                            <Field type="text" className='form-control shadow-sm p-3 mb-2' id="last_name" name="last_name" onKeyUp={handleApellido} placeholder="Escriba el/los apellidos"/>
                            <ErrorMessage className='dark-grey-handbook mt-2 mb-2 p-2 text-white rounded text-center fw-bold' component="div" id="last_name" name="last_name"/>  
                        </div>
                        <div className='col-lg-4 col-md-12'>
                            <label className='form-label fw-bold'>Correo: </label>
                            <Field type="text" className='form-control shadow-sm p-3 mb-2' id="email" name="email" onKeyUp={handleCorreo} placeholder="Escriba el correo"/>
                            <ErrorMessage className='dark-grey-handbook mt-2 mb-2 p-2 text-white rounded text-center fw-bold' component="div" id="email" name="email"/>  
                        </div>
                    </div>

                    <br/>

                    <div className='row'>
                        <div className='col-lg-4 col-md-6'>
                            <label className='form-label fw-bold'>Número telefonico: </label>
                            <Field type="text" className='form-control shadow-sm p-3 mb-2 ' id="phone" name="phone" onKeyUp={handleCelular} placeholder="Escriba el teléfono"/>
                            <ErrorMessage className='dark-grey-handbook mt-2 mb-2 p-2 text-white rounded text-center fw-bold' component="div" id="phone" name="phone"/>  
                        </div>
                        <div className='col-lg-4 col-md-6'>
                            <label className='form-label fw-bold ' htmlFor='ciudad'> Ciudad:</label>
                            <Field as="select" className='form-select w-100 shadow-sm p-3 mb-2' name="city" id="city" 
                                    value ={ciudad} onChange={handleCiudad}>
                                    <option value="">Seleccione..</option>
                                    {ciudades.map((ciudad) => (
                                        <option value={ciudad} >
                                            {ciudad}
                                        </option>
                                    ))}
                            </Field>
                            <ErrorMessage name='city' component='div' className='dark-grey-handbook mt-2 mb-2 p-2 text-white rounded text-center fw-bold'/>
                        </div>
                        <div className='col-lg-4 col-md-12'>
                            <label className='form-label fw-bold'>Dirección: </label>
                            <Field type="text" className='form-control shadow-sm p-3 mb-2' id="address" name="address" onKeyUp={handleDireccion} placeholder="Escriba la dirección"/>
                            <ErrorMessage className='dark-grey-handbook mt-2 mb-2 p-2 text-white rounded text-center fw-bold' component="div" id="address" name="address"/>
                        </div>
                    </div>

                    <br/>


                    <div className='row'>
                        <div className='col-lg-4 col-md-6'>
                            <label className='form-label fw-bold'>Cargo: </label>
                            <Field type="text" className='form-control shadow-sm p-3 mb-2' id="job_title" name="job_title" onKeyUp={handleCargo} placeholder="Escriba el cargo"/>
                            <ErrorMessage className='dark-grey-handbook mt-2 mb-2 p-2 text-white rounded text-center fw-bold' component="div" id="job_title" name="job_title"/>
                        </div>
                        <div className='col-lg-4 col-md-6'>
                            <label className='form-label fw-bold'>Area: </label>
                            <Field type="text" className='form-control shadow-sm p-3 mb-2' id="area" name="area" onKeyUp={handleArea} placeholder="Escriba el area"/>
                            <ErrorMessage className='dark-grey-handbook mt-2 mb-2 p-2 text-white rounded text-center fw-bold' component="div" id="area" name="area"/>
                        </div>
                        <div className='col-lg-4 col-md-12'>
                            <label className='form-label fw-bold'>Contraseña: </label>
                            <div className='input-group'>
                                <Field type={contrasenaVisible == true ? "text" : "password"} className='form-control shadow-sm p-3' id="password" name="password" onKeyUp={handleContrasena} placeholder="Contraseña para su usuario" />
                                <div className='input-group-append'>
                                    <button className='border border-0 px-3 dark-red-handbook h-100 rounded-end text-white' type='button' onClick={() => muestraContrasena()}>
                                        {contrasenaVisible == false ? (
                                            <FaEye/>
                                        ) : (
                                            <FaEyeSlash/>
                                        )}
                                    </button>
                                </div>
                            </div>
                            <ErrorMessage className='dark-grey-handbook mt-2 mb-2 p-2 text-white rounded text-center fw-bold' component="div" id="password" name="password" />
                        </div>
                    </div>

                    <br/>


                    <p className='text-muted fw-bold' role='button'>Recordatorio: al usted registrarse en el aplicativo esta aceptando la politica de privacidad de FonoDX.</p>


                    <div className='row'>
                        <div className='col-md-12'>
                            <button className='btn dark-red-handbook mt-4 text-white w-100 p-3' type='submit'> 
                                Registrarse
                            </button>
                        </div>
                    </div>
                </div>
            </Form>
        </Formik>
    )
}

export default RegistroMedico