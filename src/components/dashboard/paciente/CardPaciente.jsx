import {useEffect, useState} from 'react'
import { useOutletContext } from 'react-router-dom'
import {Bar} from 'react-chartjs-2';
import { FaQuestionCircle } from 'react-icons/fa';
import Lottie from 'lottie-react';
import HeartLogin from '../../../assets/lottie-anim/HeartLogin.json'
import Confetti from 'react-confetti'
import useWindowDimensions from '../../../hooks/useWindowDimensions.js'
import Cookies from 'universal-cookie';
import axios from 'axios'
import Chart from 'chart.js/auto';

//Componente para renderizado del dashboard de administrador
const CardPaciente = () => {

    //Trae los datos del usuario
    const [user, setUser] = useOutletContext();

    //Toma las medidas de la pantalla
    const {height, width } = useWindowDimensions();


    //Variable para controlar confeti inicial
    const [confeti,setConfeti] = useState(false) 

    //Datos paciente
    const [conteoAuscultaciones,setConteoAuscultaciones] = useState([])


    //Datos del API
    const VITE_API_LINK = import.meta.env.VITE_API_LINK
    const cookies = new Cookies();

    //Función para la revisión de la actividad
    const revisionActividad = async () => {

        //Trae el token desde las cookies
        const tokenAcceso = cookies.get("token")

        await axios.get(VITE_API_LINK + "actividades/mis-actividades",{
            headers: {
                Authorization: `Bearer ${tokenAcceso}`,
                ClientAuth : import.meta.env.VITE_APIKEY
            },
            timeout : 10000  
        }).then((response) => {
            //Agrega los datos al estado
            setConteoAuscultaciones(response.data[0].SignalCount)
        }).catch((error) =>{
            console.log(error)
        })  
    }
    
    //Ejecuta esta función cada vez que renderice el componente
    useEffect(() => {
        revisionActividad()
        setConfeti(true)

        setTimeout(() => {
            setConfeti(false)
        }, 5500);
    }, [])



    //Handle para el modal de tareas (abierto - cerrado)
    const handleModalTareas = () => {
        setModalTareas(!modalTareas)
    }
    

    return (
        <div>

            {confeti == true ? (<Confetti width={width} height={height} numberOfPieces={250} recycle={false}/>) : null}

            <div className='row'>
                <div className='col-md-6 mb-2'>
                     <div className='col-md-12'>
                        <div className='py-3 pb-4 rounded shadow-lg position-relative'>
                            <div className='d-flex justify-content-center'>
                                <Lottie animationData={HeartLogin} style={{width : '100px', height : '100px'}}/>
                                <h4 className='fw-bold mt-5 me-4'>
                                    Hola, {`${user.Nombre_Usuario}`}
                                </h4>
                            </div>
                            <p className='fs-5 mt-3'>¡Nos alegra verte de nuevo en <b>FonoDX</b>!</p>
                        </div>
                    </div>
                <div className='col-md-12'>
                    <div className='col-sm-12 col-md-12 col-lg-12'>
                        <div className='py-5 mt-4 rounded shadow-lg px-5 dark-grey-handbook w-100 h-100'>
                                <div className='row'>
                                    <div className='col-md-12'>
                                        <p className='dark-red-handbook-text display-1 fw-bold' >
                                            {conteoAuscultaciones}
                                        </p>
                                    </div>
                                    <div className='col-md-12'>
                                        {parseInt(conteoAuscultaciones) > 1 ? (
                                            <p className='text-white fs-5'>
                                                auscultaciones cardiacas nuevas.
                                            </p>
                                        ) : (
                                             null
                                        )}

                                        {parseInt(conteoAuscultaciones) < 1  ? (
                                            <p className='text-white fs-5'>
                                                auscultaciones nuevas.
                                            </p>
                                        ) : (
                                            null
                                        )}

                                        {parseInt(conteoAuscultaciones) == 1 ?  (
                                            <p className='text-white fs-5'>
                                                auscultacion nueva.
                                            </p>
                                        ) : (
                                            null
                                        )}
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
                </div>
                    <div className='col-md-6 mb-2'>
                        <div className='py-3 pb-4 rounded shadow-lg position-relative h-100'>
                            <img src='https://img.freepik.com/free-icon/user_318-159711.jpg' width={100} height={100} className='img-fluid'/>

                            <div className='px-5 mt-1 text-start'>
                                <label className='fw-bold'>Nombre:</label>
                                <input type='text' className='form-control p-3 mb-4' disabled value={user.Nombre_Completo}/>

                                <label className='fw-bold'>Familiar:</label>
                                <input type='text' className='form-control p-3 text-uppercase mb-4' disabled value={user.Familiar}/>

                                <label className='fw-bold'>Telefono del familiar:</label>
                                <input type='text' className='form-control p-3 text-uppercase' disabled value={user.FamiliarTelefono}/>
                            </div>
                        </div>
                    </div>
                </div>

        </div>
    )
}

export default CardPaciente