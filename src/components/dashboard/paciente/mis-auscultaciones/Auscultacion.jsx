import React from 'react'
import { FaAngleDown } from 'react-icons/fa'
import { useOutletContext } from 'react-router-dom'

//Componente para renderizar las auscultaciones del paciente
const Auscultacion = ({auscultacion,handleVerSenales}) => {

    //Datos usuario
    const [user] = useOutletContext()

    //Recupera datos de la auscultación
    const {createdAt,focus} = auscultacion

    return (
        <tr className='align-middle light-grey-handbook'>
            <td className='tall-cell dark-grey-handbook fw-bold' role='button' onClick={() => handleVerSenales(createdAt)} title='Ampliar datos'>
                <FaAngleDown className='fw-bold dark-red-handbook-text fs-4'/>
            </td>
            <td> {user.Nombre_Completo}</td>
            <td className='tall-cell'>
                {createdAt}
            </td>
        </tr>
    )
}

export default Auscultacion