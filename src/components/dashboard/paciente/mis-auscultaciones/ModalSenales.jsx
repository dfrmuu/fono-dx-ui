import { useEffect, useState } from 'react'
import { FaEye, FaTimesCircle } from 'react-icons/fa'
import SpinnerHeart from '../../../SpinnerHeart'
import Modal from 'react-responsive-modal'
import axios from 'axios'
import Cookies from 'universal-cookie'
import Resultados from '../../medico/tabla/filas-tabla/Resultados'

//Componente para mostrar los datos de las auscultaciones por fecha
const ModalSenales = ({abierto,handleModal,fecha}) => {

    //Variable spinner de carga
    const [cargando,setCargando] = useState(false)

    //Variable para almacenar la auscultaciones
    const [senales,setSenales] = useState([])

    //Viendo resultados
    const [viendoResultados,setViendoResultados] = useState(false)

    //Variable para almacenar los resultados (si tiene)
    const [resultados,setResultados] = useState([])
    const [idSenal,setIdSenal] = useState("")

    //Datos API
    const cookies = new Cookies()
    const VITE_API_LINK = import.meta.env.VITE_API_LINK

    //Recupera las señales disponibles a partir de la fecha
    useEffect(() => {
        traerSenalesFecha()
    }, [])
    
    //Oculta las filas a partir de la auscultación que se esta revisando
    const ocultarFilaSenales = (id) => {
        const copiaSenales = [...senales]

        const filasOcultas = copiaSenales.filter((senal) => (senal.id === id))
        setSenales(filasOcultas)
    }

    //Recupera la lista de señales / auscultaciones por fecha
    const traerSenalesFecha = async () => {

        setCargando(true)
        const tokenAcceso = cookies.get("token")
  
        await axios.post(VITE_API_LINK + `senal/paciente/fecha`, {
            fecha_creacion: fecha,
        },{
            headers: {
                Authorization: `Bearer ${tokenAcceso}`,
                ClientAuth : import.meta.env.VITE_APIKEY,
                Resource : 'mis-auscultaciones',
                Action : 'listar'
            },
            timeout : 10000          
        }).then((response) => {
            setSenales(response.data)
        }).catch((error) => {
            console.log(error)
            toast.error(error.response.data.Error)
        }).finally(() => {
          setTimeout(() => {
              setCargando(false)
          }, 500);
        })
    }

    //Consulta los resultados de una auscultación
    const consultarResultados = async(id) => {
        try {
          setCargando(true)
          const tokenAcceso = cookies.get("token")

          await axios.get(VITE_API_LINK + `resultados/senal/${id}`, {
              headers: {
                  Authorization: `Bearer ${tokenAcceso}`,
                  ClientAuth : import.meta.env.VITE_APIKEY,
                  Resource: 'auscultacion',
                  Action : 'listar'
              },
              timeout : 10000
          })
          .then((response) =>{
              setResultados(response.data)
          }).catch((error) => {
              toast.error(error.response.data.Error)
          }).finally(() => {
              setTimeout(() => {
                  setCargando(false)
              }, 500);
          })
        } catch (error) {
          console.log(error)
      }
    }

    //Controla la visualización de resultados
    const handleResultados = (id) => {
        if(viendoResultados == true){
            traerSenalesFecha()
            setViendoResultados(false)
        }else{
            ocultarFilaSenales(id)
            consultarResultados(id)
            setViendoResultados(true)
        }
    }

    return (
        <>
            {cargando == true ? (
                <SpinnerHeart height={500} width={500}/>
            ) : (

                <Modal  open={abierto}
                        onClose={handleModal}
                        classNames={{
                            modal : 'w-75 rounded mid-grey-handbook'
                        }}
                        animationDuration={500}
                        center>

                                            
                        <h2 className='fw-bold text-uppercase text-center'>Lista auscultaciones</h2>

                        <hr/>

                        {cargando == true ? (
                            <SpinnerHeart height={500} width={500}/>
                        ) : (                        

                          senales.length > 0 ? (
                              senales.map((senal) => 
                  
                              <div className='row' key={senal.id}>
                                <div className='col-md-12 mt-3'>
                                    <div className="card light-grey-handbook shadow-sm">
                                      <div className="card-body">
                                        <div className='row mb-2'>
                                            <div className='col-md-12'>
                                              <h6 className="card-subtitle mt-2 text-muted">Foco aortico: <b>{senal.focus}</b></h6><br/>
    
                                              {senal.status == "procesada" ? (
                                                <h6 className="card-subtitle mb-2 text-muted">Estado de la señal: 
                                                  <b className='text-uppercase ms-1'>
                                                    PROCESADA (YA TIENE RESULTADOS)
                                                  </b>
                                                </h6>
                                              ) : (
                                                <h6 className="card-subtitle mb-2 text-muted">Estado de la señal: <b className='text-uppercase'>{senal.status}</b></h6>
                                              )}
                                            </div>
                                        </div>
                                        <div className='row'>
                                            <div className='col-md-12'>
                                              <audio className='shadow-sm rounded-pill mt-2 w-100 ' controls>
                                                <source src={senal.reference} type='audio/mpeg'/>
                                              </audio>
                                            </div>
                                        </div>
                                        <div className='row mt-2'>
    
                                            <div className='col-lg-12 col-md-12'>
                                              <button className='w-100 p-3 dark-red-handbook btn rounded mt-1  text-white'  type='button' onClick={() => handleResultados(senal.id)}>
                                                  <FaEye/>
                                                  Mostrar resultados
                                              </button>
                                            </div>
    
                                        </div>
            
                                        </div>
                                      </div>
                                  </div>
    
                                  {viendoResultados == true ? (
                                    <div className='col-md-12 mt-3'>
                                        <div className="card shadow-sm light-grey-handbook p-3">
                                        <h6 className="card-subtitle mb-2 mt-3 text-muted">Análisis de resultados en la señal</h6>
                                        <hr/>
    
                                        {cargando == true ? (
                                            <SpinnerHeart height={500} width={500}/>
                                        ) : (
    
                                            resultados.length > 0 ? (
                                                resultados.map((resultado) => (
                                                    <Resultados resultado={resultado} key={resultado.id} origen={""}/>
                                                ))
                                            ) : (
                                                <div className='row'>
                                                        <div className='col-md-12'>
                                                            <div className='bg-light p-3  fw-bold rounded text-center shadow'>
                                                                <FaTimesCircle/>
                                                                <span className='px-2'>Actualmente no hay resultados.</span>
                                                            </div>
                                                        </div>
                                                </div>
                                            )
                                        )}
    
                                        </div>
                                    </div>
                                  ) : (
                                    null
                                  )}
                              </div>
                            )
                          ) : (
                            <div className='px-1 rounded mt-2'>
                                <p className='alert dark-red-handbook text-white text-center'>
                                  <span className='ms-2'>Al parecer no hay auscultaciones en estas fechas.</span>
                                </p>
                            </div>
                          )
                      )}

                </Modal>
            )}
        </>
    )
}

export default ModalSenales