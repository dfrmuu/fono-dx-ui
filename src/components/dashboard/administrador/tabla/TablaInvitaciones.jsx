import {useState,useEffect} from 'react'
import {BeatLoader} from 'react-spinners'
import { Form, Field, Formik, ErrorMessage } from 'formik'
import { useOutletContext, useNavigate} from 'react-router-dom'
import { toast } from 'react-toastify'
import { Invitacion, InvitacionesCard } from './filas-tabla/FilasTabla.js'
import { FaEnvelope, FaHands, FaHandsWash, FaPaperPlane, FaSearch } from 'react-icons/fa'
import { RegistroInvitaciones } from '../../administrador/invitaciones/ComponentesInvitaciones.js'
import axios from 'axios'
import Cookies from 'universal-cookie';
import * as Yup from 'yup'
import SpinnerHeart from '../../../SpinnerHeart.jsx'
import EditarInvitacion from '../invitaciones/EditarInvitacion.jsx'
import SweetAlert from 'react-bootstrap-sweetalert'

//Componente para renderizar la tabla de invitaciones
const TablaInvitaciones = () => {

    //Función para redireccionar
    const navigate = useNavigate()

    //Datos del usuario
    const [user,rol,permisos] = useOutletContext()

    //Variable para spinner de carga
    const [cargando,setCargando] = useState(false)

    //Variable para almacenar las invitaciones
    const [invitaciones,setInvitaciones] = useState([])

    //Variables para filtros
    const [selectedOption,setSelectedOption] = useState("")
    const [dato,setDato] = useState("")


    //Modales CREAR - EDITAR  - ELIMINAR
    const [modalCrear,setModalCrear] = useState(false)

    const [idEditar,setIdEditar] = useState("")
    const [modalEditar,setModalEditar] = useState(false)

    const [idEliminar,setIdEliminar] = useState("")
    const [alertaEliminar,setAlertaEliminar] = useState(false)

    //Recupera las cookies del navegador y los datos del API
    const cookies = new Cookies()
    const VITE_API_LINK = import.meta.env.VITE_API_LINK

    useEffect(() => {
        misInvitaciones()
    }, [])
    

    //Trae las invitaciones
    const misInvitaciones = async () => {
        try{
            setCargando(true)
            const tokenAcceso = cookies.get("token")

            await axios.get(VITE_API_LINK + "invitaciones/mis-invitaciones",{
                headers: {
                    Authorization: `Bearer ${tokenAcceso}`,
                    ClientAuth : import.meta.env.VITE_APIKEY,
                    Resource : 'invitaciones',
                    Action : 'listar' 
                },
                timeout : 10000
            }).then((response) => {
                setInvitaciones(response.data)
            }).catch((error) => {
                toast.error(error.response.data.Error)
            }).finally(() => {
                setTimeout(() => {
                    setCargando(false) 
                }, 500);
            })
        }catch(error){
            console.log(error)
            setInvitaciones([])
        }
    }

    //Elimina una invitación
    const eliminarInvitacion = async () => {
        try{
            setCargando(true)
            const tokenAcceso = cookies.get("token")

            await axios.delete(VITE_API_LINK + `invitaciones/eliminar/${idEliminar}`,{
                headers: {
                    Authorization: `Bearer ${tokenAcceso}`,
                    ClientAuth : import.meta.env.VITE_APIKEY,
                    Resource : 'invitaciones',
                    Action : 'eliminar' 
                },
                timeout : 10000
            }).then((response) => {
                toast.info(response.data.Message)
                handleEliminar()
                

                misInvitaciones()

            }).catch((error) => {
                toast.error(error.response.data.Error)
            }).finally(() => {
                setTimeout(() => {
                    setCargando(false) 
                }, 500);
            })
        }catch(error){
            setInvitaciones([])
        }
    }

    //Handle para los datos de filtrado
    const handleCriterioChange = (e) => {
        setSelectedOption(e.target.value)
        setDato("")
    }

    const handleDatoChange = (e) => {
        setDato(e.target.value)
    }

    //Handle para los modales (crear,editar,eliminar)
    const handleCrear = () => {
        setModalCrear(!modalCrear)
    }

    const handleEliminar = (id) => {
        if(alertaEliminar == true) {
            setAlertaEliminar(false)
            setIdEliminar("")
        }else{
            setAlertaEliminar(true)
            setIdEliminar(id)
        }
    }

    const handleEditar = (id) => {
        if(modalEditar == true) {
            setModalEditar(false)
            setIdEditar("")
        }else{
            setModalEditar(true)
            setIdEditar(id)
        }
    }

    //Estructura para los filtros
    const SearchScheme = Yup.object().shape({
        criterio : Yup.string()
                    .required("El criterio de busqueda es requerido")
                    .oneOf(["status","email","user_type"],"El criterio de busqueda es requerido"),
        
        dato : Yup.string()
                .required("El dato de busqueda es requerido"),
    })

    //Filtrar invitaciones
    const filtrarDatos = async () => {

        setCargando(true)

        const tokenAcceso = cookies.get("token")

        await axios.post(VITE_API_LINK + "invitaciones/filtrar",{
            dato : dato,
            criterio : selectedOption
        },{
            headers: {
                Authorization: `Bearer ${tokenAcceso}`,
                ClientAuth : import.meta.env.VITE_APIKEY,
                Resource : 'invitaciones',
                Action : 'listar'
            },
            timeout : 10000
        })
        .then((response) => {
            setInvitaciones(response.data)
        }).catch((error) => {
            toast.error(error.response.data.Error)
        }).finally(() => {
            setTimeout(() => {
                setCargando(false)
            }, 500);
        })
    } 

    //Limpia la busqueda recuperando datos del API
    const limpiarBusqueda = () => {
        misInvitaciones()
        setSelectedOption("")
        setDato("")
    }


    return (
        <>

            
            <div className="position-fixed bottom-0 end-0 m-5 z-3">
                {permisos.some(permiso => permiso.resource === "invitaciones" && permiso.action === "crear") ? (
                    <div title='Enviar invitación'>
                        <button className='btn dark-red-handbook rounded-circle red-border shadow w-100 text-white p-3' type='button' onClick={() => handleCrear()}>
                            <FaPaperPlane className='fs-1'/>
                        </button>
                    </div>
                )  : (
                    null
                )}
            </div>



            <Formik enableReinitialize={true}
                            validationSchema={SearchScheme} 
                            initialValues={{criterio : selectedOption, dato : dato}}
                            onSubmit ={ async() => {filtrarDatos()}}>
                                        
                            {(props) => (

                            <Form>
                                <div className='row'>
                                        <h5 className='fw-bold mb-3 mt-3'> Filtros:</h5>
                                        <div className="col-lg-3 col-md-6">
                                            <Field as='select' className='form-select w-100 shadow dark-red-handbook text-white mt-2 p-3' name="critero" id="criterio"
                                                value ={selectedOption} onChange={handleCriterioChange}>
                                                <option value="">Seleccione un criterio de busqueda:</option>
                                                <option value="status">Estado</option>
                                                <option value="user_type">Rol</option>
                                                <option value="email">Correo electronico</option>
                                            </Field>
                                            <ErrorMessage name='criterio' component='div' className='dark-grey-handbook mt-2 mb-2 p-2 text-white rounded text-center fw-bold'/>
                                        </div>
                                        <div className="col-lg-4 col-md-6">
                                            {selectedOption == "email" ? (
                                                <>
                                                    <Field type="text" className='form-control shadow-sm mt-2 p-3' id='dato' name='dato' placeholder='Ingrese datos de busqueda'
                                                        onKeyUp={handleDatoChange}/>
                                                    <ErrorMessage name='dato' component='div' className='dark-grey-handbook mt-2 mb-2 p-2 text-white rounded text-center fw-bold'/>
                                                </>
                                            ) : (
                                                null
                                            )}

                                            {selectedOption == "status" ? (
                                                <>
                                                    <Field as="select" className='form-control shadow-sm mt-2 p-3' id='dato' name='dato'
                                                        onChange={handleDatoChange} value={dato}>
                                                            <option value=""> Selecione.. </option>
                                                            <option value="aceptada">Aceptada</option>
                                                            <option value="enviada">Enviada</option>
                                                            <option value="expirada">Expirada</option>
                                                    </Field>
                                                    <ErrorMessage name='dato' component='div' className='dark-grey-handbook mt-2 mb-2 p-2 text-white rounded text-center fw-bold'/>
                                                </>
                                            ) : (
                                                null
                                            )}


                                            {selectedOption == "user_type" ? (
                                                <>
                                                    <Field as="select" className='form-control shadow-sm mt-2 p-3' id='dato' name='dato'
                                                        onChange={handleDatoChange} value={dato}>
                                                            <option value=""> Selecione.. </option>
                                                            <option value="2">Médico</option>
                                                            <option value="3">Investigador</option>
                                                            <option value="4">Administrador</option>
                                                    </Field>
                                                    <ErrorMessage name='dato' component='div' className='dark-grey-handbook mt-2 mb-2 p-2 text-white rounded text-center fw-bold'/>
                                                </>
                                            ) : (
                                                null
                                            )}

                                            
                                            {selectedOption == "" ? (
                                                <div className='mt-2 p-3 dark-grey-handbook text-white text-center rounded shadow'>
                                                    Seleccione un criterio para consultar.
                                                </div>
                                            ) : (
                                                null
                                            )}

                                        </div>      
                                        <div className="col-lg-2 col-md-6">
                                            <button className='btn dark-red-handbook  text-white shadow-sm w-100 mt-2 p-3 ' type='submit'> 
                                                <FaSearch className='text-white'/> 
                                                <span className='px-2'>Buscar </span>
                                            </button>
                                        </div> 
                                        <div className="col-lg-3 col-md-6">
                                        <button className='btn dark-red-handbook  text-white shadow-sm w-100 mt-2 p-3' type='button' onClick={() => limpiarBusqueda()}>
                                            <FaHandsWash className='text-white'/>
                                            <span className='px-2'>Limpiar busqueda </span>
                                        </button>
                                    </div>              
                                </div>
                            </Form>
                         )}
            </Formik>

            <br/>
            <hr/>

            {modalCrear == true ? (
                <RegistroInvitaciones abierto={modalCrear}  handleCerrarModal={handleCrear} traerInvitaciones={misInvitaciones}/>
            ) : (
                null
            )}


            {modalEditar == true ? (
                <EditarInvitacion abierto={modalEditar}  handleCerrarModal={handleEditar} traerInvitaciones={misInvitaciones} idEditar={idEditar}/>
            ) : (
                null
            )}

            {alertaEliminar == true ? (
                <SweetAlert
                        warning
                        showCancel
                        confirmBtnText="Si"
                        cancelBtnText = "No"
                        confirmBtnBsStyle="success"
                        cancelBtnBsStyle="danger"
                        title="Estas seguro de eliminar esta invitación?"
                        onConfirm={() => eliminarInvitacion()}
                        onCancel={() => handleEliminar()}
                        focusCancelBtn>
                    </SweetAlert>
            ) : (
                null
            )}



            <div className='mt-5'>
                {cargando == true ? 

                    <SpinnerHeart height={500} width={500}/>: 
                    
                    <div className='row'>
                        {invitaciones.length > 0 ? (
                            invitaciones.map((invitacion) => (
                                <InvitacionesCard invitacion={invitacion} 
                                                  key={invitacion.id} 
                                                  handleEditar={handleEditar}
                                                  handleEliminar={handleEliminar}/>
                            ))
                        ) : (
                            <div className='row'>
                                    <div className='col-md-12'>
                                        <div className='gray-bg p-3 fw-bold rounded text-center'>
                                            Actualmente no hay invitaciones registradas.
                                        </div>
                                    </div>
                            </div>
                        )}
                    </div>  

                }
            </div>
        
        </>
    )
}

export default TablaInvitaciones