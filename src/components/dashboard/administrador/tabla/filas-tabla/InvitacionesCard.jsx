import { DateTime } from 'luxon'
import {useState,useEffect} from 'react'
import { FaAddressBook, FaBookMedical, FaEdit, FaEnvelope, FaRegEdit, FaSearch, FaTrash } from 'react-icons/fa'
import { useOutletContext } from 'react-router-dom'

//Componente para renderizar las invitaciones
const InvitacionesCard = ({invitacion, handleEditar, handleEliminar}) => {

    //Datos usuario
    const [user,rol,permisos,setPermisos] = useOutletContext()

    //Recupera los datos de la invitación
    const {id,status,email,user_type,createdAt,expirationDate} = invitacion

    //Cambia el formato de la fecha, agrega la zona actual
    const fecha_creacion_legible = DateTime.fromISO(createdAt).setZone('America/Bogota').toRelative()
    const fecha_caducidad_legible = DateTime.fromISO(expirationDate).setZone('America/Bogota').toRelative()

    return (
        <div className='col-lg-4 col-md-6'> 
            <div className='py-2 rounded light-grey-handbook shadow w-100'>
                <div className='row'>
                    <div className='col-md-12'>
                        <div className='dark-red-handbook-text  ms-4 mt-1'>
                            <FaEnvelope className='fs-2'/>
                        </div>
                    </div>
                </div>
                <div className='col-md-12'>
                    <table className="table text-center table-striped m-0" id='data-table'>
                        <thead>
                            <tr>
                                <th colSpan={2}>
                                    Datos invitación
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td className='align-middle fw-bold'>
                                    Tipo
                                </td>
                                <td className='text-uppercase'>
                                    {user_type == 2 ? (
                                        <span className='dark-grey-handbook text-white rounded p-1'>
                                            <FaBookMedical className='me-2'/>
                                            Medico
                                        </span>
                                    ) : (
                                        null
                                    )}

                                    {user_type == 3 ? (
                                        <span className='dark-grey-handbook  text-white rounded p-1'>
                                            <FaSearch className='me-2'/>
                                            Investigador
                                        </span>
                                    ) : (
                                        null
                                    )}

                                    {user_type == 4 ? (
                                        <span className='dark-grey-handbook  text-white rounded p-1'>
                                            <FaAddressBook className='me-2'/>
                                            Administrador
                                        </span>
                                    ) : (
                                        null
                                    )}
                                </td>
                            </tr>
                            <tr>
                                <td className='align-middle fw-bold'>
                                    Correo
                                </td>
                                <td>
                                    {email}
                                </td>
                            </tr>
                            <tr>
                                <td className='align-middle fw-bold'>
                                    Fecha creación
                                </td>
                                <td>
                                    {fecha_creacion_legible ? fecha_creacion_legible : ""}
                                </td>
                            </tr>
                            <tr>
                                <td className='align-middle fw-bold'>
                                    Fecha caducidad
                                </td>
                                <td>
                                    {status != "aceptada" ? (
                                        fecha_caducidad_legible ? fecha_caducidad_legible : ""
                                    ) : (
                                        "N.A"
                                    )}
                                </td>
                            </tr>
                            <tr>
                                <td className='align-middle fw-bold'>
                                    Estado
                                </td>
                                <td>
                                    {status == "aceptada" ? (
                                        <span className='bg-success text-uppercase text-white p-1 shadow rounded'>
                                            {status}
                                        </span>
                                    ) : (
                                        null
                                    )}

                                    {status == "enviada" ? (
                                        <span className='bg-info text-uppercase text-white p-1 shadow rounded'>
                                            {status}
                                        </span>
                                    ) : (
                                        null
                                    )}

                                    {status == "expirada" ? (
                                        <span className='bg-danher text-uppercase text-white p-1 shadow rounded'>
                                            {status}
                                        </span>
                                    ) : (
                                        null
                                    )}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div className='px-2 mt-2'>
                    <div className='row'>
                        <div className='col-md-6'>
                            
                            {status == "aceptada" || status == "expirada" ? (
                                <button className='dark-red-handbook  text-white btn w-100'>
                                    <FaRegEdit/>
                                </button>
                            ) : (
                                <button className='dark-red-handbook p-2 text-white btn w-100' onClick={() => handleEditar(id)}>
                                    <FaEdit/>
                                </button>
                            )}
                        </div>
                        <div className='col-md-6'>
                            <button className='dark-red-handbook text-white p-2 btn w-100' onClick={() => handleEliminar(id)}>
                                <FaTrash/>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default InvitacionesCard