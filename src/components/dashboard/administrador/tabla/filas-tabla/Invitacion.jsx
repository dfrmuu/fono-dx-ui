import { useState, useEffect} from 'react'
import { DateTime } from 'luxon'

//Este componente no renderiza las invitaciones como cards, en su lugar las toma como filas de una tabla
const Invitacion = ({invitacion}) => {

    //Recupera los datos de la invitación
    const {estado,tipo_usuario,fecha_creacion,fecha_caducidad} = invitacion

    //Formatea los datos de fecha
    const fecha_creacion_legible = DateTime.fromISO(fecha_creacion).toRelative()
    const fecha_caducidad_legible = DateTime.fromISO(fecha_caducidad).toRelative()

    return (
        <tr>
            {tipo_usuario == 2 ? (
                <td className='p-3 align-middle'>
                    <span className='text-uppercase py-1 px-2 fw-bold'>
                        Medico
                    </span>
                </td>
            ): null}

            {tipo_usuario == 3 ? (
                <td className='p-3 align-middle'>
                    <span className='text-uppercase py-1 px-2 fw-bold'>
                        Investigador
                    </span>
                </td>
            ) : null}


            {tipo_usuario == 4 ? (
                <td className='p-3 align-middle'>
                    <span className='text-uppercase py-1 px- fw-bold'>
                        Administrativo
                    </span>
                </td>
            ) : null}

            <td className='p-3 align-middle'>
                {invitacion.correo}
            </td>
            <td className='p-3 align-middle'>
                {fecha_creacion_legible}
            </td>

            {estado == 'enviada' ? (
                <>
                    <td className='fw-bold p-3 align-middle'>
                        {fecha_caducidad_legible}
                    </td>
                    <td className='p-3 align-middle'>
                        <span className='text-uppercase bg-info py-1 px-2 shadow rounded text-white fw-bold'>
                            {invitacion.estado}
                        </span>
                    </td>
                </>
            ): null}

            {estado == 'aceptada' ? (
                <>
                    <td> </td>
                    <td className='p-3 align-middle'>
                        <span className='text-uppercase bg-success py-1 px-2 shadow rounded text-white fw-bold'>
                            {invitacion.estado}
                        </span>
                    </td>
                </>
            ): null}

            {estado == 'expirada' ? (
                <>
                    <td> </td>
                    <td className='p-3 align-middle'>
                        <span className='text-uppercase bg-danger py-1 px-2 shadow rounded text-white fw-bold'>
                            {invitacion.estado}
                        </span>
                    </td>
                </>
            ) : null}
        </tr>
    )
}

export default Invitacion