import {useEffect, useState} from 'react'
import { useOutletContext } from 'react-router-dom'
import {Bar} from 'react-chartjs-2';
import { FaQuestionCircle } from 'react-icons/fa';
import Lottie from 'lottie-react';
import HeartLogin from '../../../assets/lottie-anim/HeartLogin.json'
import Confetti from 'react-confetti'
import useWindowDimensions from '../../../hooks/useWindowDimensions.js'
import Cookies from 'universal-cookie';
import axios from 'axios'
import Tareas from '../Tareas'
import Chart from 'chart.js/auto';

//Componente para renderizado del dashboard de administrador
const CardAdmin = () => {

    //Trae los datos del usuario
    const [user, setUser] = useOutletContext();

    //Toma las medidas de la pantalla
    const {height, width } = useWindowDimensions();


    //Variable para controlar confeti inicial
    const [confeti,setConfeti] = useState(false) 

    //Datos administrador
    const [conteoUsuarios,setConteoUsuarios] = useState([])
    const [conteoAuscultaciones,setConteoAuscultaciones] = useState([])

    //Graficos administrador
    const [barData,setBarData] = useState([])
    const [pieData,setPieData] = useState([])

    //Variable para controlar del modal de tareas
    const [modalTareas,setModalTareas] = useState(false)

    //Datos del API
    const VITE_API_LINK = import.meta.env.VITE_API_LINK
    const cookies = new Cookies();

    //Función para la revisión de la actividad
    const revisionActividad = async () => {

        //Trae el token desde las cookies
        const tokenAcceso = cookies.get("token")

        await axios.get(VITE_API_LINK + "actividades/mis-actividades",{
            headers: {
                Authorization: `Bearer ${tokenAcceso}`,
                ClientAuth : import.meta.env.VITE_APIKEY
            },
            timeout : 10000  
        }).then((response) => {

            console.log(response)

            //Agrega los datos al estado
            setConteoUsuarios(response.data.conteoUsuarios)
            setConteoAuscultaciones(response.data.conteoAuscultaciones)
        }).catch((error) =>{
            console.log(error)
        })  
    }
    
    //Ejecuta esta función cada vez que renderice el componente
    useEffect(() => {
        revisionActividad()
        setConfeti(true)

        setTimeout(() => {
            setConfeti(false)
        }, 5500);
    }, [])

    //Ejecuta esto cuando el conteo de usuarios tenga un valor diferente a nulo
    useEffect(() => {
        if(conteoUsuarios != null){
            // Procesa los datos de la consulta
            const labels = conteoUsuarios.map(result => result.role.name.toUpperCase());
            const userCount = conteoUsuarios.map(result => result.UserCount);

            setBarData({
                labels: labels,
                datasets: [
                    {
                        label: `Usuarios Registrados`,
                        data: userCount,
                        backgroundColor: '#cac7c7',
                        borderColor: '#f0f0f0',
                        borderWidth: 2,
                    },
                ],
            });
        }
    }, [conteoUsuarios])
    
    //Ejecuta esto cuando el conteo de auscultaciones tenga un valor diferente a nulo
    useEffect(() => {
        if(conteoAuscultaciones != null){
            // Procesa los datos de la consulta
            const labels = conteoAuscultaciones.map(result => {
                if(result.focus == 'FA'){
                    return 'Foco aórtico'
                } if (result.focus == 'FP'){
                    return 'Foco pulmonar'
                } if (result.focus == 'FM'){
                    return 'Foco mitral'
                } if (result.focus == 'FT'){
                    return 'Foco tricuspidal'
                }
            });
            const signalCount = conteoAuscultaciones.map(result => result.SignalCount);

            setPieData({
                labels: labels,
                datasets: [
                    {
                        label: `Señales`,
                        data: signalCount,
                        backgroundColor: '#cac7c7',
                        borderColor: '#f0f0f0',
                        borderWidth: 2,
                    },
                ],
            });
        }
    }, [conteoAuscultaciones])
    

    //Handle para el modal de tareas (abierto - cerrado)
    const handleModalTareas = () => {
        setModalTareas(!modalTareas)
    }
    

    return (
        <div>

            {confeti == true ? (<Confetti width={width} height={height} numberOfPieces={250} recycle={false}/>) : null}


            {modalTareas == true ? (
                <Tareas abierto={modalTareas} handleModal={handleModalTareas}/>
            ) : (
                null
            )}

            <div className='row'>
                <div className='col-md-6 mb-2'>
                     <div className='col-md-12'>
                        <div className='py-3 pb-4 rounded shadow-lg position-relative'>
                            <div className='d-flex justify-content-center'>
                                <Lottie animationData={HeartLogin} style={{width : '100px', height : '100px'}}/>
                                <h4 className='fw-bold mt-5 me-4'>
                                    Hola, {`${user.Nombre_Usuario}`}
                                </h4>
                            </div>
                            <p className='fs-5 mt-3'>¡Nos alegra verte de nuevo en <b>FonoDX</b>!</p>
                        </div>
                    </div>
                <div className='col-md-12'>
                <div className='col-sm-12 col-md-12 col-lg-12'>
                    <div className='p-5 mt-4 rounded shadow-lg h-100 ' role='button' onClick={() => handleModalTareas()}>
                        <div>
                            <FaQuestionCircle className=' mx-auto d-block fs-1 dark-red-handbook-text'/>
                            <h4 className='fw-bold mt-3 text-center'> Tareas</h4>
                        </div>
                            <p className='fs-5 mt-5'> ¿Tienes alguna tarea en mente?.</p>
                        </div>
                    </div>
                </div>
                </div>
                    <div className='col-md-6 mb-2'>
                        <div className='py-3 pb-4 rounded shadow-lg position-relative h-100'>
                            <img src='https://img.freepik.com/free-icon/user_318-159711.jpg' width={100} height={100} className='img-fluid'/>

                            <div className='px-5 mt-1 text-start'>
                                <label className='fw-bold'>Nombre:</label>
                                <input type='text' className='form-control p-3 mb-4' disabled value={user.Nombre_Completo}/>

                                <label className='fw-bold'>Cargo:</label>
                                <input type='text' className='form-control p-3 text-uppercase mb-4' disabled value={user.Cargo}/>

                                <label className='fw-bold'>Area:</label>
                                <input type='text' className='form-control p-3 text-uppercase' disabled value={user.Area}/>
                            </div>
                        </div>
                    </div>
                </div>


                <br/>
                
                {conteoUsuarios != null  && conteoAuscultaciones != null ? (
                    <div className='mb-3'>
                        <div className='row'>
                            <div className='col-lg-6 col-md-12 '>
                                <div className='py-5 mt-4 rounded shadow-lg px-5 dark-grey-handbook mb-2'>
                                    <span className='text-center text-white fs-4 '> Total de señales por foco aortico.</span>
                                    <hr/>
                                    {pieData.labels != null ? (
                                        <Bar data={pieData}  className='d-flex justify-content-center align-items-middle'/>
                                    ) : (
                                        null
                                    )}
                                </div>
                            </div>    

                            <div className='col-lg-6 col-md-12'>
                                <div className='py-5 mt-4 rounded shadow-lg px-5 dark-grey-handbook mb-2'>
                                    <span className='text-center text-white fs-4'> Total de usuarios por rol.</span>
                                    <hr/>
                                    {barData.labels != null ? (
                                        <Bar data={barData}  className='d-flex justify-content-center align-items-middle'/>
                                    ) : (
                                        null
                                    )}
                                </div>
                            </div>
                        </div>
                    </div>
                ):(
                    null
                )}
        </div>
    )
}

export default CardAdmin