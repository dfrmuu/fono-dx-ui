import { DateTime } from 'luxon'
import {useState,useEffect} from 'react'
import { toast } from 'react-toastify'
import { ErrorMessage, Field, Form, Formik } from 'formik'
import Modal from 'react-responsive-modal'
import Cookies from 'universal-cookie'
import SpinnerHeart from '../../../SpinnerHeart'
import axios from 'axios'
import * as Yup from 'yup'
import { FaAngleLeft, FaAngleRight, FaHandsWash, FaSearch } from 'react-icons/fa'

//Componente para revisar los inicios de sesión del sistema
const RegistrosInicio = ({abierto,handleModal,id}) => {

    //Variable para almacenar los registros de inicio
    const [registros,setRegistros] = useState([])

    //Variable para almacenar los registros de inicio (paginación)
    const [currentRegistros,setCurrentRegistros] = useState([])

    //Variable spinner de carga
    const [cargando,setCargando] = useState(false)

    //Datos del API
    const VITE_API_LINK = import.meta.env.VITE_API_LINK

    //Recupera las cookies del sistema
    const cookies = new Cookies()

    useEffect(() => {
        traerRegistrosInicio()
    }, [])
    

    //Filtros
    const [fechaInicio,setFechaInicio] = useState("") 
    const [fechaFin,setFechaFin] = useState("") 

    //PAGINACION
    const [currentPage, setCurrentPage] = useState(1);
    const itemsPerPage = 10;

    // Calcula los índices y filtra los equipos para la página actual
    const indexOfLastItem = currentPage * itemsPerPage;

    // Función para cambiar de página
    const handlePageChange = (pageNumber) => {
        if (pageNumber >= 1 && pageNumber <= Math.ceil(usuarios.length / itemsPerPage)) {
          setCurrentPage(pageNumber);
  
          // Calcular los índices y actualizar currentMarcas para reflejar la página actual
          const newIndexOfLastItem = pageNumber * itemsPerPage;
          const newIndexOfFirstItem = newIndexOfLastItem - itemsPerPage;
          setCurrentUsuarios(usuarios.slice(newIndexOfFirstItem, newIndexOfLastItem));
        }
    };

    //Trae los registros de inicio del sistema
    const traerRegistrosInicio = async() => {
        try {
            setCargando(true)
            const tokenAcceso = cookies.get("token")
    
            await axios.get(VITE_API_LINK + `logs/iniciosesion/${id}`,{
                headers: {
                    Authorization: `Bearer ${tokenAcceso}`,
                    ClientAuth : import.meta.env.VITE_APIKEY,
                    Resource : 'logs',
                    Action : 'listar'
                },
                timeout : 10000
            })
            .then((response) => {
                setRegistros(response.data)
                setCurrentRegistros(response.data)
            }).catch((error) => {
                console.log(error)
                setRegistros([])
                setCurrentRegistros([])
                toast.error(error.response.data.Error)
            }).finally(() => {
                setTimeout(() => {
                    setCargando(false) 
                }, 200);
            })
        } catch (error) {
            console.log(error)
        }
    }

    //Filtra las fechas del sistema
    const filtrarFechas = async() => { 
        try {
            setCargando(true)
            const tokenAcceso = cookies.get("token")
            var data 

            data = {
                UserID : id,
                fecha_inicio : fechaInicio
            }

            if(fechaFin != "" || fechaFin != null){
                data.fecha_fin = fechaFin
            }
        

            await axios.post(VITE_API_LINK + `logs/iniciosesion/filtrar`,{
                data
            },{
                headers: {
                    Authorization: `Bearer ${tokenAcceso}`,
                    ClientAuth : import.meta.env.VITE_APIKEY,
                    Resource : 'logs',
                    Action : 'listar'
                },
                timeout : 10000
            })
            .then((response) => {
                setRegistros(response.data)
                setCurrentRegistros(response.data)
            }).catch((error) => {
                console.log(error)
                setRegistros([])
                setCurrentRegistros([])
                toast.error(error.response.data.Error)
            }).finally(() => {
                setTimeout(() => {
                    setCargando(false) 
                }, 200);
            })
        } catch (error) {
            console.log(error)
        }
    }

    //Estructura para el filtro de registros
    const searchScheme = Yup.object().shape({
        fechaInicio : Yup.date()
                        .required("Debe escoger una fecha"),

        fechaFin : Yup.date(),
    })


    //Handle para la digitación de datos
    const handleFechaInicio = (e) => {
        setFechaInicio(e.target.value)
    }

    const handleFechaFin = (e) => {
        setFechaFin(e.target.value)
    }

    return (
        <Modal open={abierto}
               onClose={handleModal}
               classNames={{
                modal : 'w-75 rounded mid-grey-handbook'
               }}
               animationDuration={500}
               center>

                <h2 className='fw-bold text-uppercase text-center'>Logs de inicios de sesión</h2>
                <hr/>

                <div className='p-2 mb-3'>
                    <div className='row'>
                        <Formik enableReinitialize={true}
                                initialValues={{
                                    fechaInicio : fechaInicio,
                                    fechaFin : fechaFin
                                }}
                                validationSchema={searchScheme}
                                onSubmit = {async() => filtrarFechas()}>
                                <Form>
                                    <label className='fw-bold mb-3'>Filtrar logs por fecha (puedes dejar la de fin vacia):</label>
                                    <div className='row'>
                                        <div className='col-md-12 col-lg-6'>
                                            <label className='fw-bold'>Inicio :</label>
                                            <Field  type="date" className='form-control p-3 mb-1 text-center w-100 shadow-sm' id="fechaInicio" name="fechaInicio" onChange={handleFechaInicio}/>
                                            <ErrorMessage name='fechaInicio' component='div' className='dark-grey-handbook mt-2 mb-2 p-2 text-white rounded text-center'/>
                                        </div>
                                        <div className='col-md-12 col-lg-6'>
                                            <label className='fw-bold'>Fin :</label>
                                            <Field  type="date" className='form-control p-3 mb-1 text-center w-100 shadow-sm' id="fechaFin" name="fechaFin" onChange={handleFechaFin}/>
                                            <ErrorMessage name='fechaFin' component='div' className='dark-grey-handbook mt-2 mb-2 p-2  text-white rounded text-center'/>
                                        </div>
                                    </div>

                                    <br/>

                                    <div className='row'>
                                        <div className='col-md-6'>
                                            <button className='dark-red-handbook w-100 p-3  btn text-white shadow-sm' type='submit'>
                                                <FaSearch className='px-1 fs-4'/>
                                                Buscar
                                            </button>
                                        </div>
                                        <div className='col-md-6'>
                                            <button className='dark-red-handbook  w-100 p-3  btn text-white shadow-sm' type='button' onClick={() => traerRegistrosInicio()}>
                                                <FaHandsWash className='px-1 fs-4'/>
                                                Limpiar busqueda
                                            </button>
                                        </div>
                                    </div>
                                </Form>
                        </Formik>
                    </div>
                </div>

                <hr/>

                {cargando == true ? (
                    <SpinnerHeart height={350} width={350}/>
                ) : (
                    registros.length > 0 ? (
                        <>
                        <div className='table-responsive px-2'>
                            <table className='table table-borderless text-center rounded-1 border overflow-hidden'>
                                <thead className='dark-grey-handbook text-white'>
                                    <tr className='align-middle rounded-start'>
                                        <th className='tall-cell' scope="col">Fecha</th>
                                        <th className='tall-cell' scope="col">Usuario</th>
                                        <th className='tall-cell' scope="col">Dispositivo</th>
                                        <th className='tall-cell' scope="col">Ingresos</th>
                                    </tr>
                                </thead>                                
                                <tbody>
                                    {currentRegistros.map((registro) => (
                                        <tr key={registro.id + "-" + registro.device} className='light-grey-handbook'>
                                            <td className='align-middle tall-cell'>
                                                {DateTime.fromISO(registro.createdDate).toFormat('yyyy-MM-dd')}
                                            </td>
                                            <td className='align-middle tall-cell'>
                                                {registro.user.name + " " + registro.user.last_name}
                                            </td>
                                            <td className='align-middle tall-cell text-uppercase fw-bold'>
                                                {registro.device}
                                            </td>
                                            <td className='align-middle tall-cell text-uppercase fw-bold dark-grey-handbook'>
                                                {registro.ingresos}
                                            </td>
                                        </tr>
                                    ))}
                                </tbody>
                            </table>
                        </div>

                        <br/>

                        {registros.length >= 10 ? (
                            <div className='p-3'>
                                <hr/>
                                <div className='row'> 
                                    <div className='col-md-4'>
                                        <div className='d-flex justify-content-center'>
                                            <button className={`btn rounded-circle fw-bold light-grey-handbook-btn shadow mb-2`} type='button'
                                                    onClick={() => handlePageChange(currentPage - 1)} disabled={currentPage === 1}>
                                                <FaAngleLeft/>
                                            </button>
                                        </div>
                                    </div>
                                    <div className='col-md-4'>
                                        <div className='d-flex justify-content-center align-middle'>
                                            <p className={`fw-bold dark-red-handbook-text mt-2`}>Página {currentPage}</p>
                                        </div>
                                    </div>
                                    <div className='col-md-4'>
                                        <div className='d-flex justify-content-center'>
                                            <button className={` btn rounded-circle fw-bold light-grey-handbook-btn shadow mb-2`} type='button'
                                                    onClick={() => handlePageChange(currentPage + 1)} disabled={indexOfLastItem >= registros.length}>
                                                <FaAngleRight/>
                                            </button>
                                        </div>
                                    </div>  
                                </div>
                            </div>
                        ) : (
                            null
                        )}  

                        </>
                    ) : (
                        <div className='row'>
                            <div className='col-md-12'>
                                <div className='gray-bg p-3 fw-bold rounded text-center'>
                                    Actualmente no hay registros de inicio de sesión.
                                 </div>
                            </div>
                        </div>
                    )

                )}

        </Modal>
    )
}

export default RegistrosInicio