import axios from 'axios'
import { ErrorMessage, Field, Form, Formik } from 'formik'
import {useState,useEffect} from 'react'
import Modal from 'react-responsive-modal'
import { toast } from 'react-toastify'
import Cookies from 'universal-cookie'
import * as Yup from 'yup'

//Componente para la creación de un usuario
const CrearUsuario = ({abierto,handleModal,traerUsuario}) => {

    //Variables para los datos del formulario
    const [ci,setCi] = useState("")
    const [correo,setCorreo] = useState("")
    const [nombre,setNombre] = useState("")
    const [apellido,setApellido] = useState("")
    const [celular,setCelular] = useState("")
    const [rol,setRol] = useState("")
    const [cargo,setCargo] = useState("")
    const [area,setArea] = useState("")
    const [ciudades,setCiudades] = useState([])
    const [ciudad,setCiudad] = useState("")
    const [direccion,setDireccion] = useState("")

    //Recuperación de cookies del navegador
    const cookies = new Cookies()

    //Datos del API
    const VITE_API_LINK = import.meta.env.VITE_API_LINK

    //Recupera las ciudades
    useEffect(() => {
        consultaCiudades()
    }, [])
    
    //Estructura para el formulario de un usuario
    const usuarioScheme = Yup.object().shape({
        ci : Yup.string()
                .required("Digite  el documento"),


        email : Yup.string()
                    .email("Por favor escriba un correo valido")
                    .required("El correo es requerido"),

        name : Yup.string()
                    .required("El nombre es requerido"),

        last_name : Yup.string()
                    .required("El apellido es requerido"),

        phone : Yup.string()
                     .required("El celular es requerido"),

        city : Yup.string()
                  .required("La ciudad es requerida"),

        address : Yup.string()
                 .required("La dirección es requerida"),


        rol : Yup.string()
                 .oneOf(['paciente','administrativo','medico', 'investigador'])
                 .required("El rol es requerido."),
    
        job_title : Yup.string()
                   .required("El cargo es requerido"),

        area : Yup.string()
                  .required("El area es requerida")
    })

    //Handle para la digitación de datos
    const handleCi = (e) => {
        setCi(e.target.value)
    }

    const handleCorreo = (e) => {
        setCorreo(e.target.value)
    }

    const handleNombre = (e) => {
        setNombre(e.target.value)
    }

    const handleApellido = (e) => {
        setApellido(e.target.value)
    }


    const handleCelular = (e) => {
        setCelular(e.target.value)
    }

    const handleCiudad = (e) => {
        setCiudad(e.target.value)
    }

    const handleDireccion = (e) => {
        setDireccion(e.target.value)
    }

    const handleCargo = (e) => {
        setCargo(e.target.value)
    }

    const handleArea = (e) => {
        setArea(e.target.value)
    }

    const handleRol = (e) => {
        setRol(e.target.value)
    }

    //Envia los datos del usuario
    const handleSubmit = async (values) => {
        try {

            const tokenAcceso = cookies.get("token")
            const {ci,job_title,phone,email,name,last_name,rol,city,address,area} = values

            await axios.post(VITE_API_LINK + "usuarios/crear",{
                ci : ci,
                job_title : job_title,
                phone : phone,
                email : email,
                name : name,
                last_name : last_name,
                city : city,
                rol : rol,
                address : address,
                area : area
            },{
                headers: {
                    Authorization: `Bearer ${tokenAcceso}`,
                    ClientAuth : import.meta.env.VITE_APIKEY,
                    Resource : 'usuarios',
                    Action : 'crear'
                },
                timeout : 10000
            })
            .then((response) => {
                toast.info(response.data.Message)
                traerUsuario()
            }).catch((error) => {
                console.log(error)
                toast.error(error.response.data.Error)
            })
        } catch (error) {
            console.log(error)
        }
    }

    //Consulta las ciudades de un API externa
    const consultaCiudades = async() => {
        try {
            
            await axios.get("https://www.datos.gov.co/resource/xdk5-pm3f.json")
                       .then((response) => {
                            const ciudades = response.data.map((ciudad) => ciudad.municipio)
                            setCiudades(ciudades)
                       }).catch((error) => {
                            console.log(error)
                       })

        } catch (error) {
            console.log(error)
        }
    }

    return (
        <Modal open={abierto}
               onClose={handleModal}
               classNames={{
                modal : 'w-75 rounded mid-grey-handbook'
               }}
               animationDuration={500}
               center>
            
            <Formik validationSchema={usuarioScheme}
                    enableReinitialize={true}
                    initialValues={{
                        ci : ci,
                        email : correo,
                        name : nombre,
                        last_name : apellido,
                        city : ciudad,
                        address : direccion,
                        phone : celular,
                        job_title : cargo,
                        area : area,
                        rol : rol,
                    }}
                    onSubmit={async(values) => {
                        handleSubmit(values)
                    }}>
                <Form>

                    <h2 className='fw-bold text-uppercase text-center'>Crear usuario</h2>
                    <hr/>

                    <br/>

                    <div className='row'>
                        <div className='col-md-6'>
                            <label className='form-label fw-bold'>Rol del usuario: </label>
                            <Field as="select" className='form-select w-100 shadow-sm p-3 mb-2' name="rol" id="rol" 
                                    value ={rol} onChange={handleRol}>
                                    <option value="">Seleccione..</option>
                                    <option value="administrativo">Administrador</option>
                                    <option value="medico">Médico</option>
                                    {/* <option value="paciente">Paciente</option> */}
                                    <option value="investigador">Investigador</option>
                            </Field>
                            <ErrorMessage className='dark-grey-handbook mt-2 mb-2 p-2 text-white rounded text-center fw-bold' component="div" id="rol" name="rol"/>
                        </div>
                        <div className='col-md-6'>
                            <label className='form-label fw-bold'>Documento: </label>
                            <Field type="text" className='form-control shadow-sm p-3 mb-2' id="ci" name="ci" onKeyUp={handleCi} placeholder="Escriba el documento"/>
                            <ErrorMessage className='dark-grey-handbook mt-2 mb-2 p-2 text-white rounded text-center fw-bold' component="div" id="ci" name="ci"/>  
                        </div>
                    </div>

                    <br/>

                    <div className='row'>
                        <div className='col-md-6'>
                            <label className='form-label fw-bold'>Nombres: </label>
                            <Field type="text" className='form-control shadow-sm p-3 mb-2' id="name" name="name" onKeyUp={handleNombre} placeholder="Escriba el/los nombres"/>
                            <ErrorMessage className='dark-grey-handbook mt-2 mb-2 p-2 text-white rounded text-center fw-bold' component="div" id="name" name="name"/>
                        </div>
                        <div className='col-md-6'>
                            <label className='form-label fw-bold'>Apellidos: </label>
                            <Field type="text" className='form-control shadow-sm p-3 mb-2' id="last_name" name="last_name" onKeyUp={handleApellido} placeholder="Escriba el/los apellidos"/>
                            <ErrorMessage className='dark-grey-handbook mt-2 mb-2 p-2 text-white rounded text-center fw-bold' component="div" id="last_name" name="last_name"/>  
                        </div>
                    </div>


                    <br/>

                    <div className='row'>
                        <div className='col-md-6'>
                            <label className='form-label fw-bold'>Correo: </label>
                            <Field type="text" className='form-control shadow-sm p-3 mb-2' id="email" name="email" onKeyUp={handleCorreo} placeholder="Escriba el correo"/>
                            <ErrorMessage className='dark-grey-handbook mt-2 mb-2 p-2 text-white rounded text-center fw-bold' component="div" id="email" name="email"/>  
                        </div>
                        <div className='col-md-6'>
                            <label className='form-label fw-bold'>Número telefonico: </label>
                            <Field type="text" className='form-control shadow-sm p-3 mb-2 ' id="phone" name="phone" onKeyUp={handleCelular} placeholder="Escriba el teléfono"/>
                            <ErrorMessage className='dark-grey-handbook mt-2 mb-2 p-2 text-white rounded text-center fw-bold' component="div" id="phone" name="phone"/>  
                        </div>
                    </div>

                    <br/>

                    <div className='row'>
                        <div className='col-md-6'>
                            <label className='form-label fw-bold ' htmlFor='ciudad'> Ciudad:</label>
                            <Field as="select" className='form-select w-100 shadow-sm p-3 mb-2' name="city" id="city" 
                                    value ={ciudad} onChange={handleCiudad}>
                                    <option value="">Seleccione..</option>
                                    {ciudades.map((ciudad) => (
                                        <option value={ciudad} >
                                            {ciudad}
                                        </option>
                                    ))}
                            </Field>
                            <ErrorMessage name='city' component='div' className='dark-grey-handbook mt-2 mb-2 p-2 text-white rounded text-center fw-bold'/>
                        </div>
                        <div className='col-md-6'>
                            <label className='form-label fw-bold'>Dirección: </label>
                            <Field type="text" className='form-control shadow-sm p-3 mb-2' id="address" name="address" onKeyUp={handleDireccion} placeholder="Escriba la dirección"/>
                            <ErrorMessage className='dark-grey-handbook mt-2 mb-2 p-2 text-white rounded text-center fw-bold' component="div" id="address" name="address"/>
                        </div>
                    </div>

                    

                    <br/>


                    {rol == "paciente" ? (
                        null
                    ) : (
                        <>
                            <hr/>
                            <div className='row'>
                                <div className='col-md-6'>
                                    <label className='form-label fw-bold'>Cargo: </label>
                                    <Field type="text" className='form-control shadow-sm p-3 mb-2' id="job_title" name="job_title" onKeyUp={handleCargo} placeholder="Escriba el cargo"/>
                                    <ErrorMessage className='dark-grey-handbook mt-2 mb-2 p-2 text-white rounded text-center fw-bold' component="div" id="job_title" name="job_title"/>
                                </div>
                                <div className='col-md-6'>
                                    <label className='form-label fw-bold'>Area: </label>
                                    <Field type="text" className='form-control shadow-sm p-3 mb-2' id="area" name="area" onKeyUp={handleArea} placeholder="Escriba el area"/>
                                    <ErrorMessage className='dark-grey-handbook mt-2 mb-2 p-2 text-white rounded text-center fw-bold' component="div" id="area" name="area"/>
                                </div>
                            </div>
                            
                            <br/>
                        </>
                    )}



                    <div className='row'>
                        <div className='col-md-12'>
                            <button className='btn dark-red-handbook mt-2 mb-2 text-white w-100 p-3' type='submit'> 
                                Crear usuario
                            </button>
                        </div>
                    </div>
                </Form>
            </Formik>

        </Modal>
    )
}

export default CrearUsuario