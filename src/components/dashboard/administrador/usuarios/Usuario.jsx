import React from 'react'
import { FaBook, FaBookMedical, FaClinicMedical, FaEye, FaKey, FaPowerOff, FaRedo, FaSearch, FaUser, FaUserSlash } from 'react-icons/fa'
import { useOutletContext } from 'react-router-dom'

//Componente para renderizar los datos de un usuario
const Usuario = ({usuario,handleActivar,handleEditar,handleEliminar,handleDesactivar, handleRegistrosInicio}) => {

    //Recupera datos del usuario provenientes del inicio de sesión
    const [user,rol,permisos] = useOutletContext()

    //Recupera los datos de cada usuario
    const {id,email,ci, name, last_name, phone, name_user, status, role} = usuario

    return (
        <tr className='light-grey-handbook'>
            <td className='align-middle dark-grey-handbook'>
                <small className='fw-bold text-uppercase text-white px-4'>
                    {status}
                </small>
            </td>  
            <td className='align-middle tall-cell'>
                {ci}
            </td>
            <td className='align-middle tall-cell'>
                {name + " " + last_name}
            </td>
            <td className='align-middle tall-cell'>
                {email}
            </td>
            <td className='align-middle tall-cell'>
                {phone}
            </td>
            <td className='align-middle tall-cell'>
                {name_user}
            </td>              
            <td className='align-middle tall-cell'>

                {role.name == "médico" ? (
                    <p className='fw-bold px-1 rounded-pill'>
                        <FaBookMedical/>
                        <span className='px-2'>Medico</span>
                    </p>
                ) : (
                    null
                )}

                {role.name == "administrador" ? (
                    <p className='fw-bold px-1 rounded-pill'>
                        <FaBook/>
                        <span className='px-2'>Administrador</span>
                    </p>
                ) : (
                    null
                )}

                {role.name == "paciente" ? (
                    <p className='fw-bold px-1 rounded-pill'>
                        <FaClinicMedical/>
                        <span className='px-2'>Paciente</span>
                    </p>
                ) : (
                    null
                )}


            </td>
            <td>
                {status == "activo" ? (
                    <div className='col-md-12'>
                        <button className='dark-red-handbook btn  w-100 p-3 mt-1 text-white' onClick={() => handleDesactivar(id)}>
                            <FaPowerOff/>
                            <span className='px-2'>Desactivar usuario</span>
                        </button>
                    </div>
                ) : (
                    <div className='col-md-12'>
                        <button className='dark-red-handbook btn  w-100 p-3 mt-1 text-white' onClick={() => handleActivar(id)}>
                            <FaPowerOff/>
                            <span className='px-2'>Activar usuario</span>
                        </button>
                    </div>
                )}

                {permisos.some(permiso => permiso.resource === "usuarios" && permiso.action === "editar") ? (
                    <div className='col-md-12'>
                        <button className='dark-red-handbook btn  w-100 p-3 mt-1 text-white' onClick={() => handleEditar(id)}>
                            <FaUser/>
                            <span className='px-2'>Editar usuario</span>
                        </button>
                    </div>
                ):(
                    null
                )}

                {permisos.some(permiso => permiso.resource === "usuarios" && permiso.action === "eliminar") ? (
                    <div className='col-md-12'>
                        <button className='dark-red-handbook btn  w-100 p-3 mt-1 text-white' onClick={() => handleEliminar(id)}>
                            <FaUserSlash/>
                            <span className='px-2'>Eliminar usuario</span>
                        </button>
                    </div>
                ):(
                    null
                )}
            
                <div className='col-md-12'>
                    <button className='dark-red-handbook btn  w-100 p-3 mt-1 text-white' onClick={() => handleRegistrosInicio(id)}>
                        <FaEye/>
                        <span className='px-2'>Seguimiento inicios de sesión.</span>
                    </button>
                </div>

            </td>
        </tr>
    )
}

export default Usuario