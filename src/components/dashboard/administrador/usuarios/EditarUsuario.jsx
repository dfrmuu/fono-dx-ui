import axios from 'axios'
import { ErrorMessage, Field, Form, Formik } from 'formik'
import {useState,useEffect} from 'react'
import Modal from 'react-responsive-modal'
import { toast } from 'react-toastify'
import Cookies from 'universal-cookie'
import * as Yup from 'yup'

//Componente para la edición de un usuario
const EditarUsuario = ({abierto,handleModal,id,traerUsuarios}) => {

    //Variables para los datos del formulario
    const [ci,setCi] = useState("")
    const [correo,setCorreo] = useState("")
    const [nombre,setNombre] = useState("")
    const [apellido,setApellido] = useState("")
    const [celular,setCelular] = useState("")
    const [ciudades,setCiudades] = useState([])
    const [ciudad,setCiudad] = useState("")
    const [direccion,setDireccion] = useState("")
    const [cargo,setCargo] = useState("")
    const [area,setArea] = useState("")

    const [rol,setRol] = useState("")

    //Datos Paciente (cuando sea rol paciente)
    const [edad,setEdad] = useState("")
    const [familiar,setFamiliar] = useState("")
    const [telefonoFamiliar,setTelefonoFamiliar] = useState("")

    //Datos Paciente (cuando sea rol paciente)
    const cookies = new Cookies()

    //Almacena los datos del usuario a traves del API
    const [usuarioAPI,setUsuarioAPI] = useState([])

    //Datos del API 
    const VITE_API_LINK = import.meta.env.VITE_API_LINK

    //Recupera las ciudades y los datos del usuario
    useEffect(() => {
        consultaCiudades()
        traerUsuario()
    }, [])
    
    //Según los datos del usuario cambia el valor de las variables
    useEffect(() => {

        setCi(usuarioAPI.ci)
        setCorreo(usuarioAPI.email)
        setNombre(usuarioAPI.name)
        setApellido(usuarioAPI.last_name)
        setCelular(usuarioAPI.phone)
        if(usuarioAPI.city){
            setCiudad(usuarioAPI.city)
        }
        setDireccion(usuarioAPI.address)
        setCargo(usuarioAPI.job_title)
        setArea(usuarioAPI.area)
        setRol(usuarioAPI.RoleID)


        if(usuarioAPI.RoleID == 1){
            setEdad(usuarioAPI.age)
            setFamiliar(usuarioAPI.familiar)
            setTelefonoFamiliar(usuarioAPI.familiar_phone)
        }

    }, [usuarioAPI])
    

    //Esctructura valida para el usuario
    const usuarioScheme = Yup.object().shape({

        ci : Yup.string()
                .required("Digite  el documento del usuario"),

        email : Yup.string()
                    .email("Por favor escriba un correo valido")
                    .required("El correo es requerido"),

        name : Yup.string()
                    .required("El nombre es requerido"),

        last_name : Yup.string()
                       .required("Los apellidos son requeridos"),

        city : Yup.string()
                     .required("La ciudad es requerida"),

        address : Yup.string()
                     .required("La dirección es requerida"),

        phone : Yup.string()
                    .required("El teléfono es requerido"),

        job_title : Yup.string(),
    
        area : Yup.string(),

        age : Yup.string(),

        familiar : Yup.string(),

        familiar_phone : Yup.string()

    })

    //Handle para la digitación de datos
    const handleCi = (e) => {
        setCi(e.target.value)
    }

    const handleCorreo = (e) => {
        setCorreo(e.target.value)
    }

    const handleNombre = (e) => {
        setNombre(e.target.value)
    }

    const handleApellido = (e) => {
        setApellido(e.target.value)
    }


    const handleCelular = (e) => {
        setCelular(e.target.value)
    }

    const handleCiudad = (e) => {
        setCiudad(e.target.value)
    }

    const handleDireccion = (e) => {
        setDireccion(e.target.value)
    }

    const handleCargo = (e) => {
        setCargo(e.target.value)
    }

    const handleArea = (e) => {
        setArea(e.target.value)
    }

    //Handle datos paciente

    const handleEdad = (e) => {
        setEdad(e.target.value)
    }

    const handleFamiliar = (e) => {
        setFamiliar(e.target.value)
    }

    const handleTelefonoFamiliar = (e) => {
        setTelefonoFamiliar(e.target.value)
    }

    //Consulta las ciudades de un API externa
    const consultaCiudades = async() => {
        try {
            
            await axios.get("https://www.datos.gov.co/resource/xdk5-pm3f.json")
                       .then((response) => {
                            const ciudades = response.data.map((ciudad) => ciudad.municipio)
                            setCiudades(ciudades)
                       }).catch((error) => {
                            console.log(error)
                       })

        } catch (error) {
            console.log(error)
        }
    }


    //Consulta al usuario en el API y lo trae
    const traerUsuario = async () => {
        try {

            const tokenAcceso = cookies.get("token")

            await axios.get(VITE_API_LINK + `usuarios/${id}`,{
                headers: {
                    Authorization: `Bearer ${tokenAcceso}`,
                    ClientAuth : import.meta.env.VITE_APIKEY,
                    Resource : 'usuarios',
                    Action : 'listar'
                },
                timeout : 10000
            }).then((response) => {
                setUsuarioAPI(response.data)
            }).catch((error) => {
                toast.error(error.response.data.Error)
            })
        } catch (error) {
            console.log(error)
        }
    }

    //Envia los datos para edición
    const handleSubmit = async (values) => {
        try {

            const tokenAcceso = cookies.get("token")
            const {ci,job_title,area,phone,email,city,name,last_name} = values


            if(rol != 1 ){
                if(area == "" || cargo == ""){
                    toast.error("Dejaste el cargo / area vacios, no pueden quedar vacios.")
                }
            } 


            await axios.put(VITE_API_LINK + "usuarios/editar",{
                id : id,
                ci : ci,
                job_title : job_title,
                phone : phone,
                email : email,
                name : name,
                last_name : last_name,
                area : area,
                city : city,
                age : edad,
                familiar : familiar,
                familiar_phone : telefonoFamiliar
            },{
                headers: {
                    Authorization: `Bearer ${tokenAcceso}`,
                    ClientAuth : import.meta.env.VITE_APIKEY,
                    Resource : 'usuarios',
                    Action : 'editar'
                },
                timeout : 10000
            })
            .then((response) => {
                toast.info(response.data.Message)
                traerUsuarios()
            }).catch((error) => {
                console.log(error)
                toast.error(error.response.data.Error)
            })
        } catch (error) {
            console.log(error)
        }
    }

    return (
        <Modal open={abierto}
               onClose={handleModal}
               classNames={{
                modal : 'w-75 rounded mt-5 mid-grey-handbook'
               }}
               animationDuration={500}
               center>
            
            <Formik validationSchema={usuarioScheme}
                    enableReinitialize={true}
                    initialValues={{
                        ci : ci,
                        email : correo,
                        name : nombre,
                        last_name : apellido,
                        phone : celular,
                        city : ciudad,
                        address : direccion,
                        job_title : cargo,
                        area : area,
                        age : edad,
                        familiar : familiar,
                        familiar_phone : telefonoFamiliar
                    }}
                    onSubmit={async(values) => {
                        handleSubmit(values)
                    }}>
                <Form>

                    <h2 className='fw-bold text-uppercase text-center'>Editar usuario</h2>
                    <hr/>

                    <div className='row'>
                        <div className='col-md-12'>
                            <label className='form-label fw-bold'>Documento: </label>
                            <Field type="text" className='form-control shadow-sm p-3 mb-2' id="ci" name="ci" onKeyUp={handleCi} placeholder="Escriba el documento"/>
                            <ErrorMessage className='dark-grey-handbook mt-2 mb-2 p-2 text-white rounded text-center fw-bold' component="div" id="ci" name="ci"/>  
                        </div>
                    </div>


                    <br/>

                    <div className='row'>
                        <div className='col-md-6'>
                            <label className='form-label fw-bold'>Correo: </label>
                            <Field type="text" className='form-control shadow-sm p-3 mb-2' id="email" name="email" onKeyUp={handleCorreo} placeholder="Escriba el correo"/>
                            <ErrorMessage className='bg-danger p-2 text-white rounded text-center fw-bold' component="div" id="email" name="email"/>  
                        </div>
                        <div className='col-md-6'>
                            <label className='form-label fw-bold'>Número telefonico: </label>
                            <Field type="text" className='form-control shadow-sm p-3' id="phone" name="phone" onKeyUp={handleCelular} placeholder="Escriba su teléfono"/>
                            <ErrorMessage className='dark-grey-handbook mt-2 mb-2 p-2 text-white rounded text-center fw-bold' component="div" id="phone" name="phone"/>  
                        </div>
                    </div>

                    <br/>


                    <div className='row'>
                        <div className='col-md-6'>
                            <label className='form-label fw-bold'>Nombres: </label>
                            <Field type="text" className='form-control shadow-sm p-3 mb-2' id="name" name="name" onKeyUp={handleNombre} placeholder="Escriba el/los nombres"/>
                            <ErrorMessage className='dark-grey-handbook mt-2 mb-2 p-2 text-white rounded text-center fw-bold' component="div" id="name" name="name"/>
                        </div>
                        <div className='col-md-6'>
                            <label className='form-label fw-bold'>Apellidos: </label>
                            <Field type="text" className='form-control shadow-sm p-3' id="last_name" name="last_name" onKeyUp={handleApellido} placeholder="Escriba el/los apellidos"/>
                            <ErrorMessage className='dark-grey-handbook mt-2 mb-2 p-2 text-white rounded text-center fw-bold' component="div" id="last_name" name="last_name"/>  
                        </div>
                    </div>


                    <br/>


                    <div className='row'>
                        <div className='col-md-6'>
                            <label className='form-label fw-bold mt-1' htmlFor='ciudad'> Ciudad:</label>
                            <Field as="select" className='form-select w-100 shadow-sm p-3 mb-2' name="ciudad" id="ciudad" 
                                    value ={ciudad} onChange={handleCiudad}>
                                    <option value="">Seleccione..</option>
                                    {ciudades.map((ciudad) => (
                                        <option value={ciudad} >
                                            {ciudad}
                                        </option>
                                    ))}
                            </Field>
                            <ErrorMessage name='ciudad' component='div' className='dark-grey-handbook mt-2 mb-2 p-2 text-white rounded text-center fw-bold'/>
                        </div>
                        <div className='col-md-6'>
                            <label className='form-label fw-bold'>Dirección: </label>
                            <Field type="text" className='form-control shadow-sm p-3 mb-2' id="address" name="address" onKeyUp={handleDireccion} placeholder="Escriba su dirección"/>
                            <ErrorMessage className='bg-danger p-2 text-white rounded text-center fw-bold' component="div" id="address" name="address"/>
                        </div>
                    </div>

                    <br/>
                    <hr/>

                    {rol == 1 ? (
                        <>
                            <div className='row'>
                                <div className='col-md-6'>
                                    <label className='form-label fw-bold'>Edad: </label>
                                    <Field type="text" className='form-control shadow-sm p-3 mb-2' id="age" name="age" onKeyUp={handleEdad} placeholder="Escriba la edad"/>
                                    <ErrorMessage className='dark-grey-handbook mt-2 mb-2 p-2 text-white rounded text-center fw-bold' component="div" id="age" name="age"/>
                                </div>
                                <div className='col-md-6'>
                                    <label className='form-label fw-bold'>Familiar: </label>
                                    <Field type="text" className='form-control shadow-sm p-3 mb-2' id="familiar" name="familiar" onKeyUp={handleFamiliar} placeholder="Escriba el familiar"/>
                                    <ErrorMessage className='dark-grey-handbook mt-2 mb-2 p-2 text-white rounded text-center fw-bold' component="div" id="familiar" name="familiar"/>
                                </div>
                            </div> 

                            <br/>

                            <div className='row'>
                                <div className='col-md-12'>
                                    <label className='form-label fw-bold'>Teléfono del familiar: </label>
                                    <Field type="text" className='form-control shadow-sm p-3 mb-2' id="familiar_phone" name="familiar_phone" onKeyUp={handleTelefonoFamiliar} placeholder="Escriba el teléfono del familiar"/>
                                    <ErrorMessage className='dark-grey-handbook mt-2 mb-2 p-2 text-white rounded text-center fw-bold' component="div" id="familiar_phone" name="familiar_phone"/>
                                </div>
                            </div> 

                        </>
                    ) : (
                        <>
                            <div className='row'>
                                <div className='col-md-6'>
                                    <label className='form-label fw-bold'>Cargo: </label>
                                    <Field type="text" className='form-control shadow-sm p-3 mb-2' id="job_title" name="job_title" onKeyUp={handleCargo} placeholder="Escriba el cargo"/>
                                    <ErrorMessage className='dark-grey-handbook mt-2 mb-2 p-2 text-white rounded text-center fw-bold' component="div" id="job_title" name="cargo"/>
                                </div>
                                <div className='col-md-6'>
                                    <label className='form-label fw-bold'>Area: </label>
                                    <Field type="text" className='form-control shadow-sm p-3 mb-2' id="area" name="area" onKeyUp={handleArea} placeholder="Escriba el area"/>
                                    <ErrorMessage className='dark-grey-handbook mt-2 mb-2 p-2 text-white rounded text-center fw-bold' component="div" id="area" name="cargo"/>
                                </div>
                            </div>
                        </>
                    )}

                    
                    <br/>

                    <div className='row'>
                        <div className='col-md-12'>
                            <button className='btn dark-red-handbook text-white w-100 p-3' type='submit'> 
                                Editar usuario
                            </button>
                        </div>
                    </div>
                </Form>
            </Formik>

        </Modal>
    )
}

export default EditarUsuario