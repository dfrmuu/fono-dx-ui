import React from 'react'
import { FaKey, FaKeybase } from 'react-icons/fa'
import { useOutletContext } from 'react-router-dom'

//Componente para renderizar los roles del sistema
const Rol = ({datosRol,verPermisosRol}) => {

    //Recupera los datos del rol
    const {id, name, createdAt} = datosRol

    //Recupera datos del usuario
    const [user,rol,permisos] = useOutletContext()

    return (
        <tr className='light-grey-handbook'>
            <td className='align-middle fw-bold text-uppercase tall-cell'>
                {name} 
            </td>
            <td className='align-middle tall-cell'>
                {createdAt.replace('T',' ').split('.')[0]}
            </td>
            <td className='tall-cell'>
                <div className='col-md-12'>
                    <button className='dark-red-handbook btn shadow p-3 mt-1 text-white' onClick={() => verPermisosRol(id)}>
                        <FaKey/>
                        <span className='px-2'>Ver permisos rol</span>
                    </button>
                </div>
            </td>
        </tr>
    )
}

export default Rol