import {useState,useEffect} from 'react'
import { FaCheck, FaFileInvoice, FaGhost, FaHeart, FaHeartbeat, FaKey, FaKeybase, FaPaperPlane, FaSignal, FaTimes, FaUser, FaUserClock, FaUserNurse, FaUsers, FaVest } from 'react-icons/fa'
import Modal from 'react-responsive-modal'
import Cookies from 'universal-cookie'
import SpinnerHeart from '../../../SpinnerHeart'
import axios from 'axios'
import { toast } from 'react-toastify'
import { useOutletContext } from 'react-router-dom'

//Renderiza los diferentes permisos del aplicativo
const ModalPermisos = ({abierto,handleModal,idRol}) => {

    //Variable para almacenar el rol que esta presente
    const [rolVisible,setRolVisible] = useState("")

    //Variable spinner de carga
    const [cargando,setCargando] = useState(false)

    //Recupera datos del usuario
    const [user,rol,permisos,setPermisos] = useOutletContext()

    //PERMISOS
    const CREAR = "crear"
    const EDITAR = "editar"
    const ELIMINAR = "eliminar"
    const LISTAR = "listar"
    const ENVIAR = "enviar"

    //RECURSOS
    const LOGS = "logs"
    const INVITACIONES = "invitaciones"
    const USUARIOS = "usuarios"
    const PACIENTES = "pacientes"
    const PERMISOS = "permisos"
    const AUSCULTACIONES = "auscultacion"
    const MISAUSCULTACIONES = "mis-auscultaciones"
    const ROLES = "roles"
    const MIUSUARIO = "mi-usuario"

    const [permisosUsuarios,setPermisosUsuarios] = useState([])
    const VITE_API_LINK = import.meta.env.VITE_API_LINK
    const cookies = new Cookies()

    useEffect(() => {
        revisarRol(idRol)
        verPermisosRol(idRol)
    }, [])
    
    //Revisa los datos del rol
    const revisarRol = (id) => {
        switch (id) {
            case 1:
                setRolVisible("Paciente")
            break;

            case 2:
                setRolVisible("Medico")
            break;
        
            case 3:
                setRolVisible("Investigador")
            break;
        

            case 4:
                setRolVisible("Administrativo")
            break;
        }
    }


    //Consulta en el api que permisos tiene ese rol
    const verPermisosRol = async(id) => {
        try {

            setCargando(true)
            const tokenAcceso = cookies.get("token")

            await axios.get(VITE_API_LINK + `roles/permisos/${id}`,{
                headers: {
                    Authorization: `Bearer ${tokenAcceso}`,
                    ClientAuth : import.meta.env.VITE_APIKEY
                },
                timeout : 3000
            })
            .then((response) => {
                console.log(response.data)
                setPermisosUsuarios(response.data)
                setPermisos(response.data)
            }).catch((error) => {
                setPermisosUsuarios([])
            }).finally(() => {
                setTimeout(() => {
                    setCargando(false) 
                }, 500);
            })

        } catch (error) {
            console.log(error)
        }
    }

    //Añade un nuevo permiso
    const agregarPermiso = async(recurso,accion) => {
        try {

            const tokenAcceso = cookies.get("token")

            await axios.post(VITE_API_LINK + `roles/agregar/permiso`,{
                  rol : idRol,
                  resource: recurso,
                  action: accion
            },{
                headers: {
                    Authorization: `Bearer ${tokenAcceso}`,
                    ClientAuth : import.meta.env.VITE_APIKEY,
                    Resource : PERMISOS,
                    Action : CREAR
                },
                timeout : 10000
            })
            .then((response) => {
                toast.info(response.data.Message)
                verPermisosRol(idRol)
            }).catch((error) => {
                toast.error(error.response.data.Error)
            })

        } catch (error) {
            console.log(error)
        }
    }

    //Quita un permiso al rol
    const quitarPermiso = async(recurso,accion) => {
        try {

            const tokenAcceso = cookies.get("token")

            await axios.post(VITE_API_LINK + `roles/quitar/permiso`,{
                  rol : idRol,
                  resource: recurso,
                  action: accion
            },{
                headers: {
                    Authorization: `Bearer ${tokenAcceso}`,
                    ClientAuth : import.meta.env.VITE_APIKEY,
                    Resource : PERMISOS,
                    Action : ELIMINAR
                },
                timeout : 10000
            })
            .then((response) => {
                toast.info(response.data.Message)
                verPermisosRol(idRol)
            }).catch((error) => {
                toast.error(error.response.data.Error)
            })

        } catch (error) {
            console.log(error)
        }
    }


    return (
        <>
            {cargando == true ? (
                <SpinnerHeart height={500} width={500}/>
            ) : (

                <Modal  open={abierto}
                        onClose={handleModal}
                        classNames={{
                            modal : 'w-75 rounded light-grey-handbook'
                        }}
                        animationDuration={500}
                        center>

                                            
                        <h2 className='fw-bold text-uppercase text-center'>Lista permisos rol</h2>

                        <hr/>

                        <p className='text-center'>
                            <span className='px-2 fw-bold'>Mostrando permisos del rol: </span>
                            <span className='p-2 shadow text-white dark-grey-handbook rounded'>{rolVisible}</span>
                        </p>

                        <hr/>
                    
                        <div className='mt-2 table-responsive'>
                                <table className="table text-center m-0" id='data-table'>
                                    <thead>
                                        <tr>
                                            <th scope="col">Recurso</th>
                                            <th scope='col'>Listar</th>
                                            <th scope="col">Crear</th>
                                            <th scope="col">Editar</th> 
                                            <th scope="col">Eliminar</th>
                                            {idRol == 2 ? (
                                                <th scope="col">Enviar</th>
                                            ) : (
                                                null
                                            )} 
                                        </tr>
                                    </thead>                                
                                    <tbody>
                                        {idRol == 1 ? (
                                                <tr> {/* MIS AUSCULTACIONES*/}
                                                <td className='fw-bold align-middle mid-grey-handbook'>
                                                    <FaHeart/>
                                                    <span className='px-2'>Mis auscultaciones</span>
                                                </td>
                                                <td className='fw-bold align-middle'>
                                                    {permisosUsuarios.some(permiso => permiso.resource === MISAUSCULTACIONES && permiso.action === LISTAR) ? (
                                                        <button className='btn dark-grey-handbook  shadow rounded-circle text-white' onClick={() => quitarPermiso(MISAUSCULTACIONES,LISTAR)}>
                                                            <FaCheck/>
                                                        </button>
                                                    ) : (
                                                        <button className='btn dark-red-handbook  shadow rounded-circle text-white' onClick={() => agregarPermiso(MISAUSCULTACIONES,LISTAR)}>
                                                            <FaTimes/>
                                                        </button>
                                                    )}
                                                </td>
                                                <td className='fw-bold align-middle'>
                                                    <button className='btn bg-secondary shadow rounded-circle text-white' disabled>
                                                        <FaGhost/>
                                                    </button>
                                                </td>
                                                <td className='fw-bold align-middle'>
                                                    <button className='btn bg-secondary shadow rounded-circle text-white' disabled>
                                                        <FaGhost/>
                                                    </button>
                                                </td>
                                                <td className='fw-bold align-middle'>
                                                    <button className='btn bg-secondary shadow rounded-circle text-white' disabled>
                                                        <FaGhost/>
                                                    </button>
                                                </td>
                                                </tr>
                                        ) : (
                                            null
                                        )}

                                        {idRol == 2 ? (
                                            <>
                                            <tr> {/* SEÑALES*/}
                                            <td className='fw-bold align-middle  mid-grey-handbook'>
                                                <FaHeartbeat/>
                                                <span className='px-2'>Señales (auscultaciones de referencia)</span>
                                            </td>
                                            <td className='fw-bold align-middle'>
                                                {permisosUsuarios.some(permiso => permiso.resource === AUSCULTACIONES && permiso.action === LISTAR) ? (
                                                    <button className='btn dark-grey-handbook  shadow rounded-circle text-white' onClick={() => quitarPermiso(AUSCULTACIONES,LISTAR)}>
                                                        <FaCheck/>
                                                    </button>
                                                ) : (
                                                    <button className='btn dark-red-handbook  shadow rounded-circle text-white' onClick={() => agregarPermiso(AUSCULTACIONES,LISTAR)}>
                                                        <FaTimes/>
                                                    </button>
                                                )}
                                            </td>
                                            <td className='fw-bold align-middle'>
                                                {permisosUsuarios.some(permiso => permiso.resource === AUSCULTACIONES && permiso.action === CREAR) ? (
                                                    <button className='btn dark-grey-handbook  shadow rounded-circle text-white' onClick={() => quitarPermiso(AUSCULTACIONES,CREAR)}>
                                                        <FaCheck/>
                                                    </button>
                                                ) : (
                                                    <button className='btn dark-red-handbook  shadow rounded-circle text-white' onClick={() => agregarPermiso(AUSCULTACIONES,CREAR)}>
                                                        <FaTimes/>
                                                    </button>
                                                )}
                                            </td>
                                            <td className='fw-bold align-middle'>
                                                {permisosUsuarios.some(permiso => permiso.resource === AUSCULTACIONES && permiso.action === EDITAR) ? (
                                                    <button className='btn dark-grey-handbook shadow rounded-circle text-white' onClick={() => quitarPermiso(AUSCULTACIONES,EDITAR)}>
                                                        <FaCheck/>
                                                    </button>
                                                ) : (
                                                    <button className='btn dark-red-handbook  shadow rounded-circle text-white' onClick={() => agregarPermiso(AUSCULTACIONES,EDITAR)}>
                                                        <FaTimes/>
                                                    </button>
                                                )}
                                            </td>
                                            <td className='fw-bold align-middle'>
                                                {permisosUsuarios.some(permiso => permiso.resource === AUSCULTACIONES && permiso.action === ELIMINAR) ? (
                                                    <button className='btn dark-grey-handbook shadow rounded-circle text-white' onClick={() => quitarPermiso(AUSCULTACIONES,ELIMINAR)}>
                                                        <FaCheck/>
                                                    </button>
                                                ) : (
                                                    <button className='btn dark-red-handbook  shadow rounded-circle text-white' onClick={() => agregarPermiso(AUSCULTACIONES,ELIMINAR)}>
                                                        <FaTimes/>
                                                    </button>
                                                )}
                                            </td>
                                            <td className='fw-bold align-middle'>
                                                <button className='btn bg-secondary shadow rounded-circle text-white' disabled>
                                                    <FaGhost/>
                                                 </button>
                                            </td>
                                            </tr>
                                            <tr> {/* PACIENTES*/}
                                                <td className='fw-bold align-middle mid-grey-handbook'>
                                                    <FaUserNurse/>
                                                    <span className='px-2'>Pacientes</span>
                                                </td>
                                                <td className='fw-bold align-middle'>
                                                    {permisosUsuarios.some(permiso => permiso.resource === PACIENTES && permiso.action === LISTAR) ? (
                                                        <button className='btn dark-grey-handbook shadow rounded-circle text-white' onClick={() => quitarPermiso(PACIENTES,LISTAR)}>
                                                            <FaCheck/>
                                                        </button>
                                                    ) : (
                                                        <button className='btn dark-red-handbook  shadow rounded-circle text-white' onClick={() => agregarPermiso(PACIENTES,LISTAR)}>
                                                            <FaTimes/>
                                                        </button>
                                                    )}
                                                </td>
                                                <td className='fw-bold align-middle'>
                                                    {permisosUsuarios.some(permiso => permiso.resource === PACIENTES && permiso.action === CREAR) ? (
                                                        <button className='btn dark-grey-handbook  shadow rounded-circle text-white' onClick={() => quitarPermiso(PACIENTES,CREAR)}>
                                                            <FaCheck/>
                                                        </button>
                                                    ) : (
                                                        <button className='btn dark-red-handbook  shadow rounded-circle text-white' onClick={() => agregarPermiso(PACIENTES,CREAR)}>
                                                            <FaTimes/>
                                                        </button>
                                                    )}
                                                </td>
                                                <td className='fw-bold align-middle'>
                                                    {permisosUsuarios.some(permiso => permiso.resource === PACIENTES && permiso.action === EDITAR) ? (
                                                        <button className='btn dark-grey-handbook  shadow rounded-circle text-white' onClick={() => quitarPermiso(PACIENTES,EDITAR)}>
                                                            <FaCheck/>
                                                        </button>
                                                    ) : (
                                                        <button className='btn dark-red-handbook  shadow rounded-circle text-white' onClick={() => agregarPermiso(PACIENTES,EDITAR)}>
                                                            <FaTimes/>
                                                        </button>
                                                    )}
                                                </td>
                                                <td className='fw-bold align-middle'>
                                                    {permisosUsuarios.some(permiso => permiso.resource === PACIENTES && permiso.action === ELIMINAR) ? (
                                                        <button className='btn dark-grey-handbook shadow rounded-circle text-white' onClick={() => quitarPermiso(PACIENTES,ELIMINAR)}>
                                                            <FaCheck/>
                                                        </button>
                                                    ) : (
                                                        <button className='btn dark-red-handbook shadow rounded-circle text-white' onClick={() => agregarPermiso(PACIENTES,ELIMINAR)}>
                                                            <FaTimes/>
                                                        </button>
                                                    )}
                                                </td>
                                                <td className='fw-bold align-middle'>
                                                    <button className='btn bg-secondary shadow rounded-circle text-white' disabled>
                                                        <FaGhost/>
                                                    </button>
                                                </td>
                                                </tr>
                                                <tr> {/* AUSCULTACION*/}
                                                <td className='fw-bold align-middle mid-grey-handbook'>
                                                    <FaHeart/>
                                                    <span className='px-2'>Auscultaciones</span>
                                                </td>
                                                <td className='fw-bold align-middle'>
                                                    {permisosUsuarios.some(permiso => permiso.resource === AUSCULTACIONES && permiso.action === LISTAR) ? (
                                                        <button className='btn dark-grey-handbook  shadow rounded-circle text-white' onClick={() => quitarPermiso(AUSCULTACIONES,LISTAR)}>
                                                            <FaCheck/>
                                                        </button>
                                                    ) : (
                                                        <button className='btn dark-red-handbook  shadow rounded-circle text-white' onClick={() => agregarPermiso(AUSCULTACIONES,LISTAR)}>
                                                            <FaTimes/>
                                                        </button>
                                                    )}
                                                </td>
                                                <td className='fw-bold align-middle'>
                                                    {permisosUsuarios.some(permiso => permiso.resource === AUSCULTACIONES && permiso.action === CREAR) ? (
                                                        <button className='btn dark-grey-handbook  shadow rounded-circle text-white' onClick={() => quitarPermiso(AUSCULTACIONES,CREAR)}>
                                                            <FaCheck/>
                                                        </button>
                                                    ) : (
                                                        <button className='btn  dark-red-handbook  shadow rounded-circle text-white' onClick={() => agregarPermiso(AUSCULTACIONES,CREAR)}>
                                                            <FaTimes/>
                                                        </button>
                                                    )}
                                                </td>
                                                <td className='fw-bold align-middle'>
                                                    {permisosUsuarios.some(permiso => permiso.resource === AUSCULTACIONES && permiso.action === EDITAR) ? (
                                                        <button className='btn dark-grey-handbook  shadow rounded-circle text-white' onClick={() => quitarPermiso(AUSCULTACIONES,EDITAR)}>
                                                            <FaCheck/>
                                                        </button>
                                                    ) : (
                                                        <button className='btn  dark-red-handbook  shadow rounded-circle text-white' onClick={() => agregarPermiso(AUSCULTACIONES,EDITAR)}>
                                                            <FaTimes/>
                                                        </button>
                                                    )}
                                                </td>
                                                <td className='fw-bold align-middle'>
                                                    {permisosUsuarios.some(permiso => permiso.resource === AUSCULTACIONES && permiso.action === ELIMINAR) ? (
                                                        <button className='btn dark-grey-handbook  shadow rounded-circle text-white' onClick={() => quitarPermiso(AUSCULTACIONES,ELIMINAR)}>
                                                            <FaCheck/>
                                                        </button>
                                                    ) : (
                                                        <button className='btn  dark-red-handbook  shadow rounded-circle text-white' onClick={() => agregarPermiso(AUSCULTACIONES,ELIMINAR)}>
                                                            <FaTimes/>
                                                        </button>
                                                    )}
                                                </td>
                                                <td className='fw-bold align-middle'>
                                                    {permisosUsuarios.some(permiso => permiso.resource === AUSCULTACIONES && permiso.action === ENVIAR) ? (
                                                        <button className='btn dark-grey-handbook  shadow rounded-circle text-white' onClick={() => quitarPermiso(AUSCULTACIONES,ENVIAR)}>
                                                            <FaCheck/>
                                                        </button>
                                                    ) : (
                                                        <button className='btn  dark-red-handbook  shadow rounded-circle text-white' onClick={() => agregarPermiso(AUSCULTACIONES,ENVIAR)}>
                                                            <FaTimes/>
                                                        </button>
                                                    )}
                                                </td>
                                                </tr>
                                            </>
                                        ) : (
                                            null
                                        )}

                                        
                                        {idRol == 4 || idRol == 3 ? (
                                            <>
                                                {idRol == 4 ? (
                                                    <tr> {/* INVITACIONES*/}
                                                        <td className='fw-bold align-middle mid-grey-handbook'>
                                                            <FaPaperPlane/>
                                                            <span className='px-2'>Invitaciones</span>
                                                        </td>
                                                        <td className='fw-bold align-middle'>
                                                            {permisosUsuarios.some(permiso => permiso.resource === INVITACIONES && permiso.action === LISTAR) ? (
                                                                <button className='btn dark-grey-handbook shadow rounded-circle text-white' onClick={() => quitarPermiso(INVITACIONES,LISTAR)}>
                                                                    <FaCheck/>
                                                                </button>
                                                            ) : (
                                                                <button className='btn  dark-red-handbook shadow rounded-circle text-white' onClick={() => agregarPermiso(INVITACIONES,LISTAR)}>
                                                                    <FaTimes/>
                                                                </button>
                                                            )}
                                                        </td>
                                                        <td className='fw-bold align-middle'>
                                                            {permisosUsuarios.some(permiso => permiso.resource === INVITACIONES && permiso.action === CREAR) ? (
                                                                <button className='btn dark-grey-handbook shadow rounded-circle text-white' onClick={() => quitarPermiso(INVITACIONES,CREAR)}>
                                                                    <FaCheck/>
                                                                </button>
                                                            ) : (
                                                                <button className='btn dark-red-handbook  shadow rounded-circle text-white' onClick={() => agregarPermiso(INVITACIONES,CREAR)}>
                                                                    <FaTimes/>
                                                                </button>
                                                            )}
                                                        </td>
                                                        <td className='fw-bold align-middle'>
                                                            {permisosUsuarios.some(permiso => permiso.resource === INVITACIONES && permiso.action === EDITAR) ? (
                                                                <button className='btn dark-grey-handbook shadow rounded-circle text-white' onClick={() => quitarPermiso(INVITACIONES,EDITAR)}>
                                                                    <FaCheck/>
                                                                </button>
                                                            ) : (
                                                                <button className='btn  dark-red-handbook  shadow rounded-circle text-white' onClick={() => agregarPermiso(INVITACIONES,EDITAR)}>
                                                                    <FaTimes/>
                                                                </button>
                                                            )}
                                                        </td>
                                                        <td className='fw-bold align-middle'>
                                                            {permisosUsuarios.some(permiso => permiso.resource === INVITACIONES && permiso.action === ELIMINAR) ? (
                                                                <button className='btn dark-grey-handbook shadow rounded-circle text-white' onClick={() => quitarPermiso(INVITACIONES,ELIMINAR)}>
                                                                    <FaCheck/>
                                                                </button>
                                                            ) : (
                                                                <button className='btn dark-red-handbook  shadow rounded-circle text-white' onClick={() => agregarPermiso(INVITACIONES,ELIMINAR)}>
                                                                    <FaTimes/>
                                                                </button>
                                                            )}
                                                        </td>
                                                    </tr>
                                                ) : (
                                                    null
                                                )}

                                            <tr> {/* USUARIOS*/}
                                                <td className='fw-bold align-middle mid-grey-handbook'>
                                                    <FaUsers/>
                                                    <span className='px-2'>Usuarios</span>
                                                </td>
                                                <td className='fw-bold align-middle'>
                                                    {permisosUsuarios.some(permiso => permiso.resource === USUARIOS && permiso.action === LISTAR) ? (
                                                        <button className='btn dark-grey-handbook shadow rounded-circle text-white' onClick={() => quitarPermiso(USUARIOS,LISTAR)}>
                                                            <FaCheck/>
                                                        </button>
                                                    ) : (
                                                        <button className='btn dark-red-handbook  shadow rounded-circle text-white' onClick={() => agregarPermiso(USUARIOS,LISTAR)}>
                                                            <FaTimes/>
                                                        </button>
                                                    )}
                                                </td>
                                                <td className='fw-bold align-middle'>
                                                    {permisosUsuarios.some(permiso => permiso.resource === USUARIOS && permiso.action === CREAR) ? (
                                                        <button className='btn dark-grey-handbook shadow rounded-circle text-white' onClick={() => quitarPermiso(USUARIOS,CREAR)}>
                                                            <FaCheck/>
                                                        </button>
                                                    ) : (
                                                        <button className='btn dark-red-handbook  shadow rounded-circle text-white' onClick={() => agregarPermiso(USUARIOS,CREAR)}>
                                                            <FaTimes/>
                                                        </button>
                                                    )}
                                                </td>
                                                <td className='fw-bold align-middle'>
                                                    {permisosUsuarios.some(permiso => permiso.resource === USUARIOS && permiso.action === EDITAR) ? (
                                                        <button className='btn dark-grey-handbook shadow rounded-circle text-white'  onClick={() => quitarPermiso(USUARIOS,EDITAR)}>
                                                            <FaCheck/>
                                                        </button>
                                                    ) : (
                                                        <button className='btn dark-red-handbook  shadow rounded-circle text-white' onClick={() => agregarPermiso(USUARIOS,EDITAR)}>
                                                            <FaTimes/>
                                                        </button>
                                                    )}
                                                </td>
                                                <td className='fw-bold align-middle'>
                                                    {permisosUsuarios.some(permiso => permiso.resource === USUARIOS && permiso.action ===  ELIMINAR) ? (
                                                        <button className='btn dark-grey-handbook shadow rounded-circle text-white' onClick={() => quitarPermiso(USUARIOS,ELIMINAR)}>
                                                            <FaCheck/>
                                                        </button>
                                                    ) : (
                                                        <button className='btn dark-red-handbook  shadow rounded-circle text-white'  onClick={() => agregarPermiso(USUARIOS,ELIMINAR)}>
                                                            <FaTimes/>
                                                        </button>
                                                    )}
                                                </td>
                                            </tr>
                                            <tr>  {/* PERMISOS*/}
                                                <td className='fw-bold align-middle mid-grey-handbook'>
                                                    <FaKey/>
                                                    <span className='px-2'>Permisos</span>
                                                </td>
                                                <td className='fw-bold align-middle'>
                                                    {permisosUsuarios.some(permiso => permiso.resource === PERMISOS && permiso.action === LISTAR) ? (
                                                        <button className='btn dark-grey-handbook shadow rounded-circle text-white'  onClick={() => quitarPermiso(PERMISOS,LISTAR)}>
                                                            <FaCheck/>
                                                        </button>
                                                    ) : (
                                                        <button className='btn dark-red-handbook  shadow rounded-circle text-white' onClick={() => agregarPermiso(PERMISOS,LISTAR)}>
                                                            <FaTimes/>
                                                        </button>
                                                    )}
                                                </td>
                                                <td className='fw-bold align-middle'>
                                                    {permisosUsuarios.some(permiso => permiso.resource === PERMISOS && permiso.action === CREAR) ? (
                                                        <button className='btn dark-grey-handbook shadow rounded-circle text-white' onClick={() => quitarPermiso(PERMISOS,CREAR)}>
                                                            <FaCheck/>
                                                        </button>
                                                    ) : (
                                                        <button className='btn dark-red-handbook  shadow rounded-circle text-white' onClick={() => agregarPermiso(PERMISOS,CREAR)}>
                                                            <FaTimes/>
                                                        </button>
                                                    )}
                                                </td>
                                                <td className='fw-bold align-middle'>
                                                    <button className='btn bg-secondary shadow rounded-circle text-white' disabled>
                                                        <FaGhost/>
                                                    </button>
                                                </td>
                                                <td className='fw-bold align-middle'>
                                                    {permisosUsuarios.some(permiso => permiso.resource === PERMISOS && permiso.action === ELIMINAR) ? (
                                                        <button className='btn dark-grey-handbook shadow rounded-circle text-white' onClick={() => quitarPermiso(PERMISOS,ELIMINAR)}>
                                                            <FaCheck/>
                                                        </button>
                                                    ) : (
                                                        <button className='btn dark-red-handbook shadow rounded-circle text-white' onClick={() => agregarPermiso(PERMISOS,ELIMINAR)}>
                                                            <FaTimes/>
                                                        </button>
                                                    )}
                                                </td>
                                            </tr>
                                            <tr> {/* ROLES*/}
                                                <td className='fw-bold align-middle mid-grey-handbook'>
                                                    <FaVest/>
                                                    <span className='px-2'>Roles</span>
                                                </td>
                                                <td className='fw-bold align-middle'>
                                                    {permisosUsuarios.some(permiso => permiso.resource === ROLES && permiso.action === LISTAR) ? (
                                                        <button className='btn dark-grey-handbook  shadow rounded-circle text-white' onClick={() => quitarPermiso(ROLES,LISTAR)}>
                                                            <FaCheck/>
                                                        </button>
                                                    ) : (
                                                        <button className='btn dark-red-handbook shadow rounded-circle text-white' onClick={() => agregarPermiso(ROLES,LISTAR)}>
                                                            <FaTimes/>
                                                        </button>
                                                    )}
                                                </td>
                                                <td className='fw-bold align-middle'>
                                                    <button className='btn bg-secondary shadow rounded-circle text-white' disabled>
                                                        <FaGhost/>
                                                    </button>
                                                </td>
                                                <td className='fw-bold align-middle'>
                                                    <button className='btn bg-secondary shadow rounded-circle text-white' disabled>
                                                        <FaGhost/>
                                                    </button>
                                                </td>
                                                <td className='fw-bold align-middle'>
                                                    <button className='btn bg-secondary shadow rounded-circle text-white' disabled>
                                                        <FaGhost/>
                                                    </button>
                                                </td>
                                            </tr>
                                            <tr> {/* LOGS*/}
                                                <td className='fw-bold align-middle mid-grey-handbook'>
                                                    <FaFileInvoice/>
                                                    <span className='px-2'>Logs</span>
                                                </td>
                                                <td className='fw-bold align-middle'>
                                                    {permisosUsuarios.some(permiso => permiso.resource === LOGS && permiso.action === LISTAR) ? (
                                                        <button className='btn dark-grey-handbook  shadow rounded-circle text-white' onClick={() => quitarPermiso(LOGS,LISTAR)}>
                                                            <FaCheck/>
                                                        </button>
                                                    ) : (
                                                        <button className='btn  dark-red-handbook shadow rounded-circle text-white' onClick={() => agregarPermiso(LOGS,LISTAR)}>
                                                            <FaTimes/>
                                                        </button>
                                                    )}
                                                </td>
                                                <td className='fw-bold align-middle'>
                                                    <button className='btn bg-secondary shadow rounded-circle text-white' disabled>
                                                        <FaGhost/>
                                                    </button>
                                                </td>
                                                <td className='fw-bold align-middle'>
                                                    <button className='btn bg-secondary shadow rounded-circle text-white' disabled>
                                                        <FaGhost/>
                                                    </button>
                                                </td>
                                                <td className='fw-bold align-middle'>
                                                    <button className='btn bg-secondary shadow rounded-circle text-white' disabled>
                                                        <FaGhost/>
                                                    </button>
                                                </td>
                                            </tr>                                               
                                            </>
                                        ) : (
                                            null
                                        )}

                                        <tr> {/* MI-USUARIO*/}
                                             <td className='fw-bold align-middle mid-grey-handbook'>
                                                 <FaUser/>
                                                <span className='px-2'>Mi usuario</span>
                                            </td>
                                            <td className='fw-bold align-middle'>
                                                {permisosUsuarios.some(permiso => permiso.resource === MIUSUARIO && permiso.action === LISTAR) ? (
                                                    <button className='btn dark-grey-handbook  shadow rounded-circle text-white' onClick={() => quitarPermiso(MIUSUARIO,LISTAR)}>
                                                        <FaCheck/>
                                                    </button>
                                                ) : (
                                                    <button className='btn  dark-red-handbook shadow rounded-circle text-white' onClick={() => agregarPermiso(MIUSUARIO,LISTAR)}>
                                                        <FaTimes/>
                                                    </button>
                                                )}
                                            </td>
                                            <td className='fw-bold align-middle'>
                                                <button className='btn bg-secondary shadow rounded-circle text-white' disabled>
                                                    <FaGhost/>
                                                </button>
                                            </td>
                                            <td className='fw-bold align-middle'>
                                                {permisosUsuarios.some(permiso => permiso.resource === MIUSUARIO && permiso.action === EDITAR) ? (
                                                    <button className='btn dark-grey-handbook  shadow rounded-circle text-white' onClick={() => quitarPermiso(MIUSUARIO,EDITAR)}>
                                                        <FaCheck/>
                                                    </button>
                                                ) : (
                                                    <button className='btn  dark-red-handbook shadow rounded-circle text-white' onClick={() => agregarPermiso(MIUSUARIO,EDITAR)}>
                                                        <FaTimes/>
                                                    </button>
                                                )}
                                            </td>
                                            <td className='fw-bold align-middle'>
                                                <button className='btn bg-secondary shadow rounded-circle text-white' disabled>
                                                    <FaGhost/>
                                                </button>
                                             </td>
                                        </tr> 

                                    </tbody>                         
                                </table> 
                        </div>

                </Modal>
            )}
    
        </>

    )
}

export default ModalPermisos