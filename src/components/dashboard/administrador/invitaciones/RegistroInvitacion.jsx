import {useState,useEffect} from 'react'
import { useNavigate, useOutletContext } from 'react-router-dom'
import { Form, Field, Formik, ErrorMessage } from 'formik'
import { DateTime } from 'luxon'
import { toast } from 'react-toastify'
import { FaBackward } from 'react-icons/fa'
import Cookies from 'universal-cookie';
import * as Yup from 'yup'
import axios from 'axios'
import Modal from 'react-responsive-modal'

//Componente para realizar el envio / registro de una invitación
const RegistroInvitacion = ({abierto, handleCerrarModal, traerInvitaciones}) => {

    //Variables para los datos de la invitación
    const [correo,setCorreo] = useState("")
    const [tipoUsuario,setTipoUsuario] = useState("")

    //Fecha actual
    const [dateTime, setDateTime] = useState(DateTime.local().toFormat('yyyy-MM-dd HH:mm'));

    //Fecha actual más 1 semana (expiración)
    const [dateTimeWeek,setDateTimeWeek] = useState(DateTime.local().plus({week : 1}).toFormat('yyyy-MM-dd HH:mm'))

    //Variable para redireccionar
    const navigate = useNavigate()

    //Recupera las cookies del navegador
    const cookies = new Cookies()
    const VITE_API_LINK = import.meta.env.VITE_API_LINK

    //Envia los datos de la invitación
    const handleSubmit =async (values) => {
        try{
            const tokenAcceso = cookies.get("token")
            const {email,user_type} = values

            await axios.post(VITE_API_LINK + "invitaciones/enviar", {
                email,
                user_type,
            },{
                headers : {
                    Authorization: `Bearer ${tokenAcceso}`,
                    ClientAuth : import.meta.env.VITE_APIKEY,
                    Resource : 'invitaciones',
                    Action : 'crear' 
                },
                timeout : 10000
            }).then((response) => {
                toast.info(response.data.Message)
                reiniciarFormulario()
                traerInvitaciones()
            }).catch((error) => {
                console.log(error)
                toast.error(error.response.data.Error)
            })

        }catch(error){
            console.log(error)
        }

    }


    //Reinicia el formulario de consulta de la invitación
    const reiniciarFormulario = () => {
        setCorreo("")
        setTipoUsuario("")
    }

    //Handle para la digitación de datos de los campos
    const handleCorreoChange = (e) => {
        setCorreo(e.target.value)
    }

    const handleTipoUsuarioChange = (e) => {
        setTipoUsuario(e.target.value)
    }

    //Esctructura correcta para una invitación
    const InvitacionScheme = Yup.object().shape({
        email :  Yup.string()
                    .email('Ingrese un correo electrónico válido')
                    .required('Ingrese un correo electrónico'),

        user_type : Yup.number()
                          .required("El tipo de usuario es requerido")
                          .oneOf([1,2,3,4], "Este valor no es aceptado"),
    })


    return (
        <Modal open={abierto}
               onClose={handleCerrarModal}
                classNames={{
                    modal: `rounded mid-grey-handbook`
                }}
               closeOnOverlayClick={false} 
               animationDuration={400} center>

            

            <h3 className='text-center fw-bold mt-3'> Registro de invitación</h3>
            <p className='fs-5 text-center mt-1'>Invite a participar a alguien de la fundación en el proyecto, solamente colocando el correo institucional.</p>

            <hr/>

            <Formik enableReinitialize={true}
                    initialValues = {{
                        email : correo,
                        user_type : tipoUsuario,
                        expirationDate : dateTimeWeek,
                        fecha_creacion : dateTime
                    }}
                    validationSchema = {InvitacionScheme}
                    onSubmit = {async(values) => handleSubmit(values)}>
                    
                <Form className='mt-3'>
                    <div className='row'> 
                        <div className="col-md-12">
                            <label className='form-label fw-bold mt-2' htmlFor='correo'> Correo del usuario:</label>
                            <Field type="text" className='form-control shadow-sm p-3' id="email" name="email" placeholder='Ingrese el correo'
                                    onKeyUp={handleCorreoChange}/>
                            <ErrorMessage name='email' component='div' className='dark-grey-handbook mt-2 mb-2  p-2 text-white rounded text-center fw-bold'/>
                        </div>
                        <div className="col-md-12">
                            <label className='form-label fw-bold mt-2' htmlFor='tipo_usuario'> Tipo de usuario en la invitación:</label>
                            <Field as='select' className='shadow-sm w-100 form-control p-3' id="user_type" name="user_type" value={tipoUsuario} onChange = {handleTipoUsuarioChange}>
                                <option value="">Seleccione..</option>
                                {/* <option value="1"> Paciente</option> */}
                                <option value="3"> Investigador</option>
                                <option value="2"> Medico</option>
                                <option value="4"> Administrador</option>
                            </Field>
                            <ErrorMessage name='user_type' component='div' className='dark-grey-handbook mt-2 mb-2  p-2 text-white rounded text-center fw-bold'/>
                        </div>  
                        </div> 

                        <br/>

                        <div className='row'>
                            <div className="col-md-12">
                                <label className='form-label fw-bold mt-1' htmlFor='fecha_creacion'> Fecha de creación:</label>
                                <Field type="text" className='form-control shadow-sm p-3' id="fecha_creacion" name="fecha_creacion" disabled/>
                                <ErrorMessage name='fecha_creacion' component='div' className='dark-grey-handbook mt-2 mb-2  p-2 text-white rounded text-center fw-bold'/>
                            </div>                            
                        </div>

                        <br/>

                        <div className='row'>
                            <div className="col-md-12">
                                <label className='form-label fw-bold mt-1' htmlFor='expirationDate'> Fecha de caducidad de la invitación:</label>
                                <Field type="text" className='form-control shadow-sm p-3' id="expirationDate" name="expirationDate" disabled/>
                                <ErrorMessage name='expirationDate' component='div' className='dark-grey-handbook mt-2 mb-2  p-2 text-white rounded text-center fw-bold'/>
                            </div>                            
                        </div>

                        <br/>

                        <div className='row'>
                            <div className="col-md-12">
                                <label className='form-label fw-bold mt-1' htmlFor='encargado'> Quien envia la invitación:</label>
                                <input type="text" className='form-control shadow-sm p-3' id="encargado" name="encargado" defaultValue={"Usted"} disabled/>
                            </div>                            
                        </div>

                        <div className='row mt-5'>
                            <div className='col-md-12'>
                                <button type="submit" className='btn  dark-red-handbook text-white w-100 p-3'>
                                    <i className='text-white px-1 fa fa-send'></i>
                                    Enviar invitación
                                </button>
                            </div>
                        </div>
                </Form>
            </Formik>

        </Modal>
    )
}

export default RegistroInvitacion