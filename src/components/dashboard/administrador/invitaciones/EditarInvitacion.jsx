import {useState,useEffect} from 'react'
import { useNavigate, useOutletContext } from 'react-router-dom'
import { Form, Field, Formik, ErrorMessage } from 'formik'
import { DateTime } from 'luxon'
import { toast } from 'react-toastify'
import { FaBackward } from 'react-icons/fa'
import Cookies from 'universal-cookie';
import * as Yup from 'yup'
import axios from 'axios'
import Modal from 'react-responsive-modal'

//Componente para editar una invitación
const EditarInvitacion = ({abierto, handleCerrarModal, traerInvitaciones, idEditar}) => {

    //Variable para los datos de la invitación
    const [correo,setCorreo] = useState("")
    const [tipoUsuario,setTipoUsuario] = useState("")

    //Variable para redireccionar
    const navigate = useNavigate()

    //Almacena la invitación que se recupera desde el API
    const [invitacionAPI,setInvitacionAPI] = useState([])

    //Datos del API
    const cookies = new Cookies()
    const VITE_API_LINK = import.meta.env.VITE_API_LINK

    //Recupera la invitación
    useEffect(() => {
        traerInvitacion()
    }, [])


    //Cambia los datos a partir de la invitación
    useEffect(() => {
        setCorreo(invitacionAPI.email)
        setTipoUsuario(invitacionAPI.user_type)
    },[invitacionAPI])
    
    
    //Recupera los datos desde la base de datos
    const traerInvitacion = async () => {
        try {
            const tokenAcceso = cookies.get("token")
            await axios.get(VITE_API_LINK + `invitaciones/traer/${idEditar}`,{
                headers : {
                    Authorization: `Bearer ${tokenAcceso}`,
                    ClientAuth : import.meta.env.VITE_APIKEY,
                    Resource : 'invitaciones',
                    Action : 'listar' 
                },
                timeout : 10000
            }).then((response) => {
                setInvitacionAPI(response.data)
            }).catch((error) => {
                toast.error(error.response.data.Error)
            })
        } catch (error) {
            
        }
    }

    //Envia los datos para editarlos
    const handleSubmit =async (values) => {
        try{
            const tokenAcceso = cookies.get("token")
            const {email,user_type} = values

            await axios.put(VITE_API_LINK + "invitaciones/editar", {
                id : idEditar,
                email: email,
                user_type: user_type,
            },{
                headers : {
                    Authorization: `Bearer ${tokenAcceso}`,
                    ClientAuth : import.meta.env.VITE_APIKEY,
                    Resource : 'invitaciones',
                    Action : 'editar' 
                },
                timeout : 20000
            }).then((response) => {
                toast.info(response.data.Message)
                traerInvitaciones()
            }).catch((error) => {
                console.log(error)
                toast.error(error.response.data.Error)
            })

        }catch(error){
            console.log(error)
        }

    }

    //Reinicia los datos
    const reiniciarFormulario = () => {
        setCorreo("")
        setTipoUsuario("")
    }

    //Handle para la digitación de los datos
    const handleCorreoChange = (e) => {
        setCorreo(e.target.value)
    }

    const handleTipoUsuarioChange = (e) => {
        setTipoUsuario(e.target.value)
    }

    //Estructura de la invitación
    const InvitacionScheme = Yup.object().shape({
        email :  Yup.string()
                    .email('Ingrese un correo electrónico válido')
                    .required('Ingrese un correo electrónico'),

        user_type : Yup.number()
                          .required("El tipo de usuario es requerido")
                          .oneOf([1,2,3,4], "Este valor no es aceptado"),
    })


    return (
        <Modal open={abierto}
               onClose={handleCerrarModal}
                classNames={{
                    modal: `rounded mid-grey-handbook`
                }}
               closeOnOverlayClick={false} 
               animationDuration={400} center>

            

            <h3 className='text-center fw-bold mt-3'> Editar invitación</h3>
            <p className='fs-5 text-center mt-4'>Cambie el tipo de usuario de la invitación, en caso de necesitar cambiar el 
                correo digite el nuevo, se enviara el correo de invitación nuevamente.</p>

            <hr/>

            <Formik enableReinitialize={true}
                    initialValues = {{
                        email : correo,
                        user_type : tipoUsuario,
                    }}
                    validationSchema = {InvitacionScheme}
                    onSubmit = {async(values) => handleSubmit(values)}>
                    
                <Form className='mt-3  p-3'>
                    <div className='row'> 
                        <div className="col-md-12">
                            <label className='form-label fw-bold mt-2' htmlFor='correo'> Correo institucional del usuario:</label>
                            <Field type="text" className='form-control shadow-sm p-3' id="email" name="email" placeholder='Ingrese el correo institucional'
                                    onKeyUp={handleCorreoChange}/>
                            <ErrorMessage name='email' component='div' className='dark-grey-handbook mt-2 mb-2 p-2 text-white rounded text-center fw-bold'/>
                        </div>
                    </div> 

                    <br/>

                    <div className='row'>
                        <div className="col-md-12">
                            <label className='form-label fw-bold mt-2' htmlFor='tipo_usuario'> Tipo de usuario en la invitación:</label>
                            <Field as='select' className='shadow-sm w-100 form-control p-3' id="user_type" name="user_type" value={tipoUsuario} onChange = {handleTipoUsuarioChange}>
                                <option value="">Seleccione..</option>
                                {/* <option value="1"> Paciente</option> */}
                                <option value="2"> Medico</option>
                                <option value="3"> Investigador</option>
                                <option value="4"> Administrador</option>
                            </Field>
                            <ErrorMessage name='user_type' component='div' className='dark-grey-handbook mt-2 mb-2 p-2 text-white rounded text-center fw-bold'/>
                        </div>                            
                    </div>

                    <div className='row mt-5'>
                         <div className='col-md-12'>
                             <button type="submit" className='btn dark-red-handbook text-white w-100 p-3'>
                                <i className='text-white px-1 fa fa-send'></i>
                                 Editar invitación
                            </button>
                         </div>
                     </div>
                </Form>
            </Formik>

        </Modal>
    )
}

export default EditarInvitacion