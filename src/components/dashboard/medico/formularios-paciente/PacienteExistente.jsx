import React from 'react'

const PacienteExistente = ({usuario}) => {

    return (
      <div className='mt-3 light-grey-handbook p-3 shadow rounded'>

          <div className='rounded shadow mid-grey-handbook p-3'>
            <div className='rounded border border-secondary'>
              <div className='row'>
                  <div className='col-md-12'>
                    <div className='row'>
                      <div className='col-lg-6 col-md-6 col-sm-12'>
                          <p className='text-center d-flex justify-content-center align-items-center h-100 fw-bold  fs-4'>
                            Datos del paciente
                          </p>
                        </div>
                        <div className='col-lg-6 col-md-6 col-sm-12'>
                          <p className='text-center lg-me-5 md-me-5 sm-me-0 mt-2'>
                            <img src='https://img.freepik.com/free-icon/user_318-159711.jpg' width={100} height={100} className='img-fluid'/>
                          </p>
                        </div>
                    </div>
                  </div>
              </div>

              <hr/>

              <div className='row'>
                <div className='col-lg-6 col-md-6 col-sm-12 text-center'>
                  <p className='fw-bold fs-4 mt-2'> Nombre completo: </p>
                  <p className='fs-5 mt-1'> {usuario.nombre_completo} </p>
                </div>
                <div className='col-lg-6 col-md-6 col-sm-12 text-center'>
                  <p className='fw-bold fs-4 mt-2'> Edad: </p>
                  <p className='fs-5 mt-1'> {usuario.edad} </p>
                </div>
              </div>

              <hr/>

              <div className='row'>
                <div className='col-lg-6 col-md-6  col-sm-12 text-center'>
                  <p className='fw-bold fs-4 mt-2'>Clasificación inicial: </p>
                  <p className='fs-5 mt-1 text-uppercase'> {usuario.clasificacion_inicial} </p>
                </div>
                <div className='col-lg-6 col-md-6 col-sm-12 text-center'>
                  <p className='fw-bold fs-4 mt-2'>Foco aortico: </p>

                  {usuario.foco_aortico == "FA" ? (
                     <p className='fs-5 mt-1'> Foco aortico </p>
                  ) : (
                    null
                  )}

                  {usuario.foco_aortico == "FT" ? (
                     <p className='fs-5 mt-1'> Foco tricuspideo </p>
                  ) : (
                    null
                  )}

                  {usuario.foco_aortico == "FP" ? (
                     <p className='fs-5 mt-1'> Foco pulmonar </p>
                  ) : (
                    null
                  )}

                  {usuario.foco_aortico == "FM" ? (
                     <p className='fs-5 mt-1'> Foco mitral </p>
                  ) : (
                    null
                  )}
                  
                </div>
              </div>

              <hr/>

              <div className='row'>
                <div className='col-lg-6 col-md-6 col-sm-12 text-center'>
                  <p className='fw-bold fs-4 mt-2'>Número telefonico: </p>
                  <p className='fs-5 mt-1'> {usuario.celular} </p>
                </div>
                <div className='col-lg-6 col-md-6  col-sm-12 text-center'>
                  <p className='fw-bold fs-4 mt-2'>Familiar: </p>

                  {usuario.familiar == "" || usuario.familiar == null ? (
                    <p className='fs-5 mt-1'> SIN INFORMACIÓN.. </p>
                  ) : (
                    <p className='fs-5 mt-1'> {usuario.familiar} </p>
                  )}

                </div>
              </div>

              <hr/>


              <div className='row'>
                <div className='col-lg-12 text-center'>
                  <p className='fw-bold fs-4 mt-2'>Número telefonico familiar: </p>

                  {usuario.familiar_phone == "" || usuario.familiar_phone == null ? (
                    <p className='fs-5 mt-1'> SIN INFORMACIÓN.. </p>
                  ) : (
                    <p className='fs-5 mt-1'> {usuario.familiar_phone} </p>
                  )}


                </div>
              </div>              
            </div>
          </div>

      </div>
    )
}

export default PacienteExistente