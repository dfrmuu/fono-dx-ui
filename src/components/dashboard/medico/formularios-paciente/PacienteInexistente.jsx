import {useState,useEffect} from 'react'
import { Field, Form, Formik, ErrorMessage } from 'formik'
import { DateTime } from 'luxon';
import { toast} from 'react-toastify'
import * as Yup from 'yup'
import axios from 'axios'
import Cookies from 'universal-cookie';

const PacienteInexistente = ({dato,tipo}) => {

    //Variables para el formulario de registro
    const [nombre, setNombre] = useState("")
    const [apellido,setApellido] = useState("")
    const [direccion,setDireccion] = useState("")
    const [ciudad,setCiudad] = useState("")
    const [ciudades,setCiudades] = useState([])
    const [correoUsuario, setCorreoUsuario] = useState("")
    const [celularUsuario,setCelularUsuario] = useState("")
    const [institucionUsuario,setInstitucionUsuario] = useState("")
    const [edadUsuario,setEdadUsuario] = useState("")
    const [clasificacionSeleccionada,setClasificacionSeleccionada] = useState("")
    const [politicaAceptada,setPoliticaAceptada] = useState(false)
    const [numeroDocumento,setNumeroDocumento] = useState("")
    const [focoSeleccionado,setFocoSeleccionado] = useState("");
    const [familiar,setFamiliar] = useState("")
    const [telefonoFamiliar,setTelefonoFamiliar] = useState("")

    //Datos API
    const VITE_API_LINK = import.meta.env.VITE_API_LINK

    //Recupera las cookies del navegador
    const cookies = new Cookies()

    //Se ejecuta al cargar el componente
    useEffect(() => {
        consultaCiudades()
    }, [])
    
    //Controla la digitación de las variables del formulario
    const handleNombreChange = (e) => {
        setNombre(e.target.value)
    }

    const handleApellidosChange = (e) => {
        setApellido(e.target.value)
    }

    const handleDireccion = (e) => {
        setDireccion(e.target.value)
    }

    const handleCiudad = (e) => {
        setCiudad(e.target.value)
    }


    const handleCorreoChange = (e) => {
        setCorreoUsuario(e.target.value) 
    }

    const handleCelularChange = (e) => {
        setCelularUsuario(e.target.value)
    }

    const handleInstitucionChange = (e) => {
        setInstitucionUsuario(e.target.value)
    }

    const handleEdadChange = (e) => {
        setEdadUsuario(e.target.value)
    }

    const handleNumeroDocumentoChange = (e) => {
        setNumeroDocumento(e.target.value)
    }

    const handleClasificacionChange = (e) => {
        setClasificacionSeleccionada(e.target.value)
    }

    const handleFocoChange = (e) => {
        setFocoSeleccionado(e.target.value);
    }

    const handleFamiliar = (e) => {
        setFamiliar(e.target.value)
    }

    const handleTelefonoFamiliar = (e) => {
        setTelefonoFamiliar(e.target.value)
    }

    const handlePolitica = (e) => {
        if(e.target.value == "true"){
            setPoliticaAceptada(true)
        }else{
            setPoliticaAceptada(false)
        }
    }

    //Limpia los datos del formulario
    const reiniciarFormulario = () => {
        setNombre("")
        setApellido("")
        setDireccion("")
        setCiudad("")
        setCorreoUsuario("")
        setInstitucionUsuario("")
        setFocoSeleccionado("")
        setNumeroDocumento("")
        setEdadUsuario("")
        setClasificacionSeleccionada("")
        setCelularUsuario("")
        setFamiliar("")
        setTelefonoFamiliar("")
    }

    //Consulta las ciudades
    const consultaCiudades = async() => {
        try {
            
            await axios.get("https://www.datos.gov.co/resource/xdk5-pm3f.json")
                       .then((response) => {
                            const ciudades = response.data.map((ciudad) => ciudad.municipio)
                            setCiudades(ciudades)
                       }).catch((error) => {
                            console.log(error)
                       })

        } catch (error) {
            console.log(error)
        }
    }

    //Envia los datos del formulario
    const handleSubmit = async (values) => {

        const tokenAcceso = cookies.get("token")
        const {nombre, apellido,
               direccion , ciudad,
               correo, celular, 
               rol, ci, 
               foco_aortico,politica_aceptada,
               familiar, telefono_familiar,
               edad,clasificacion_inicial,
            } = values

        await axios.post(VITE_API_LINK + "paciente/registrar", {
           nombre : nombre,
           apellido : apellido,
           direccion : direccion,
           ciudad : ciudad,
           correo : correo,
           celular : celular,
           rol : rol,
           ci : ci,
           edad : edad,
           foco_aortico : foco_aortico,
           politica_aceptada : politica_aceptada,
           clasificacion_inicial : clasificacion_inicial,
           familiar : familiar,
           telefono_familiar : telefono_familiar
        },{
            headers: {
                Authorization: `Bearer ${tokenAcceso}`,
                ClientAuth : import.meta.env.VITE_APIKEY,
                Resource : 'pacientes',
                Action : 'crear'
            },

            timeout : 10000
        }).then((response) => {
            toast.info(response.data.Message)
            actividadRegistroPaciente()
            reiniciarFormulario()
        }).catch((error) => {
            toast.error(error.response.data.Error)
        })
    }


    //Cambia el contador de pacientes registrados al médico
    const actividadRegistroPaciente = async () => {

        //¿Que actividad es?
        const actividad = "Paciente"
        const tokenAcceso = cookies.get("token")

        //Envia los datos
        axios.post(VITE_API_LINK + "actividad/crear",{
            actividad,
        },{
            headers: {
                Authorization: `Bearer ${tokenAcceso}`,
                ClientAuth : import.meta.env.VITE_APIKEY
            },
            timeout : 10000  
        }).then((response) => {

        }).catch((error) =>{
            toast.error(error.response.data.Error)
        })  

    }


    //Estructura para el formulario de paciente - usuario
    const PacienteUsuarioScheme = Yup.object().shape({
        nombre :        Yup.string()
                            .required('El nombre es requerido')
                            .min(2, "Digite nombre valido")
                            .max(30, 'Digite nombre valido'),

        apellido :      Yup.string()
                            .required('El apellido es requerido')
                            .min(2, "Digite apellido valido")
                            .max(30, 'Digite apellido valido'),

        direccion :     Yup.string(),

        ciudad :    Yup.string()
                        .required("¿Qué ciudad es?"),

  
        celular :     Yup.string()
                       .required("El celular es requerido")
                       .min(7, "Digite celular valido")
                       .max(15, 'Digite celular valido'),
     
  
        correo : Yup.string()
                    .email("Debe ser un correo valido")
                    .required("El correo es requerido")
                    .min(10, "Digite correo valido")
                    .max(50, 'Digite correo valido'),
  
  
        ci :            Yup.string()
                            .required('El número de documento es requerido')
                            .matches(/^[0-9]+$/, "Solo números")
                            .min(6, "Digite un número de documento valido")
                            .max(14, 'Digite un número de documento valido'),

        edad :     Yup.string()
                        .required("La edad es requerida")
                        .matches(/^[0-9]+$/, "Solo números")
                        .min(1, 'Digite una edad valida')
                        .max(3, 'Solamente 3 digitos de edad'),

        clasificacion_inicial :  Yup.string()
                                    .required("La clasificación inicial es requerida"),

                        
        ID_Medico : Yup.string()
                        .required("El medico es requerido"),


        foco_aortico :   Yup.string()
                            .required("Foco requerido")
                            .oneOf(["FA","FP","FT","FM"], "Escoja algún foco"),


        politica_aceptada : Yup.string()
                              .required("¿Aceptas la politica?")
                              .oneOf(['true','false'], "No son valores validos.."),


        familiar : Yup.string(),


        telefono_familiar : Yup.string(),

    
    })



    return (
            <Formik validationSchema={PacienteUsuarioScheme} 
                    enableReinitialize={true}
                    onSubmit={async(values) => { handleSubmit(values)}}
                    initialValues={{
                        nombre : nombre ,
                        apellido : apellido,
                        direccion : direccion,
                        ciudad : ciudad, 
                        correo : tipo == 'correo' ? dato : correoUsuario,
                        celular : celularUsuario,
                        institucion : "",
                        rol : 1,
                        ci : tipo == 'documento' ? dato : numeroDocumento,
                        ID_Medico : "1",
                        edad : edadUsuario,
                        clasificacion_inicial : clasificacionSeleccionada,
                        foco_aortico : focoSeleccionado,
                        politica_aceptada : politicaAceptada,
                        familiar :familiar,
                        telefono_familiar : telefonoFamiliar
                    }}>

                        <Form className='mt-3 light-grey-handbook p-3 shadow rounded'>
                            <br/>
                            <div className='dark-red-handbook text-white p-2 mb-3 rounded shadow'>
                                <p className='text-center mt-2'> Este usuario no existe, puede iniciar registrandolo con el formulario a continuación: </p>
                            </div>

                            <div className='row'> 
                                <div className="col-md-4">
                                    <label className='form-label fw-bold mt-1' htmlFor='nombre'> Nombres del paciente:</label>
                                    <Field type="text" className='form-control shadow-sm p-3' id="nombre" name="nombre" placeholder='Ingrese el nombre del paciente'
                                        onKeyUp={handleNombreChange}/>
                                    <ErrorMessage name='nombre' component='div' className='dark-grey-handbook mt-1 mb-1 p-2 text-white rounded text-center fw-bold'/>
                                </div>
                                <div className="col-md-4">
                                    <label className='form-label fw-bold mt-1' htmlFor='apellido'> Apellidos del paciente:</label>
                                    <Field type="text" className='form-control shadow-sm p-3' id="apellido" name="apellido" placeholder='Ingrese los apellidos del paciente'
                                        onKeyUp={handleApellidosChange}/>
                                    <ErrorMessage name='apellido' component='div' className='dark-grey-handbook  mt-1 mb-1  p-2 text-white rounded text-center fw-bold'/>
                                </div>
                                <div className="col-md-4">
                                    <label className='form-label fw-bold mt-1' htmlFor='correo'> Correo del paciente:</label>
                                    {tipo == 'correo' ? (
                                        <>
                                            <Field type="text" className='form-control shadow-sm p-3' id="correo" name="correo" placeholder='Ingrese el correo electronico del paciente'
                                             disabled/>
                                            <ErrorMessage name='correo' component='div' className='dark-grey-handbook  mt-1 mb-1 p-2 text-white rounded text-center fw-bold'/>
                                        </>
                                    ):(
                                        <>
                                            <Field type="text" className='form-control shadow-sm p-3' id="correo" name="correo" placeholder='Ingrese el correo electronico del paciente'
                                             onKeyUp={handleCorreoChange}/>
                                            <ErrorMessage name='correo' component='div' className='dark-grey-handbook mt-1 mb-1 p-2 text-white rounded text-center fw-bold'/>
                                        </>
                                    )}
                                </div>
                            </div> 

                            <br/>

                            <div className='row'>
                                <div className='col-md-6'>
                                    <label className='form-label fw-bold mt-1' htmlFor='direccion'> Dirección de residencia:</label>
                                    <Field type="text" className='form-control shadow-sm p-3' id="direccion" name="direccion" placeholder='Ingrese la dirección de residencia del paciente'
                                        onKeyUp={handleDireccion}/>
                                    <ErrorMessage name='direccion' component='div' className='dark-grey-handbook  mt-1 mb-1 p-2 text-white rounded text-center fw-bold'/>
                                </div>
                                <div className='col-md-6'>
                                    <label className='form-label fw-bold mt-1' htmlFor='ciudad'> Ciudad:</label>
                                    <Field as="select" className='form-select w-100 shadow-sm p-3' name="ciudad" id="ciudad" 
                                        value ={ciudad} onChange={handleCiudad}>
                                        <option value="">Seleccione..</option>
                                            {ciudades.map((ciudad) => (
                                                <option value={ciudad} >
                                                    {ciudad}
                                                </option>
                                            ))}
                                    </Field>
                                    <ErrorMessage name='ciudad' component='div' className='dark-grey-handbook mt-1 mb-1 p-2 text-white rounded text-center fw-bold'/>
                                </div>
                            </div>

                            <br/>

                            <div className='row'>
                                <div className="col-md-12">
                                    <label className='form-label fw-bold mt-1' htmlFor='celular'> Celular:</label>
                                    <Field type="tel" className='form-control shadow-sm p-3' id="celular" name="celular" placeholder='Ingrese el teléfono celular del paciente'
                                        onKeyUp={handleCelularChange}/>
                                    <ErrorMessage name='celular' component='div' className='dark-grey-handbook mt-1 mb-1 p-2 text-white rounded text-center fw-bold'/>
                                </div>
                            </div> 
                            
                            
                            <br/>

                            <div className='row'>
                                <div className='col-md-6'>
                                    <label className='form-label fw-bold mt-1' htmlFor='rol'> Rol:</label>
                                    <Field as="select" className='shadow-sm w-100 form-control p-3' id="rol" name="rol" value="1" disabled>
                                        <option> Paciente </option>
                                    </Field>
                                </div>
                                <div className="col-md-6">
                                    <label className='form-label fw-bold mt-1' htmlFor='ci'> Número de documento:</label>
                                    <Field type="number" className='form-control shadow-sm p-3' id="ci" name="ci" placeholder='Ingrese el número de documento del paciente'
                                        onKeyUp={handleNumeroDocumentoChange}/>
                                    <ErrorMessage name='ci' component='div' className='dark-grey-handbook mt-1 mb-1 p-2 text-white rounded text-center fw-bold'/>
                                </div>
                            </div>

                            <br/>

                            <div className='row'>
                                <div className="col-md-6">
                                    <label className='form-label fw-bold mt-1' htmlFor='edad'> Edad:</label>
                                    <Field type="number" className='form-control shadow-sm p-3' id="edad" name="edad" placeholder='Ingrese la edad del paciente'
                                        onKeyUp={handleEdadChange}/>
                                    <ErrorMessage name='edad' component='div' className='dark-grey-handbook  mt-1 mb-1 p-2 text-white rounded text-center fw-bold'/>
                                </div>
                                <div className="col-md-6">
                                    <label className='form-label fw-bold mt-1' htmlFor='clasificacion_inicial'> Clasificación inicial:</label>
                                    <Field as="select" className='form-select w-100 shadow-sm p-3' name="clasificacion_inicial" id="clasificacion_inicial" 
                                        value ={clasificacionSeleccionada} onChange={handleClasificacionChange}>
                                        <option value="">Seleccione..</option>
                                        <option value="patologico">Patologico</option>
                                        <option value="no patologico">No patologico</option>
                                    </Field>
                                    <ErrorMessage name='clasificacion_inicial' component='div' className='dark-grey-handbook  mt-1 mb-1 p-2 text-white rounded text-center fw-bold'/>
                                </div>
                            </div>

                            <br/>


                            <div className='row'>
                                <div className="col-md-6">
                                    <label className='form-label fw-bold mt-1'> Medico encargado:</label>
                                    <Field as="select" className='form-select w-100 shadow-sm p-3' id="ID_Medico" name="ID_Medico" disabled>
                                        <option value="1"> Usted </option>
                                    </Field>
                                </div>
                                <div className="col-md-6">
                                    <label className='form-label fw-bold mt-1' htmlFor='foco_aortico'> Foco aortico:</label>
                                    <Field as="select" className='form-select w-100 shadow-sm p-3' id="foco_aortico" name="foco_aortico" value={focoSeleccionado} onChange={handleFocoChange}>
                                        <option value=""> Seleccione un foco.</option>
                                        <option value="FA"> FA - Foco aortico </option>
                                        <option value="FP"> FP - Foco pulmonar </option>
                                        <option value="FT"> FT - Foco triscupideo</option>
                                        <option value="FM"> FM - Foco mitral</option>
                                    </Field>
                                    <ErrorMessage name='foco_aortico' component='div' className='dark-grey-handbook mt-1 mb-1  p-2 text-white rounded text-center fw-bold'/>
                                </div>
                            </div>

                            <br/>

                            <div className='row'>
                                <div className='col-md-6'>
                                    <label className='form-label fw-bold mt-1'> Familiar (opcional):</label>
                                    <Field type="text" className='form-control shadow-sm p-3' id="familiar" name="familiar" placeholder='Nombre del familiar del paciente'
                                        onKeyUp={handleFamiliar}/>
                                    <ErrorMessage name='familiar' component='div' className='dark-grey-handbook mt-1 mb-1 p-2 text-white rounded text-center fw-bold'/>
                                </div>
                                <div className='col-md-6'>
                                <label className='form-label fw-bold mt-1'> Teléfono familiar (opcional):</label>
                                    <Field type="text" className='form-control shadow-sm p-3' id="telefono_familiar" name="telefono_familiar" placeholder='Digite el teléfono del familiar del paciente'
                                        onKeyUp={handleTelefonoFamiliar}/>
                                    <ErrorMessage name='telefono_familiar' component='div' className='dark-grey-handbook  mt-1 mb-1 p-2 text-white rounded text-center fw-bold'/>
                                </div>
                            </div>

                            <br/>

                            <div className='row'>
                                <div className='col-md-12'>
                                    <label className='form-label fw-bold mt-1'> ¿Acepta el paciente la politica de privacidad?:</label>
                                      <Field as="select" id="Politica" name='Politica' className="form-control form-select p-3" value={politicaAceptada} onChange = {handlePolitica}>
                                          <option value="">Seleccione..</option>
                                          <option value="true">SI</option>
                                          <option value="false">NO</option>
                                      </Field>
                                      <ErrorMessage name='Politica' component='div' className='dark-grey-handbook  mt-1 mb-1 p-2 text-white rounded text-center fw-bold'/>
                                </div>
                            </div>

                            {politicaAceptada == true ? (
                                <div className='row mt-5'>
                                    <div className='col-md-12'>
                                        <button type="submit" className='btn dark-red-handbook text-white w-100 p-3'>Guardar paciente </button>
                                    </div>
                                 </div>
                            ) : (
                                <div className='row mt-5'>
                                    <div className='col-md-12'>
                                        <button type="button" className='btn dark-red-handbook text-white w-100 p-3' disabled>Guardar paciente </button>
                                    </div>
                                </div>
                            )}
                        </Form>
        </Formik>
    )
}

export default PacienteInexistente