import {useState,useEffect} from 'react'
import { Field, Form, Formik, ErrorMessage } from 'formik'
import * as Yup from 'yup'
import axios from 'axios';
import { toast} from 'react-toastify';
import Cookies from 'universal-cookie';

//Componente que se usara cuando el paciente no tenga sus datos completos
const PacienteIncompleto = ({usuario,dato,tipo}) => {

    //Datos paciente
    const [focoSeleccionado, setFocoSeleccionado] = useState("");
    const [edad,setEdad] = useState("")
    const [familiar,setFamiliar] = useState("")
    const [numeroFamiliar,setNumeroFamiliar] = useState("")
    const [clasficacion,setClasificacion] = useState("")

    //Recupera los valores de las cookies
    const cookies = new Cookies()

    //Datos API
    const VITE_API_LINK = import.meta.env.VITE_API_LINK

    //Controla los datos del paciente
    const handleFamiliar = (e) => {
        setFamiliar(e.target.value);
    }

    const handleEdadChange = (e) => {
        setEdad(e.target.value);
    }


    const handleNumeroFamiliar = (e) => {
        setNumeroFamiliar(e.target.value);
    }

    const handleFocoChange = (e) => {
        setFocoSeleccionado(e.target.value);
    }
    
    const handleClasificacion = (e) => {
        setClasificacion(e.target.value);
    }
    

    //Reinicia los datos del formulario
    const reiniciarFormulario = () => {
        setFocoSeleccionado("")
        setFamiliar("")
        setEdad("")
        setClasificacion("")
        setNumeroFamiliar("")
    }

    //Envia los datos del formulario
    const handleSubmit = async (values) => {

        //Recupera el token en las cookies
        const tokenAcceso = cookies.get("token")

        //Recupera los datos del formulario
        const {familiar, telefono_familiar, foco_aortico,edad,clasificacion_inicial} = values
        const ID_Usuario = usuario  

        //Envia los datos
        await axios.post(VITE_API_LINK + "paciente/crear", {
           ID_Usuario,
           familiar,
           telefono_familiar,
           edad,
           clasificacion_inicial,
           foco_aortico
        },{
            headers: {
                Authorization: `Bearer ${tokenAcceso}`,
                ClientAuth : import.meta.env.VITE_APIKEY,
                Resource : 'pacientes',
                Action : 'crear'
            },
            timeout : 10000
        }).then((response) => {
            toast.info(response.data.Message)
            actividadRegistroPaciente()
            reiniciarFormulario()
        }).catch((error) => {
            toast.error(error.response.data.Error)
        })
    }

    //Aumenta el contador de registro de actividad del médico
    const actividadRegistroPaciente = async () => {

        const actividad = "Paciente"
        const tokenAcceso = cookies.get("token")


        axios.post(VITE_API_LINK + "actividad/crear",{
            actividad,
        },{
            headers: {
                Authorization: `Bearer ${tokenAcceso}`,
                ClientAuth : import.meta.env.VITE_APIKEY
            },
            timeout : 10000  
        }).then((response) => {

        }).catch((error) =>{
            console.log(error)
        })  

    }

    //Esctructura del formulario
    const PacienteScheme = Yup.object().shape({ 

        edad :     Yup.string()
                        .required("La edad es requerida")
                        .matches(/^[0-9]+$/, "Solo números")
                        .min(1, 'Digite una edad valida')
                        .max(3, 'Solamente 3 digitos de edad'),

        ID_Medico : Yup.string(),


        foco_aortico :   Yup.string()
                    .required("El foco aortico es requerido")
                    .oneOf(["FA","FP","FT","FM"], "Escoja algún foco"),
                    
        familiar : Yup.string(),
        
        
        telefono_familiar : Yup.string(),

        clasificacion_inicial : Yup.string()
                                   .required("¿En que estado se encuentra?")
                                   .oneOf(['patologico', 'no patologico'], 'Escriba un nombre valido')

    })



    return (
        <>
            <Formik validationSchema={PacienteScheme} 
                    onSubmit={async (values) => {
                        handleSubmit(values)
                    }}
 
                    enableReinitialize={true}

                    initialValues = {{
                        familiar : familiar,
                        telefono_familiar : numeroFamiliar,
                        clasificacion_inicial : clasficacion,
                        edad : edad,
                        ID_Medico :  "",
                        foco_aortico : focoSeleccionado
                    }}>


                <Form className='mt-3 light-grey-handbook p-3 shadow rounded'>
                    <br/>
                    <div className='dark-grey-handbook text-white p-2 mb-3 rounded shadow'>
                        <p className='text-center mt-2'> El paciente en consulta esta registrado como usuario, pero no ha completado sus datos de paciente, 
                                                         puede asignarle los datos a continuación:</p>
                    </div>

                    {/* Formulario usuario que no ha completado el formulario de paciente */}
                    <div className='row'> 
                        <div className="col-md-6">
                            <label className='form-label fw-bold mt-1'> Medico encargado:</label>
                            <Field as="select" className='form-select w-100 shadow-sm p-3' name="ID_Medico" id="ID_Medico">
                                <option> Usted </option>
                            </Field>
                            <ErrorMessage name='ID_Medico' component='div' className='dark-grey-handbook mb-2 mt-2 p-2 text-white rounded text-center fw-bold'/>
                        </div>
                        <div className="col-md-6">
                            <label className='form-label fw-bold mt-1' htmlFor='edad'> Edad:</label>
                            <Field type="number" className='form-control shadow-sm p-3' id="edad" name="edad" placeholder='Ingrese la edad del paciente'
                                    onKeyUp={handleEdadChange}/>
                            <ErrorMessage name='edad' component='div' className='dark-grey-handbook mb-2 mt-2 p-2 text-white rounded text-center fw-bold'/>
                        </div>
                    </div>

                    <br/>

                    <div className='row'>
                        <div className="col-md-6">
                            <label className='form-label fw-bold mt-1' htmlFor='foco_aortico'> Foco aortico:</label>
                            <Field as="select" className='form-select w-100 p-3 shadow-sm' id="foco_aortico" name="foco_aortico" value ={focoSeleccionado} onChange = {handleFocoChange}>
                                <option value=""> Seleccione un foco.</option>
                                <option value="FA">FA - Foco aortico</option>
                                <option value="FP">FP - Foco pulmonar</option>
                                <option value="FT">FT - Foco tricuspideo</option>
                                <option value="FM">FM - Foco mitral</option>
                            </Field>
                            <ErrorMessage name='foco_aortico' component='div' className='dark-grey-handbook mb-2 mt-2 p-2 text-white rounded text-center fw-bold'/>
                        </div>
                        <div className="col-md-6">
                            <label className='form-label fw-bold mt-1' htmlFor='clasficiacion_inicial'> Clasificación inicial:</label>
                            <Field as="select" className='form-select w-100 p-3 shadow-sm' id="clasificacion_inicial" name="clasificacion_inicial" value ={clasficacion} onChange = {handleClasificacion}>
                                <option value=""> Seleccione..</option>
                                <option value="patologico">Patológico</option>
                                <option value="no patologico">No patológico</option>
                            </Field>
                            <ErrorMessage name='clasificacion_inicial' component='div' className='dark-grey-handbook mb-2 mt-2 p-2 text-white rounded text-center fw-bold'/>
                        </div>
                    </div>


                    <br/>

                    <div className='row'>
                        <div className="col-md-6">
                            <label className='form-label fw-bold mt-1' htmlFor='foco_aortico'> Nombre familiar (opcional):</label>
                            <Field type="text" className='form-control w-100 p-3 shadow-sm' id="familiar" name="familiar" placeholder="Escriba el nombre completo.."  
                                    onKeyUp = {handleFamiliar}/>
                            <ErrorMessage name='familiar' component='div' className='dark-grey-handbook mb-2 mt-2 p-2 text-white rounded text-center fw-bold'/>
                        </div>
                        <div className="col-md-6">
                            <label className='form-label fw-bold mt-1' htmlFor='foco_aortico'> Número telefonico familiar (opcional):</label>
                            <Field type="text" className='form-control w-100 p-3 shadow-sm' id="telefono_familiar" name="telefono_familiar" placeholder="Digite el número.." 
                                    onKeyUp = {handleNumeroFamiliar}/>
                            <ErrorMessage name='telefono_familiar' component='div' className='dark-grey-handbook mb-2 mt-2 p-2 text-white rounded text-center fw-bold'/>
                        </div>
                    </div>

                    {/* Fin formulario usuario que no ha completado el formulario de paciente */}

                    <div className='row mt-5'>
                        <div className='col-md-12'>
                            <button className='btn dark-red-handbook p-3 text-white w-100' type="submit">Guardar paciente </button>
                        </div>
                    </div>

                </Form>
            </Formik>

        </>
    )
}

export default PacienteIncompleto