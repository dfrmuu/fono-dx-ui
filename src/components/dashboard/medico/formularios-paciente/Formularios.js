export {default as PacienteIncompleto} from './PacienteIncompleto'
export {default as PacienteInexistente} from './PacienteInexistente'
export {default as PacienteExistente} from './PacienteExistente'
export {default as EditarPaciente} from './EditarPaciente'