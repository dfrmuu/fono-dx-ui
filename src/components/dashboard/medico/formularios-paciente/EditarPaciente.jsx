import axios from 'axios'
import { ErrorMessage, Field, Form, Formik } from 'formik'
import {useEffect,useState} from 'react'
import Modal from 'react-responsive-modal'
import Cookies from 'universal-cookie'
import * as Yup from 'yup'
import SpinnerHeart from '../../../SpinnerHeart'
import { toast } from 'react-toastify'

const EditarPaciente = ({abierto,handleModal,id,traerMisPacientes}) => {

    //Datos del paciente
    const [clasificacion,setClasificacion] = useState("")
    const [foco,setFoco] = useState("")
    const [edad,setEdad] = useState("")
    const [familiar,setFamiliar] = useState("")
    const [telefonoFamiliar,setTelefonoFamiliar] = useState("")


    //API
    const [cargando,setCargando] = useState(false)
    const [pacienteAPI,setPacienteAPI] = useState([])
    const VITE_API_LINK = import.meta.env.VITE_API_LINK
    const cookies = new Cookies()

    useEffect(() => {
        traerPaciente()
    }, [])
    
    useEffect(() => {
        //Recupera los datos del paciente a partir de la consulta al API
        if(Object.keys(pacienteAPI).length > 0){
            if(pacienteAPI[0].initial_classification){
                setClasificacion(pacienteAPI[0].initial_classification)
            }

            if(pacienteAPI[0].aortic_focus){
                setFoco(pacienteAPI[0].aortic_focus)
            }

            setEdad(pacienteAPI[0].age)
            setFamiliar(pacienteAPI[0].familiar)
            setTelefonoFamiliar(pacienteAPI[0].familiar_phone)
        }
    }, [pacienteAPI])
    

    //Estructura del formulario
    const pacienteScheme = Yup.object().shape({
        initial_classification : Yup.string()
                                    .oneOf(['patologico','no patologico'], "Seleccione una opción valida")
                                    .required("La clasificación es requerida"),

        aortic_focus :   Yup.string()
                            .required("El foco aortico es requerido")
                            .oneOf(["FA","FP","FT","FM"], "Escoja algún foco"),

        age : Yup.string()
                 .required("La edad es requerida"),

        familiar : Yup.string(),

        familiar_phone : Yup.string()

    })


    //Recupera los datos del paciente
    const traerPaciente = async() => {

        //Recupera el token en las cookies
        const tokenAcceso = cookies.get("token")

        //Trae los datos
        await axios.get(VITE_API_LINK + `paciente/traer/${id}`, {
            headers: {
                Authorization: `Bearer ${tokenAcceso}`,
                ClientAuth : import.meta.env.VITE_APIKEY,
                Resource: 'pacientes',
                Action : 'listar'
            },
            timeout : 10000
        })
        .then((response) =>{
            setPacienteAPI(response.data)
        }).catch((error) => {
            setPacienteAPI([])
        })
    }

    //Envia los valores para la edición del paciente
    const editarPaciente = async(values) => {
        try {

            const {aortic_focus,initial_classification,familiar,age,familiar_phone} = values
            const tokenAcceso = cookies.get("token")


            await axios.put(VITE_API_LINK + `paciente/editar`,{
                id : id,
                aortic_focus : aortic_focus,
                initial_classification : initial_classification,
                familiar : familiar,
                age:age,
                familiar_phone : familiar_phone
            },{
                headers: {
                    Authorization: `Bearer ${tokenAcceso}`,
                    ClientAuth : import.meta.env.VITE_APIKEY,
                    Resource: 'pacientes',
                    Action : 'editar'
                },
                timeout : 10000
            })
            .then((response) =>{
                toast.info(response.data.Message)
                traerMisPacientes()
                handleModal()
            }).catch((error) => {
                toast.error(error.response.data.Error)
            })

        } catch (error) {
            console.log(error)
        }
    }

    //Controla los datos del formulario
    const handleClasificacion = (e) => {
        setClasificacion(e.target.value)
    }

    const handleFoco = (e) => {
        setFoco(e.target.value)
    }

    const handleEdad = (e) => {
        setEdad(e.target.value)
    }

    const handleFamiliar = (e) => {
        setFamiliar(e.target.value)
    }

    const handleTelefono = (e) => {
        setTelefonoFamiliar(e.target.value)
    }


    return (
        <Modal open={abierto}
               onClose={handleModal}
               classNames={{
                modal : 'w-75 rounded mt-5 mid-grey-handbook'
               }}
               animationDuration={500}
               center>
            

                        
            <div>
                    <Formik validationSchema={pacienteScheme}
                            enableReinitialize={true}
                            initialValues={{
                                age : edad,
                                initial_classification : clasificacion,
                                aortic_focus : foco,
                                familiar : familiar,
                                familiar_phone : telefonoFamiliar
                            }}
                            onSubmit={async(values) => {
                                editarPaciente(values)
                            }}>
                        <Form>

                            <h2 className='fw-bold text-uppercase text-center'>Editar paciente</h2>
                            <hr/>

                            <div className='row'>
                                <div className='col-md-12'>
                                    <label className='form-label fw-bold' htmlFor='age'>Edad: </label>
                                    <Field type="text" className='form-control shadow-sm p-3 mb-2' id="age" name="age" onKeyUp={handleEdad} placeholder="Escriba la edad"/>
                                    <ErrorMessage className='dark-grey-handbook mt-2 mb-2 p-2 text-white rounded text-center fw-bold' component="div" name="age"/>  
                                </div>
                            </div>


                            <br/>

                            <div className='row'>
                            <div className="col-md-6">
                                    <label className='form-label fw-bold mt-1' htmlFor='initial_classification'> Clasificación inicial:</label>
                                    <Field as="select" className='form-select w-100 p-3 shadow-sm' id="initial_classification" name="initial_classification" value={clasificacion} onChange = {handleClasificacion}>
                                        <option value=""> Seleccione..</option>
                                        <option value="patologico">Patológico</option>
                                        <option value="no patologico">No patológico</option>
                                    </Field>
                                    <ErrorMessage name='initial_classification' component='div' className='dark-grey-handbook mb-2 mt-2 p-2 text-white rounded text-center fw-bold'/>
                                </div>
                                <div className="col-md-6">
                                    <label className='form-label fw-bold mt-1' htmlFor='aortic_focus'> Foco aortico:</label>
                                    <Field as="select" className='form-select w-100 p-3 shadow-sm' id="aortic_focus" name="aortic_focus" value ={foco} onChange = {handleFoco}>
                                        <option value=""> Seleccione un foco.</option>
                                        <option value="FA">FA - Foco aortico</option>
                                        <option value="FP">FP - Foco pulmonar</option>
                                        <option value="FT">FT - Foco tricuspideo</option>
                                        <option value="FM">FM - Foco mitral</option>
                                    </Field>
                                    <ErrorMessage name='aortic_focus' component='div' className='dark-grey-handbook mb-2 mt-2 p-2 text-white rounded text-center fw-bold'/>
                                </div>
                            </div>

                            <br/>

                            <div className='row'>
                                <div className="col-md-12">
                                    <label className='form-label fw-bold mt-2' htmlFor='familiar'> Nombre familiar (opcional):</label>
                                    <Field type="text" className='form-control w-100 p-3 shadow-sm' id="familiar" name="familiar" placeholder="Escriba el nombre completo.."  
                                            onKeyUp = {handleFamiliar}/>
                                    <ErrorMessage name='familiar' component='div' className='dark-grey-handbook mb-2 mt-2 p-2 text-white rounded text-center fw-bold'/>
                                </div>
                                <div className="col-md-12">
                                    <label className='form-label fw-bold mt-2' htmlFor='familiar_phone'> Número telefonico familiar (opcional):</label>
                                    <Field type="text" className='form-control w-100 p-3 shadow-sm' id="familiar_phone" name="familiar_phone" placeholder="Digite el número.." 
                                            onKeyUp = {handleTelefono}/>
                                    <ErrorMessage name='familiar_phone' component='div' className='dark-grey-handbook mb-2 mt-2 p-2 text-white rounded text-center fw-bold'/>
                                </div>
                            </div>

                            <br/>

                            <div className='row'>
                                <div className='col-md-12'>
                                    <button className='btn dark-red-handbook text-white w-100 p-3' type='submit'> 
                                        Editar paciente
                                    </button>
                                </div>
                            </div>
                        </Form>
                    </Formik>

            </div>
        </Modal>
    )
}

export default EditarPaciente