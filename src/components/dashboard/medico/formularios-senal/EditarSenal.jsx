import axios from 'axios'
import { ErrorMessage, Field, Form, Formik } from 'formik'
import {useEffect, useState} from 'react'
import Modal from 'react-responsive-modal'
import { toast } from 'react-toastify'
import Cookies from 'universal-cookie'
import * as Yup from 'yup'

//Componente para editar la auscultación
const EditarSenal = ({id,traerSenales}) => {

    //Datos de la auscultación
    const [focus,setFocus] = useState("")
    const [reference,setReference] = useState("")


    //Variable para almacenar datos del API
    const [senalAPI,setSenalAPI] = useState([])

    //Recupera las cookies del navegador
    const cookies = new Cookies()

    //Datos del api
    const VITE_API_LINK = import.meta.env.VITE_API_LINK


    //Controla la digitación de valores
    const handleFocus = (e) => {
        setFocus(e.target.value)
    }

    //Trae la señal apenas renderiza el componente
    useEffect(() => {
        traerSenal()
    }, [])
    

    //Revisa los valores del objeto
    useEffect(() => {
        if(Object.keys(senalAPI).length > 0 ){
            setFocus(senalAPI.focus)
        }
    }, [senalAPI])
    

    //Estructura auscultación
    const senalScheme = Yup.object().shape({

        focus :   Yup.string()
                     .required("El foco aortico es requerido")
                     .oneOf(["FA","FP","FT","FM"], "Escoja algún foco"),

    })


    //Recupera los datos de la auscultación
    const traerSenal= async() => {

        const tokenAcceso = cookies.get("token")

        await axios.get(VITE_API_LINK + `senales/traer/${id}`, {
            headers: {
                Authorization: `Bearer ${tokenAcceso}`,
                ClientAuth : import.meta.env.VITE_APIKEY,
                Resource: 'auscultacion',
                Action : 'listar'
            },
            timeout : 10000
        })
        .then((response) =>{
            setSenalAPI(response.data)
        }).catch((error) => {
            setSenalAPI([])
        })
    }

    //Envia los datos para editar la señal
    const editarSenal = async(values) => {
        try {

            const {focus} = values
            const tokenAcceso = cookies.get("token")

            await axios.put(VITE_API_LINK + `senal/editar`,{
                id : id,
                focus : focus
            },{
                headers: {
                    Authorization: `Bearer ${tokenAcceso}`,
                    ClientAuth : import.meta.env.VITE_APIKEY,
                    Resource: 'auscultacion',
                    Action : 'editar'
                },
                timeout : 10000
            })
            .then((response) =>{
                toast.info(response.data.Message)
                traerSenales()
            }).catch((error) => {
                toast.error(error.response.data.Error)
            })

        } catch (error) {
            console.log(error)
        }
    }


    return (
        <div>           
            <div className='light-grey-handbook mt-3 p-3 shadow rounded'>
                    <Formik validationSchema={senalScheme}
                            enableReinitialize={true}
                            initialValues={{
                                focus : focus,
                            }}
                            onSubmit={async(values) => {
                                editarSenal(values)
                            }}>
                        <Form>

                            <h4 className='fw-bold text-uppercase text-start'>Editar auscultación</h4>
                            <hr/>

                            <div className='row'>
                                <div className='col-lg-12'>
                                    <label className='form-label fw-bold' htmlFor='age'>Foco: </label>
                                    <Field as="select" className='form-select w-100 p-3 shadow-sm' id="focus" name="focus" value ={focus} onChange = {handleFocus}>
                                        <option value=""> Seleccione un foco.</option>
                                        <option value="FA">FA - Foco aortico</option>
                                        <option value="FP">FP - Foco pulmonar</option>
                                        <option value="FT">FT - Foco tricuspideo</option>
                                        <option value="FM">FM - Foco mitral</option>
                                    </Field>
                                    <ErrorMessage name='focus' component='div' className='dark-grey-handbook mb-2 mt-2 p-2 text-white rounded text-center fw-bold'/>
                                </div>
                            </div>


                            <br/>

                            <div className='row'>
                                <div className='col-lg-12'>
                                    <button className='btn dark-red-handbook text-white w-100 p-3' type='submit'> 
                                        Editar auscultación
                                    </button>
                                </div>
                            </div>
                        </Form>
                    </Formik>

            </div>
        </div>
    )
}

export default EditarSenal