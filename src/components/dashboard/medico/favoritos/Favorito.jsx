import {useState,useEffect} from 'react'
import { Form, Field, Formik, ErrorMessage} from 'formik'
import { toast } from 'react-toastify';
import { useOutletContext } from 'react-router-dom';
import Modal from 'react-responsive-modal';
import * as Yup from 'yup'
import axios from 'axios';
import Cookies from 'universal-cookie';
import SweetAlert from 'react-bootstrap-sweetalert';
import { FaSave } from 'react-icons/fa';

const Favorito = ({senal,misFavoritos}) => {

    //Handle para modals (editar,eliminar)
    const [editarAbierto,setEditarAbierto] = useState(false)
    const [eliminarAbierto,setEliminarAbierto] = useState(false)

    //Datos favorito
    const [nuevaNota,setNuevaNota] = useState("")
    const [idEditar,setIDEditar] = useState("")
    const [idEliminar,setIDEliminar] = useState("")

    //Recupera las cookies del navegador
    const cookies = new Cookies()

    //Datos del API
    const VITE_API_LINK = import.meta.env.VITE_API_LINK

    //Envia los datos para editar
    const submitEditar = async (values) => {
        try {

            //Recupera el token
            const tokenAcceso = cookies.get("token")

            //Recupera los valores del formulario
            const {notes} = values


            //Envia los valores
            await axios.put(VITE_API_LINK + "favoritos/editar",{
                FavoriteID : idEditar,
                notes : notes,
            },{
                headers: {
                    Authorization: `Bearer ${tokenAcceso}`,
                    ClientAuth : import.meta.env.VITE_APIKEY
                },

                timeout : 10000
            }).then((response) => {
                toast.info(response.data.Message)
                misFavoritos()
            }).catch((error) => {
                toast.error(error.response.data.Error)
            })

        } catch (error) {
        }
    }

    //Envia los datos para eliminar
    const submitEliminar = async() => {
        try {
            const tokenAcceso = cookies.get("token")

            //Envia los valores para eliminar
            await axios.delete(VITE_API_LINK + `favoritos/eliminar/${idEliminar}`,
            {
                headers: {
                    Authorization: `Bearer ${tokenAcceso}`,
                    ClientAuth : import.meta.env.VITE_APIKEY
                },

                timeout : 10000

            }).then((response) => {
                toast.info(response.data.Message)
                misFavoritos()
            }).catch((error) => {
                toast.error(error.response.data.Error)
            })

        } catch (error) {

        }
    }

    const handleNotaChange = (e) => {
        setNuevaNota(e.target.value)
    }

    //Controla los modal para editar / eliminar
    const handleModalEditar = (id) => {
        if(editarAbierto == true){
            setEditarAbierto(false)
            setIDEditar("")
        }else{
            setEditarAbierto(true)
            setIDEditar(id)
        }
    }

    const handleModalEliminar = (id) => {
        if(eliminarAbierto == true){
            setEliminarAbierto(false)
            setIDEliminar("")
        }else{
            setEliminarAbierto(true)
            setIDEliminar(id)
        }
    }
    
    //Escturctura favorito
    const favoriteScheme = Yup.object().shape({
        notes : Yup.string()
                   .required("No puede dejar la nota vacia!")
                   .max(200),
    })

    return (

        <>

        {eliminarAbierto == true ? (
                <SweetAlert
                warning
                showCancel
                confirmBtnText="Si"
                cancelBtnText = "No"
                confirmBtnBsStyle="success"
                cancelBtnBsStyle="danger"
                title="Estas seguro de eliminar esta señal de referencia?"
                onConfirm={() => submitEliminar()}
                onCancel={() => handleModalEliminar()}
                focusCancelBtn/>
        ) : (
            null
        )}


        {editarAbierto == true ? (
            <Modal open={editarAbierto}
                    closeOnOverlayClick={false}
                    classNames={{
                        modal : `rounded w-75 light-grey-handbook`
                    }} 
                    animationDuration={400}
                    center
                    onClose={handleModalEditar}>

                <div>
                        <Formik 
                            initialValues={{ notes : "" }}
                            validationSchema={favoriteScheme}
                            onSubmit={async(values) => {submitEditar(values)}}> 
                            <Form>
                                <div className='row'>
                                    <div className='col-md-12'>
                                        <label className='mb-2'> Digite la nueva nota de la señal de referencia</label>
                                    </div>
                                </div>
                                <div className='row'>
                                    <div className='col-md-8'>
                                        <Field as="textarea" id='notes' name='notes' className='w-100 form-control shadow-sm mt-2' 
                                                    placeholder="Escriba aqui...." onKeyUp={handleNotaChange}/>
                                        <ErrorMessage name='notes' component='div' className='dark-grey-handbook p-2 mt-2 mb-2 text-white rounded text-center fw-bold'/>
                                    </div>
                                    <div className='col-md-4'>
                                        <button type='submit' className='h-100 dark-red-handbook text-white w-100 btn mt-1'>
                                            <FaSave/>
                                            <span className='px-1'>Guardar</span>
                                        </button>
                                    </div>
                                </div>
                            </Form>
                        </Formik>
                </div>

            </Modal>
        ): (
            null
        )}

        <div className='col-md-12 col-lg-4 text-center mt-3'>
            <div className='px-4 py-4 shadow rounded light-grey-handbook mb-3'>
                <audio className='shadow-sm rounded-pill mt-2 w-100 ' controls>
                    <source src={senal.reference} type='audio/mpeg'/>
                </audio>
                <p className='mt-3'>
                    <b>Fecha referencia: </b> {senal.createdAt.split('T')[0]}
                </p>
                <p>
                    <b>Nota sobre la señal: </b>  {senal.notes}
                </p>
                
                <div className='row'>
                    <div className='col-md-6'>
                        <button className='mid-grey-handbook p-3 btn w-100  fw-bold mt-2' onClick={() => handleModalEliminar(senal.id)}>
                            <i className='fa fa-trash px-2'/>
                            Eliminar referencia
                        </button>
                    </div>
                    <div className='col-md-6'>
                        <button className='mid-grey-handbook p-3 btn w-100  fw-bold mt-2' onClick={() => handleModalEditar(senal.id)}>
                            <i className='fa fa-pencil px-2'/>
                            Editar referencia
                        </button> 
                    </div>
                </div>
            </div>
        </div>

        </>
    )
}

export default Favorito