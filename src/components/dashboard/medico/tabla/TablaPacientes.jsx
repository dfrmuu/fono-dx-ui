import {useEffect, useState} from 'react'
import {Paciente} from './filas-tabla/FilasTabla'
import {ToastContainer,toast} from 'react-toastify'
import { useNavigate, useOutletContext } from 'react-router-dom';
import { Form, Field, Formik, ErrorMessage} from 'formik'
import { FaAngleLeft, FaAngleRight } from 'react-icons/fa';
import * as Yup from 'yup'
import Cookies from 'universal-cookie';
import axios from 'axios'
import ModalSeñales from './ModalSeñales'
import 'react-toastify/dist/ReactToastify.css';
import SpinnerHeart from '../../../SpinnerHeart';
import EditarPaciente from '../formularios-paciente/EditarPaciente';

//Componente para renderizar la tabla de pacientes
const TablaPacientes = () => {

    //Función para redireccionar
    const navigate = useNavigate()

    //Variables con datos del usuario
    const [user,rol,permisos] = useOutletContext()

    //Variable pacientes
    const [pacientes,setPacientes] = useState([])

    //Variable pacientes (para paginación)
    const [currentPacientes,setCurrentPacientes] = useState([])

    //Variable para el control del estado del modal de señales (abierto-cerrado)
    const [modalSeñales,setModalSeñales] = useState([])

    //Variable para el spinner de carga
    const [cargando,setCargando] = useState(false)

    //Variable para los filtros
    const [criterioSeleccionado,setCriterioSeleccionado] = useState("")
    const [dato, setDato] = useState("")

    //MODAL SEÑALES
    const [modalSenales, setModalSenales] = useState(false);

    //EDITAR PACIENTE
    const [idPaciente,setIdPaciente] = useState("")
    const [modalEditar,setModalEditar] = useState(false)

    //EDITAR SEÑAL
    const [idPacienteModal,setIdPacienteModal] = useState("")
    const [fechaModal,setFechaModal] = useState("")

    //PAGINACION
    const [currentPage, setCurrentPage] = useState(1);
    const itemsPerPage = 10;

    // Calcula los índices y filtra los equipos para la página actual
    const indexOfLastItem = currentPage * itemsPerPage;
    const indexOfFirstItem = indexOfLastItem - itemsPerPage;

    //Datos API
    const VITE_API_LINK = import.meta.env.VITE_API_LINK
    const cookies = new Cookies()

    //Consulta los pacientes cuando se renderiza el componente
    useEffect(() => {
        misPacientes()
    }, [])

    // Función para cambiar de página
    const handlePageChange = (pageNumber) => {
        if (pageNumber >= 1 && pageNumber <= Math.ceil(pacientes.length / itemsPerPage)) {
          setCurrentPage(pageNumber);
  
          // Calcular los índices y actualizar currentMarcas para reflejar la página actual
          const newIndexOfLastItem = pageNumber * itemsPerPage;
          const newIndexOfFirstItem = newIndexOfLastItem - itemsPerPage;
          setCurrentPacientes(pacientes.slice(newIndexOfFirstItem, newIndexOfLastItem));
        }
    };


    //Trae los pacientes del API
    const misPacientes = async () => {
        setCargando(true)

        const tokenAcceso = cookies.get("token")

        await axios.get(VITE_API_LINK + "paciente/mis-pacientes",{
            headers: {
                Authorization: `Bearer ${tokenAcceso}`,
                ClientAuth : import.meta.env.VITE_APIKEY,
                Resource : 'pacientes',
                Action : 'listar'
            },
            timeout : 10000
        })
        .then((response) => {

            //Toma los datos en el array
            const pacientes = response.data[0].patients

            //Crea una copia
            const pacientesAPI = [...pacientes]

            //Agrega los pacientes a las variables
            setPacientes(pacientesAPI)
            setCurrentPacientes(pacientesAPI)

        }).catch((error) => {
            toast.error(error.response.data.Error)
            setPacientes([])
            setCurrentPacientes([])

        }).finally(() => {
            //Termina el spinner de carga
            setTimeout(() => {
                setCargando(false)
            }, 500);
        })

    } 

    //Handle para los datos de filtro

    const handleCriterioChange = (e) => {
        setCriterioSeleccionado(e.target.value)
    }

    const handleDatoChange = (e) => {
        setDato(e.target.value)
    }

    const handleSubmit = () => {
        filtrarPacientes()
    }


    //Filtra los pacientes
    const filtrarPacientes = async() => {
        setCargando(true)

        const tokenAcceso = cookies.get("token")

        await axios.post(VITE_API_LINK + "paciente/filtrar",{
            dato : dato,
            criterio : criterioSeleccionado
        },{
            headers: {
                Authorization: `Bearer ${tokenAcceso}`,
                ClientAuth : import.meta.env.VITE_APIKEY,
                Resource : 'pacientes',
                Action : 'listar'
            },
            timeout : 10000
        })
        .then((response) => {

            //¿Hay datos?
            if(response.data[0].patients.length == 0){
                toast.error("No hay datos según su consulta")
            } else {
                setCurrentPacientes(response.data[0].patients)
            }

        }).catch((error) => {
            console.log(error)
            setCurrentPacientes([])
            toast.error(error.response.data.Error)
        }).finally(() => {
            setTimeout(() => {
                setCargando(false)
            }, 500);
        })

    }
  

    //Estructura del formulario de filtro
    const SearchScheme = Yup.object().shape({
        criterio : Yup.string()
                      .required("El criterio de busqueda es requerido")
                      .oneOf(["name","ci","phone","email"],"El criterio de busqueda es requerido"),
        
        search : Yup.string()
                  .required("El dato de busqueda es requerido"),
    })


    //Limpia la busqueda (filtros)
    const limpiarBusqueda = () => {
        misPacientes()
        setCriterioSeleccionado("")
        setDato("")
    }

    //Ajusta las filas a partir del paciente en consulta
    const ocultarFilas = (id) => {

        const arrayAjustado = pacientes.filter(paciente => 
            paciente.id == id
        )
    
        const pacientesAjustados = [...arrayAjustado]
        setCurrentPacientes(pacientesAjustados);
    }

    //Handle para el modal de edición de paciente (abierto-cerrado)
    const handleEditarPaciente = (id) => {
        if(modalEditar == true){
            setModalEditar(false)
            setIdPaciente("")
        }else{
            setModalEditar(true)
            setIdPaciente(id)
        }
    }



    //Handle para el modal de edición de señal (abierto-cerrado)
    const handleEditarSenal = (id,fecha) => {
        if(modalSenales == false){
            setModalSenales(true)
            setIdPacienteModal(id)
            setFechaModal(fecha)
        }else{
            setModalSenales(false)
            setFechaModal("")
            setIdPacienteModal("")
        }
    }


    return (
        <>
            <label className='mt-3 mb-3 px-2 fw-bold'> Dato del paciente: </label>

            <Formik enableReinitialize={true}
                    initialValues = {{criterio : criterioSeleccionado, search : dato }}
                    onSubmit ={handleSubmit}
                    validationSchema={SearchScheme}>

                    <Form>
                        <div className='row px-2'>
                                <div className='col-md-6 col-lg-4'>
                                    <Field type="text" id='search' name='search' className='w-100 form-control shadow-sm mt-1 p-3' 
                                            placeholder="Ingrese los criterios a buscar" onKeyUp={handleDatoChange}/>
                                    <ErrorMessage name='search' component='div' className='bg-danger p-2 text-white rounded text-center fw-bold'/>
                                </div>
                                <div className='col-md-6 col-lg-4'>
                                    <Field as='select' className='form-select text-center  shadow-sm p-3 mt-1' 
                                            id='criterio' name='criterio' value={criterioSeleccionado}
                                            onChange = {handleCriterioChange}>
                                        <option value=''> -- Filtrar por -- </option>
                                        <option value='name'> Nombre </option>
                                        <option value='ci'> Número documento </option>
                                        <option value='phone'> Número de teléfono </option>
                                        <option value='email'> Correo </option>
                                    </Field>
                                    <ErrorMessage name='criterio' component='div' className='dark-grey-handbook mb-2 mt-2 p-2 text-white rounded text-center fw-bold'/>
                                </div>
                                <div className='col-md-6 col-lg-2'>
                                    <button className='btn dark-red-handbook w-100 text-white mt-1 p-3' type='submit'>
                                        <i className='fa fa-search px-1'></i>
                                        Buscar
                                    </button>
                                </div>

                                <div className='col-md-6 col-lg-2'>
                                    <button className='btn dark-red-handbook w-100 text-white p-3 mt-1' type='button' onClick={() => limpiarBusqueda()}>
                                        <i className="fa-solid fa-hand-sparkles px-1"></i>
                                        Limpiar busqueda
                                    </button>
                                </div>
                        </div>
                </Form>
            </Formik>

            <br/>
            <hr/>

            <div className="position-absolute bottom-0 end-0 m-5 z-3">
                {permisos.some(permiso => permiso.recurso === "pacientes" && permiso.accion === "crear") ? (
                    <div title='Registrar paciente'>
                        <button className='btn dark-red-handbook rounded-circle text-white p-3 mt-1' onClick={() => navigate("/index/medico/registro-pacientes")}>
                            <i className="fa-solid fs-1 fa-users"></i>
                        </button>
                    </div>
                )  : (
                    null
                )}
            </div>

            {modalSenales == true ? (
                <ModalSeñales idPaciente={idPacienteModal}
                              fecha = {fechaModal}
                              abierto={modalSenales}
                              handleCerrar={handleEditarSenal}/>
            ) : (
                null
            )}


            {modalEditar == true ? (
                <EditarPaciente abierto={modalEditar}
                                traerMisPacientes={misPacientes}
                                handleModal={handleEditarPaciente}
                                id={idPaciente}/>
            ) : (
                null
            )}


            {cargando == true ? (
                <SpinnerHeart height={500} width={500}/>
            ) : (
                pacientes.length > 0 ? (
                <>
                    <div className='mt-5 table-responsive px-2'>
                        <table className="table table-borderless text-center rounded-1 border overflow-hidden">
                            <thead className='dark-grey-handbook text-white'>
                                <tr className='align-middle rounded-start'>
                                    <th className='tall-cell' scope="col">Ver</th>
                                    <th className='tall-cell' scope="col">Paciente</th>
                                    <th className='tall-cell' scope="col">Edad</th>
                                    <th className='tall-cell' scope="col">Clasificación inicial</th>
                                    <th className='tall-cell' scope="col">Foco aortico</th>
                                    <th className='tall-cell' scope='col'>Acciones</th>
                                </tr>
                            </thead>                                
                            <tbody>
                                    {currentPacientes.map( paciente => <Paciente key={paciente.id}
                                                                                paciente={paciente} 
                                                                                setModalSeñales={setModalSeñales}
                                                                                ocultarFilas={ocultarFilas}
                                                                                limpiarBusqueda = {limpiarBusqueda}
                                                                                handleEditarSenal = {handleEditarSenal}
                                                                                handleEditarPaciente= {handleEditarPaciente}/>)}
                            </tbody>
                        </table> 
                    </div>
                            
                    <br/>


                    {currentPacientes.length >= 10 ? (
                        <div className='p-3'>
                            <hr/>
                            <div className='row'> 
                                <div className='col-md-4'>
                                    <div className='d-flex justify-content-center'>
                                        <button className={`btn rounded-circle fw-bold light-grey-handbook-btn shadow  mb-2`} type='button'
                                                onClick={() => handlePageChange(currentPage - 1)} disabled={currentPage === 1}>
                                            <FaAngleLeft/>
                                        </button>
                                    </div>
                                </div>
                                <div className='col-md-4'>
                                    <div className='d-flex justify-content-center align-middle'>
                                        <p className={`fw-bold dark-red-handbook-text mt-2`}>Página {currentPage}</p>
                                    </div>
                                </div>
                                <div className='col-md-4'>
                                    <div className='d-flex justify-content-center'>
                                        <button className={` btn rounded-circle fw-bold light-grey-handbook-btn shadow  mb-2`} type='button'
                                                onClick={() => handlePageChange(currentPage + 1)} disabled={indexOfLastItem >= pacientes.length}>
                                            <FaAngleRight/>
                                        </button>
                                    </div>
                                </div>  
                            </div>
                        </div>
                    ) : (
                        null
                    )}  

                </>
                ) : (
                    <div className='row'>
                            <div className='col-md-12'>
                                <div className='gray-bg p-3 mt-5 fw-bold rounded text-center'>
                                    Actualmente no hay pacientes registrados.
                                </div>
                            </div>
                    </div>
                )
            )}
        </>
    )
}

export default TablaPacientes