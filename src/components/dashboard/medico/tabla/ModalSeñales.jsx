import {useState,useEffect} from 'react'
import { Favoritos } from '../tabla/Tabla.js'
import { Modal } from 'react-responsive-modal';
import { FaEdit, FaEye, FaQuestionCircle, FaStar, FaTimesCircle, FaTrash } from 'react-icons/fa';
import { Resultados } from './filas-tabla/FilasTabla.js'
import { toast } from 'react-toastify';
import { EditarSenal } from '../../medico/formularios-senal/FormulariosSenal.js'
import SpinnerHeart from '../../../SpinnerHeart.jsx';
import Cookies from 'universal-cookie';
import SweetAlert from 'react-bootstrap-sweetalert';
import axios from 'axios';

const ModalSeñales = ({handleCerrar,abierto,idPaciente,fecha}) => {

    const [cargando,setCargando] = useState(false)

    const [abierta,setAbierta] = useState(false)
    const [añadiendoFavorito,setAñadiendoFavorito] = useState(false)
    const [señal, setSeñal] = useState("")
    const [senales,setSenales] = useState([])
    
    //CONSULTA RESULTADOS
    const [idSenal,setIdSenal] = useState("")
    const [resultados,setResultados] = useState([])

    //EDITAR AUSCULTACIÓN / ELIMINAR AUSCULTACIÓN
    const [idEditar,setIdEditar] = useState("")
    const [modalEditarSenal,setModalEditarSenal] = useState(false)

    const [idEliminar,setIdEliminar] = useState("")
    const [alertaEliminar,setAlertaEliminar] = useState(false)

    useEffect(() => {
      traerSenalesFecha()
    }, [])
    

        
    useEffect(() => {
      if(abierta == true){
        consultarResultados()
      }
    }, [abierta])
    


    const traerSenalesFecha = async () => {

      setCargando(true)
      const tokenAcceso = cookies.get("token")
      console.log(fecha)

      await axios.post(VITE_API_LINK + `senal/paciente/fecha`, {
          fecha_creacion: fecha,
          idPaciente : idPaciente

      },{
          headers: {
              Authorization: `Bearer ${tokenAcceso}`,
              ClientAuth : import.meta.env.VITE_APIKEY,
              Resource : 'auscultacion',
              Action : 'listar'
          },
          timeout : 10000          
      }).then((response) => {
          console.log(response)
          setSenales(response.data)
      }).catch((error) => {
          console.log(error)
          toast.error(error.response.data.Error)
      }).finally(() => {
        setTimeout(() => {
            setCargando(false)
        }, 500);
      })
    }


    //API 
    const cookies = new Cookies()
    const VITE_API_LINK = import.meta.env.VITE_API_LINK



    const mostrarDiagnosticos = (id) => {
        if(abierta == false ){
          setAbierta(true)
          ocultarFilas(id)
          setIdSenal(id)
        }else{
          setAbierta(false)
          consultarSeñales()
          setIdSenal("")
        }
    }

    const ventanaFavorito = (id) => {
        if(añadiendoFavorito == false){
          setSeñal(id)
          ocultarFilas(id)
          setAñadiendoFavorito(true)
        }else{
          setSeñal("")
          consultarSeñales()
          setAñadiendoFavorito(false)
        }
    }

    const handleEditarAuscultacion = (id) => {
      if(modalEditarSenal == true){
          setModalEditarSenal(false)
          setIdEditar("")
          traerSenalesFecha()
      }else{
          setModalEditarSenal(true)
          setIdEditar(id)
          ocultarFilas(id)
      }
    }

    const handleEliminarAuscultacion = (id) => {
      if(alertaEliminar == true){
          setAlertaEliminar(false)
          setIdEliminar("")
      }else{
          setAlertaEliminar(true)
          setIdEliminar(id)
      }
    }


    const ocultarFilas = (id) => {
      const senalesFilter = senales.filter(senal => 
          senal.id == id
      )

      const senalesAjustadas = [...senalesFilter]
      setSenales(senalesAjustadas)
    }


    const consultarResultados = async() => {
        try {
          setCargando(true)
          const tokenAcceso = cookies.get("token")

          await axios.get(VITE_API_LINK + `resultados/senal/${idSenal}`, {
              headers: {
                  Authorization: `Bearer ${tokenAcceso}`,
                  ClientAuth : import.meta.env.VITE_APIKEY,
                  Resource: 'auscultacion',
                  Action : 'listar'
              },
              timeout : 10000
          })
          .then((response) =>{
              console.log(response.data)
              setResultados(response.data)
          }).catch((error) => {
              toast.error(error.response.data.Error)
              setAbierta(false)
              traerSenalesFecha()
          }).finally(() => {
              setTimeout(() => {
                  setCargando(false)
              }, 500);
          })
        } catch (error) {
          console.log(error)
      }
    }

    const eliminarAuscultacion = async() => {
        try {
          
          const tokenAcceso = cookies.get("token")

          await axios.delete(VITE_API_LINK + `senal/eliminar/${idEliminar}`, {
            headers: {
                  Authorization: `Bearer ${tokenAcceso}`,
                  ClientAuth : import.meta.env.VITE_APIKEY,
                  Resource: 'auscultacion',
                  Action : 'eliminar'
              },
              timeout : 10000
          })
          .then((response) =>{
              toast.info(response.data.Message)
              traerSenalesFecha()
              handleEliminarAuscultacion()
          }).catch((error) => {
              toast.error(error.response.data.Error)
          })

        } catch (error) {
          console.log(error)
        }
    }

    return (

      <>
            <Modal open={abierto}
                   closeOnOverlayClick={false}
                    classNames={{
                          modal : `rounded mid-grey-handbook`
                    }} 
                    animationDuration={400}
                    center
                    onClose={handleCerrar}>

                    <div>

                        <h3 className='text-center fw-bold mt-4'>
                          LISTADO DE SEÑALES
                        </h3>

                        <hr/>


                        {alertaEliminar == true ? (
                            <SweetAlert warning
                                        showCancel
                                        confirmBtnText="Si"
                                        confirmBtnBsStyle="success"
                                        cancelBtnText="No"
                                        cancelBtnBsStyle='danger'
                                        title="¿Deseas eliminar esta auscultación?"
                                        onCancel={handleEliminarAuscultacion}
                                        onConfirm={eliminarAuscultacion}
                                        focusCancelBtn>
                            </SweetAlert>
                        ) : (
                            null
                        )}
                
                        <div className='px-1 rounded mt-2'>
                            <p className='alert dark-red-handbook text-white text-center'>
                              <FaQuestionCircle/>
                              <span className='ms-2'>Puede que algunos cambios solo se muestren al volver a consultar la señales.</span>
                            </p>
                        </div>

                        {cargando == true ? (
                            <SpinnerHeart height={500} width={500}/>
                        ) : (                        

                          senales.length > 0 ? (
                              senales.map((senal) => 
                  
                              <div className='row' key={senal.id}>
                                <div className='col-md-12 mt-3'>
                                    <div className="card light-grey-handbook shadow-sm">
                                      <div className="card-body">
                                        <div className='row mb-2'>
                                            <div className='col-md-12'>
                                              <h6 className="card-subtitle mt-2 text-muted">Foco aortico: <b>{senal.focus}</b></h6><br/>
    
                                              {senal.status == "procesada" ? (
                                                <h6 className="card-subtitle mb-2 text-muted">Estado de la señal: 
                                                  <b className='text-uppercase ms-1'>
                                                    PROCESADA (YA TIENE RESULTADOS)
                                                  </b>
                                                </h6>
                                              ) : (
                                                <h6 className="card-subtitle mb-2 text-muted">Estado de la señal: <b className='text-uppercase'>{senal.status}</b></h6>
                                              )}
                                            </div>
                                        </div>
                                        <div className='row'>
                                            <div className='col-md-12'>
                                              <audio className='shadow-sm rounded-pill mt-2 w-100 ' controls>
                                                <source src={senal.reference} type='audio/mpeg'/>
                                              </audio>
                                            </div>
                                        </div>
                                        <div className='row mt-2'>
    
                                            {senal.favorite_signals != undefined && senal.favorite_signals.length > 0 ? (
                                              <div className='col-lg-6 col-md-12'>
                                                <button className="shadow-sm btn-gray fw-bold rounded btn w-100 text-danger" type='button'>
                                                  <i className='fa fa-check px-2'></i>
                                                  Esta señal ya es referencia
                                                </button>
                                              </div>
                                            ) : (
                                              <div className='col-lg-6 col-md-12'>
                                                <button className="w-100 p-2 mid-grey-handbook shadow-sm fw-bold mt-1 rounded btn w-100" type='button' onClick={() => ventanaFavorito(senal.id)}>
                                                  <FaStar/>
                                                  Agregar como referencia
                                                </button>
                                              </div>
                                            )}
    
                                            <div className='col-lg-6 col-md-12'>
                                              <button className='w-100 p-2 dark-red-handbook btn rounded mt-1  text-white'  type='button' onClick={() => mostrarDiagnosticos(senal.id)}>
                                                  <FaEye/>
                                                  Mostrar resultados
                                              </button>
                                            </div>
    
                                            <div className='col-lg-6 col-md-6'>
                                              <button className='w-100 p-2 dark-red-handbook btn rounded mt-1  text-white'  type='button' onClick={() => handleEditarAuscultacion(senal.id)}>
                                                  <FaEdit/>
                                                  Editar
                                              </button>
                                            </div>
    
                                            <div className='col-lg-6 col-md-6'>
                                              <button className='w-100 p-2 dark-red-handbook btn rounded mt-1  text-white' onClick={() => handleEliminarAuscultacion(senal.id)}>
                                                  <FaTrash/>
                                                  Eliminar
                                              </button>
                                            </div>
    
                                        </div>
    
                                        <Favoritos id={señal} añadiendoFavorito={añadiendoFavorito} setAñadiendoFavorito={setAñadiendoFavorito} ventanaFavorito={ventanaFavorito} />
            
                                        </div>
                                      </div>
                                  </div>
    
                                  {abierta == true ? (
                                    <div className='col-md-12 mt-3'>
                                      <div className="card shadow-sm light-grey-handbook p-3">
                                        <h6 className="card-subtitle mb-2 mt-3 text-muted">Análisis de resultados en la señal</h6>
                                        <hr/>
    
                                        {cargando == true ? (
                                              <SpinnerHeart height={500} width={500}/>
                                        ) : (
    
                                          resultados.length > 0 ? (
                                            resultados.map((resultado) => (
                                              <Resultados resultado={resultado} key={resultado.id}/>
                                            ))
                                          ) : (
                                            <div className='row'>
                                                    <div className='col-md-12'>
                                                        <div className='bg-light p-3  fw-bold rounded text-center shadow'>
                                                            <FaTimesCircle/>
                                                            <span className='px-2'>Actualmente no hay resultados.</span>
                                                        </div>
                                                    </div>
                                            </div>
                                          )
                                        )}
    
                                      </div>
                                    </div>
                                  ) : (
                                    null
                                  )}
    
    
                                  {modalEditarSenal == true ? (
                                    <EditarSenal id={idEditar} 
                                                traerSenales={traerSenalesFecha}/>
                                  ) : (
                                    null
                                  )}
    
                              </div>
                            )
                          ) : (
                            <div className='px-1 rounded mt-2'>
                                <p className='alert dark-red-handbook text-white text-center'>
                                  <span className='ms-2'>Al parecer no hay auscultaciones para este paciente.</span>
                                </p>
                            </div>
                          )
                      )}
                    </div> 
            </Modal>
      </> 
  
    )
}

export default ModalSeñales