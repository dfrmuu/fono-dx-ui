import {useState,useEffect} from 'react'
import { Form, Field, Formik, ErrorMessage} from 'formik'
import { toast } from 'react-toastify';
import { DateTime } from 'luxon';
import * as Yup from 'yup'
import axios from 'axios';
import Cookies from 'universal-cookie';
import { FaSave } from 'react-icons/fa';

//Componente para añadir una senal como favorito
const Favoritos = ({id,añadiendoFavorito,setAñadiendoFavorito,ventanaFavorito}) => {

    //Variable para la nota del favorito / señal de referencia
    const [nota,setNota] = useState("")

    //Recupera las cookies del navegador
    const cookies = new Cookies()

    //Datos del API
    const VITE_API_LINK = import.meta.env.VITE_API_LINK

    //Handle para la digitación de la nota
    const handleNotaChange = (e) => {
        setNota(e.target.value)
    }

    //Estructura de la señal
    const senalScheme = Yup.object().shape({
        notes : Yup.string()
                   .max(200),
    })

    //Envia los datos para agregar un favorito
    const agregarFavorito = async (values) => {

        //Recupera el token
        const tokenAcceso = cookies.get("token")

        //Recupera la id de la señal y los datos de la nota
        const SignalID = id
        const {notes} = values

        //Envia los valores
        await axios.post(VITE_API_LINK + "favoritos/crear", {
            SignalID,
            notes,
        },{
            headers: {
                Authorization: `Bearer ${tokenAcceso}`,
                ClientAuth : import.meta.env.VITE_APIKEY
            },
            timeout : 3000
        }).then((response) => {
            toast.info(response.data.Message)
        }).catch((error) => {
            toast.error(error.response.data.Error)
        })

        setAñadiendoFavorito(false)
        ventanaFavorito(id)
    }
        
    return (
        <>
            {añadiendoFavorito == true ? (

            <div className='row mt-2'>
                <div className='col-md-12'>
                    <div className='py-3 px-3  rounded'>
                        <hr/>
                        <Formik 
                            initialValues={{ notes : "" }}
                            validationSchema={senalScheme}
                            onSubmit={async(values) => {agregarFavorito(values)}}> 
                            <Form>
                                <div className='row'>
                                    <div className='col-md-12'>
                                        <label className='mb-2'> ¿Desea poner alguna nota sobre el audio?</label>
                                    </div>
                                </div>
                                <div className='row'>
                                    <div className='col-lg-8 col-md-12'>
                                        <Field as="textarea" id='notes' name='notes' className='w-100 h-100 form-control shadow-sm mt-1' 
                                                    placeholder="Digite la nota" onKeyUp={handleNotaChange}/>
                                        <ErrorMessage name='notes' component='div' className='dark-red-handbook p-2 text-white rounded text-center fw-bold'/>
                                    </div>
                                    <div className='col-lg-4 col-md-12'>
                                        <button type='submit' className='h-100 dark-red-handbook mt-1 text-white w-100 btn'>
                                            <FaSave/>
                                            <span className='px-1'>Guardar</span>
                                        </button>
                                    </div>
                                </div>
                            </Form>
                        </Formik>
                    </div>
                </div>
            </div>     

            ) : null}
        </>

    )
}

export default Favoritos