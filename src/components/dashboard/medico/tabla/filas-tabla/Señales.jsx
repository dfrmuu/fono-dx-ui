import { useState,useEffect } from 'react'
import { FaChevronDown, FaEdit, FaTrash } from 'react-icons/fa';
import { v4 as uuidv4 } from 'uuid';

//Mapea las señales en la tabla de pacientes
const Señales = ({señales,handleEditarSenal, paciente}) => {

    //Recupera la id del paciente
    const {id} = paciente


    return (
        <>
            {señales.length > 0 ? (
                
                señales.map(senal => 
                    <tr key={senal.id} className='align-middle mid-grey-handbook'>
                        <td className='dark-grey-handbook tall-cell' role="button"  onClick={() => handleEditarSenal(id,senal.createdAt)}>
                            <FaChevronDown className='dark-red-handbook-text mt-1'/>
                        </td>
                        <td className='tall-cell'>
                            {paciente.user.name + " " + paciente.user.last_name}
                        </td>
                        <td className='tall-cell'>
                            {senal.createdAt.split('T')[0]}
                        </td>
                    </tr>
                )

            ) : (

                <tr key={1}> 
                    <td className='mid-grey-handbook  p-4 w-100' colSpan={3}>
                        No hay señales para este paciente 
                    </td>
                </tr>
            )} 
        </>  
    )
    
}

export default Señales