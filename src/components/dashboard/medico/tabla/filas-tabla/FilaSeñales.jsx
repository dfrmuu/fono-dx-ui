import {Señales} from './FilasTabla'
import {useEffect, useState} from 'react'
import { Field, Form, Formik, ErrorMessage } from 'formik'
import * as Yup from 'yup'
import Cookies from 'universal-cookie'
import axios from 'axios'
import { toast } from 'react-toastify'
import { DateTime } from 'luxon'
import { FaHandsWash, FaSearch } from 'react-icons/fa'

//Componente para construir la fila de la tabla de señales
const FilaSeñales = ({señales,setSeñales,abierta,paciente,handleEditarSenal}) => {

    //Variable para la fecha de consulta (filtro)
    const [fecha,setFecha] = useState('')

    //Variable para las fechas de consulta disponibles
    const [fechas,setFechas] = useState([])

    //Copia del array de señales principales
    const [señalesCopia,setSeñalesCopia] = useState([])

    //Recupera las cookies del navegador
    const cookies = new Cookies()

    //Datos del API
    const VITE_API_LINK = import.meta.env.VITE_API_LINK

    //Se ejecuta apenas carga el componente
    useEffect(() => {

        //Saca una copia de la señales
        const copia = señales.slice()
        setSeñalesCopia(copia)

        consultaFechasAuscultaciones()
    }, [])
    

    //Estructura de validación para los filtros
    const SearchScheme = Yup.object().shape({
        fecha : Yup.date()
                   .required("La fecha es requerida para la consulta.")
                   
    })

    //Recupera las fechas disponibles en el API
    const consultaFechasAuscultaciones = async() => {
    
        const tokenAcceso = cookies.get("token")

        await axios.post(VITE_API_LINK + `senales/fechas`, {idPaciente : paciente.id} , {
            headers: {
                Authorization: `Bearer ${tokenAcceso}`,
                ClientAuth : import.meta.env.VITE_APIKEY,
                Resource: 'auscultacion',
                Action : 'listar'
            },
            timeout : 10000
        })
        .then((response) =>{
            setFechas(response.data)
        }).catch((error) => {
            setFechas([])
            toast.error(error.response.data.Error)
        })
    }


    //Función para filtrar señales / auscultaciones
    const handleSubmit = async() => {

        //Recupera el token en las cookies
        const tokenAcceso = cookies.get("token")

        //Trae los datos
        await axios.post(VITE_API_LINK + "senales/filtrar",{
            idPaciente : paciente.id,
            fecha : fecha
        },{
            headers: {
                Authorization: `Bearer ${tokenAcceso}`,
                ClientAuth : import.meta.env.VITE_APIKEY,
                Resource : 'auscultacion',
                Action : 'listar'
            },
            timeout : 10000
        })
        .then((response) => {
            setSeñales(response.data)
        }).catch((error) => {
            toast.error(error.response.data.Error)
        })
    }

    //Controla los datos de digitación
    const handleFecha = (e) => {
        setFecha(e.target.value)
    }

    //Limpia la busqueda de señales
    const limpiarBusqueda = () => {
        setSeñales(señalesCopia)
        setFecha('')
    }

    return (

            <>
                {abierta == true ? 
                    <tr className='light-grey-handbook border-top border-2'> 
                            <td colSpan="9" className='p-4'>   
                                <div className='rounded mid-grey-handbook shadow-sm py-2 mb-3 px-3 py-3'>

                                    <Formik validationSchema={SearchScheme}
                                            onSubmit = {async() => {
                                                    handleSubmit()
                                                }
                                            }

                                            enableReinitialize={true}
                                            
                                            initialValues = {{
                                                fecha : fecha
                                            }}>

                                            <Form>
                                                <div className='row mt-2'>
                                                    <div className='col-lg-2 col-md-6'>
                                                        <p className='fw-bold mt-3'>
                                                            Fechas para filtrar:
                                                        </p>
                                                    </div>
                                                    <div className='col-lg-6 col-md-6'>
                                                        <Field as="select" id="fecha" name="fecha" className="form-control p-3 mt-1 shadow-sm text-center " onChange={handleFecha}>
                                                            <option value="">Seleccione..</option>
                                                            {fechas.map((fecha) => (
                                                                <option>{fecha.createdAt}</option>
                                                            ))}
                                                        </Field>
                                                        <ErrorMessage name='fecha' component='div' className='dark-red-handbook p-2 text-white rounded text-center fw-bold'/>
                                                    </div>
                                                    <div className='col-lg-2 col-md-6'>
                                                        <button className='dark-red-handbook text-white p-3 mt-1 w-100 btn' type='submit'>
                                                            <FaSearch className='text-white'/> 
                                                            <span className='px-2'>Buscar </span>
                                                        </button>
                                                    </div>
                                                    <div className='col-lg-2 col-md-6'>
                                                        <button className='dark-red-handbook text-white p-3 mt-1 w-100 btn' type='button' onClick={() => limpiarBusqueda()}>
                                                            <FaHandsWash className='text-white'/> 
                                                            <span className='px-2'>Limpiar busqueda </span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </Form>

                                    </Formik>

                                </div>
                
                                <div className='table-responsive'>
                                    <table className="table table-borderless w-100 rounded-2 overflow-hidden">
                                            <thead>
                                                <tr className='dark-grey-handbook align-middle'>
                                                    <th>*</th>
                                                    <th className='tall-cell' scope="col">Paciente</th>
                                                    <th className='tall-cell'  scope="col">Fecha</th>
                                                 </tr>
                                            </thead>
                                            <tbody>
                                                <Señales key={señales.id}
                                                    señales={señales}
                                                    handleEditarSenal={handleEditarSenal}
                                                    paciente={paciente}/>
                                            </tbody>
                                    </table>  
                                </div>
                            </td>
                    </tr>   


                    :null                   
                }
            </>


  )
}

export default FilaSeñales