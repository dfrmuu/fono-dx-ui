export {default as Paciente} from './Paciente'
export {default as Señales} from './Señales'
export {default as FilaSeñales} from './FilaSeñales'
export {default as Resultados} from './Resultados'