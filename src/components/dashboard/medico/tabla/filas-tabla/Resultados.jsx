import { DateTime } from 'luxon'
import { useState } from 'react'
import { FaEnvelope, FaFileDownload, FaWaveSquare } from 'react-icons/fa'
import { useEffect } from 'react';
import Cookies from 'universal-cookie';
import jsPDF from 'jspdf';
import LogoFono from '../../../../../assets/Logo_FonoDX.png'
import axios from 'axios';
import { toast } from 'react-toastify';

//Componente para presentar los resultados de una auscultación
const Resultados = ({resultado, origen}) => {

  //Hace destructuring a los valores de resultado
  const {createdAt,diagnostic, tp,fp,fn,tn,sensivility,specificity, vpn, vpp,signal, disease} = resultado

  //Variable para el nombre del paciente
  const [nombrePaciente,setNombrePaciente] = useState("")

  //Recupera las cookies del navegador
  const cookies = new Cookies()

  //Datos del API
  const VITE_API_LINK = import.meta.env.VITE_API_LINK

  //Se ejecuta cuando carga el componente
  useEffect(() => {
    //Revisa los valores de resultado
    if(Object.keys(resultado).length > 0){
        var nombreCompleto = signal.patient.user.name +  " " + signal.patient.user.last_name
        setNombrePaciente(nombreCompleto)
    }
  },[resultado])

  //Función para descargar los resultados en pdf
  const descargarResultados = () => {
    const doc = new jsPDF();

    // Agrega negrita al diagnóstico
    doc.setFont('helvetica', 'normal');
    const img = new Image();
    img.src = LogoFono;
    doc.addImage(img, 'JPEG', 10, 5, 40, 30);
  
    doc.setFontSize(16);
  
    // Centra el título en la página
    const pageTitle = 'ANÁLISIS DE RESULTADOS';
    const titleWidth = doc.getStringUnitWidth(pageTitle) * doc.internal.getFontSize() / doc.internal.scaleFactor;
    const pageWidth = doc.internal.pageSize.width;
    const titleX = (pageWidth - titleWidth) / 2;
    doc.setFont('helvetica', 'bold');
    doc.text(pageTitle, titleX, 32);
    doc.setFont('helvetica', 'normal');
  
    doc.setFontSize(12);



    const fecha = `Fecha de hoy: ${DateTime.local().toFormat("yyyy-MM-dd HH:mm:ss")}`;
    const nombre = `Nombre del paciente: ${nombrePaciente}`
    const TP = `True Positive (TP): ${tp}`;
    const TN = `True Negative (TN): ${tn}`;
    const FP = `False Positive (FP): ${fp}`;
    const FN = `False Negative (FN): ${fn}`;
    const sensibilidad = `Sensibilidad: ${sensivility} %`;
    const especificidad = `Especificidad: ${specificity} %`;
    const VPP = `Valor Predictivo Positivo (VPP): ${vpp} %`;
    const VPN = `Valor Predictivo Negativo (VPN): ${vpn} %`;

    doc.text(fecha, 20, 60);
    doc.text(nombre, 20, 70);

    // Agrega el diagnóstico y un texto adicional debajo
    const diagnosicoX = 20;
    const diagnosicoY = 80;
    const diagnosicoText = parseFloat(diagnostic) > 0.60 ? `Diagnóstico (NO PATOLÓGICO - SALUDABLE) : ${diagnostic} %` : `Diagnóstico (PATOLÓGICO): ${diagnostic} %`;

    doc.setFont('helvetica', 'bold');
    doc.text(diagnosicoText, diagnosicoX, diagnosicoY);

    // Agrega un texto adicional debajo del diagnóstico
    const textoAdicionalX = diagnosicoX;
    const textoAdicionalY = diagnosicoY + 10; // Ajusta la posición vertical del texto adicional
    const textoAdicional =  disease != null ? `Enfermedad diagnosticada: ${disease.toUpperCase()}.` : `Ninguna enfermedad diagnosticada.`
    doc.text(textoAdicional, textoAdicionalX, textoAdicionalY);

    // Añade la línea debajo del texto adicional
    const lineY = textoAdicionalY + 5; // Ajusta la posición vertical de la línea
    doc.setLineWidth(0.5); // Ancho de la línea
    doc.line(diagnosicoX, lineY, diagnosicoX + doc.getStringUnitWidth(diagnosicoText) * 7, lineY); // Ajusta la longitud de la línea

    doc.setFont('helvetica', 'normal');
    doc.text('Lista de resultados generados:', 20, 105);
    doc.text(TP, 20, 115);
    doc.text(TN, 20, 125);
    doc.text(FP, 20, 135);
    doc.text(FN, 20, 145);
    doc.text(sensibilidad, 20, 155);
    doc.text(especificidad, 20, 165);
    doc.text(VPP, 20, 175);
    doc.text(VPN, 20, 185);
  
    // Agrega una sección de observaciones o comentarios
    doc.setFontSize(12);
    // Agrega negrita al diagnóstico
    doc.setFont('helvetica', 'bold');
    doc.text('Observaciones:', 20, 210);
    // Agrega negrita al diagnóstico
    doc.setFont('helvetica', 'normal');
    const observaciones = 'Aquí puedes agregar observaciones adicionales sobre el análisis.';
    doc.text(observaciones, 20, 220);

    //Guarda el documento
    doc.save(`informe_auscultacion_${DateTime.local().toISO()}.pdf`);
  }

  //Función para enviar los resultados al correo del paciente
  const enviarResultados = async () => {
      try {

        //Recupera el token en las cookies
        const tokenAcceso = cookies.get("token")

        //Envia los valores al API
        await axios.post(VITE_API_LINK + `paciente/enviar/resultados`, {
          PatientID : signal.patient.user.id,
          SignalID : signal.id
        } , {
            headers: {
                Authorization: `Bearer ${tokenAcceso}`,
                ClientAuth : import.meta.env.VITE_APIKEY,
                Resource: 'auscultacion',
                Action : 'enviar'
            },
            timeout : 10000
        })
        .then((response) =>{
            toast.info(response.data.Message)
        }).catch((error) => {
            toast.error(error.response.data.Error)
        })
      } catch (error) {
        console.log(error)
      }
  }

  return (
      <div className='light-grey-handbook p-3 shadow-lg rounded border border-secondary mt-2'>

        <div className='d-flex justify-content-end'>
          {origen != null ? (
              <button className='px-2 py-1 m-1 rounded-circle shadow btn dark-red-handbook' title='Descargar reporte resultados' onClick={() => descargarResultados()}>
                <FaFileDownload className='text-white'/>
              </button>
          ) : (
            <>
              <button className='px-2 py-1 m-1 rounded-circle shadow btn dark-red-handbook' title='Enviar resultados al paciente por correo' onClick={() => enviarResultados()}>
                <FaEnvelope className='text-white'/>
              </button>
              <button className='px-2 py-1 m-1 rounded-circle shadow btn dark-red-handbook' title='Descargar reporte resultados' onClick={() => descargarResultados()}>
                <FaFileDownload className='text-white'/>
              </button>
            </>
          )}
        </div>

        <h6 className="mb-2 text-center">Fecha del resultado: <b>{DateTime.fromISO(createdAt).setZone('America/Bogota').toFormat('yyyy-MM-dd HH:mm:ss')}</b></h6>

        <div className='table-responsive'>
        <table className='table light-grey-handbook  table-bordered rounded overflow-hidden'>
          <thead>
              <tr className='text-center'>
                {parseFloat(diagnostic) > 0.60 ? (
                  <td className='fw-bold dark-grey-handbook' colSpan={2}>
                    {`Saludable (NO PATOLÓGICO) - Resultado diagnóstico:  ${diagnostic} %`}
                  </td>
                ) : (
                  <td className='fw-bold dark-red-handbook text-white' colSpan={2}>
                    {`Enfermo (PATOLÓGICO) - Resultado diagnóstico:  ${diagnostic} %`}
                  </td>
                )} 
              </tr>
          </thead>
          <tbody>
              <tr className='align-middle text-center border-bottom'>
                <td colSpan={2} className='mid-grey-handbook'>
                    {parseFloat(diagnostic) > 0.60 ? (
                        <span className='fw-bold'>NINGUNA ENFERMEDAD DIAGNOSTICADA </span>
                    ) : (
                        <span className='fw-bold'>{disease.toUpperCase()} </span>
                    )} 
                </td>
              </tr>
              <tr className='align-middle text-center border-bottom'>
                <td>
                  <b>TP : </b>
                  {tp} 
                </td>
                <td>
                  <b>FP : </b>
                  {fp} 
                </td>
              </tr>
              <tr className='align-middle text-center border-bottom'>
                <td>
                  <b>FN : </b> 
                  {fn} 
                </td>
                <td>
                  <b>TN : </b>  
                  {tn} 
                </td>
              </tr>
              <tr className='align-middle text-center border-bottom'>
                <td>
                  <b>Sensibilidad : </b>
                  {sensivility} %
                </td>
                <td>
                  <b>Especificidad :  </b>
                  {specificity} %
                </td>
              </tr>
              <tr className='align-middle text-center border-bottom'>
                <td>
                  <b>VPN : </b>
                  {vpn}%
                </td>
                <td>
                  <b>VPP : </b>
                  {vpp}%
                </td>
              </tr>
          </tbody>
        </table>
        </div>
      </div>
  )
}

export default Resultados