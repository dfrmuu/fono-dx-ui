import {useState,useEffect} from 'react'
import {FilaSeñales} from './FilasTabla'
import { BeatLoader } from 'react-spinners';
import { toast } from 'react-toastify';
import axios from 'axios'
import Cookies from 'universal-cookie';
import { FaEdit, FaEye, FaHeart } from 'react-icons/fa';
import SpinnerHeart from '../../../../SpinnerHeart';

//Este componente se utiliza para renderizar los datos de la tabla de pacientes
const Paciente = ({paciente,setModalSeñales,ocultarFilas,limpiarBusqueda, handleEditarPaciente,handleEditarSenal}) => {

    //Variables para controlar modal y tabla anidada
    const [abierta, setAbierta] = useState(false)
    const [clicked,setClicked] = useState(false)

    //Variable para almacenar las señales del paciente
    const [señales,setSeñales] = useState([])

    //Variable para spinner de carga
    const [cargando,setCargando] = useState(false)


    //Variable para recuperar las cookies del navegador
    const cookies = new Cookies()

    //Datos del API
    const VITE_API_LINK = import.meta.env.VITE_API_LINK

    //Recupera las señales de la base de datos
    const consultarSeñales = async (id) => {

        setCargando(true)

        //Recupera el token
        const tokenAcceso = cookies.get("token")

        //Trae la señales
        await axios.post(VITE_API_LINK + `senales/paciente`, {idPaciente : id} , {
            headers: {
                Authorization: `Bearer ${tokenAcceso}`,
                ClientAuth : import.meta.env.VITE_APIKEY,
                Resource: 'auscultacion',
                Action : 'listar'
            },
            timeout : 10000
        }).then((response) =>{

            //Agrega las señales a la variable
            setSeñales(response.data)
        }).catch((error) => {
            console.log(error)
        }).finally(() => {
            //Finaliza el spinner de carga
            setTimeout(() => {
                setCargando(false)
            }, 500);
        })

    }

    //Mostrar la tabla anidada.
    const mostrarDatosPaciente = async (id) => {

        //Revisa si ha sido clickeado el botón
        if(clicked == false){
            setAbierta(true)
            consultarSeñales(id)
            ocultarFilas(id)
            setClicked(true)
        }else{
            setAbierta(false)
            limpiarBusqueda()
            setClicked(false)
        }
    }


    
    return (
 
        <>
                <tr className='align-middle light-grey-handbook'>
                    <td className='tall-cell dark-grey-handbook fw-bold' role='button' onClick={() => mostrarDatosPaciente(paciente.id)}>
                        <FaEye className='fw-bold dark-red-handbook-text fs-4'/>
                    </td>
                    <td className='tall-cell'>{paciente.user.name + " " + paciente.user.last_name}</td>
                    <td className='tall-cell'>{paciente.age}</td>
                    <td className='tall-cell text-uppercase'>{paciente.initial_classification}</td>
                    <td className='tall-cell '>
                        <span className='btn fw-bold'>
                            {paciente.aortic_focus == "FA" ? (
                                "FOCO AORTICO - FA"
                            ) : (
                                null
                            )}

                            {paciente.aortic_focus == "FP" ? (
                                "FOCO PULMONAR - FP"
                            ) : (
                                null
                            )}

                            {paciente.aortic_focus == "FM" ? (
                                "FOCO MITRAL - FM"
                            ) : (
                                null
                            )}

                            {paciente.aortic_focus == "FT" ? (
                                "FOCO TRICUSPIDEO - FT"
                            ) : (
                                null
                            )}

                        </span>
                    </td>
                    <td>
                        <div className='col-md-12'>
                            <button className='dark-red-handbook btn  w-100 p-3 mt-1 text-white' onClick={() => handleEditarPaciente(paciente.id)}>
                                <FaEdit/>
                                <span className='px-2'>Editar paciente</span>
                            </button>
                        </div>
                    </td>
                </tr>

                {cargando == true ? (
                            <tr>
                                <td colSpan="9">
                                    <SpinnerHeart width={500} height={500}/>
                                 </td>
                            </tr> 
                ) : (

                <FilaSeñales abierta={abierta} 
                            setAbierta={setAbierta}
                            señales={señales}
                            setSeñales = {setSeñales}
                            paciente={paciente}
                            cargando={cargando}
                            handleEditarSenal={handleEditarSenal}/>
                
                )}
        </>
    )
}

export default Paciente