import {useEffect, useState} from 'react'
import { useOutletContext } from 'react-router-dom'
import { FaEye, FaQuestion, FaQuestionCircle } from 'react-icons/fa'
import Confetti from 'react-confetti'
import HeartLogin from '../../../assets/lottie-anim/HeartLogin.json'
import useWindowDimensions from '../../../hooks/useWindowDimensions.js'
import Cookies from 'universal-cookie';
import axios from 'axios'
import Tareas from '../Tareas'
import Lottie from 'lottie-react'

//Componente para renderizado del dashboard del médico
const CardGeneral = () => {

    //Datos del usuario
    const [user, setUser] = useOutletContext();

    //Toma las dimensiones de la pantalla 
    const { height, width } = useWindowDimensions();

    //Variables (states) para el conteo de actividades
    const [actividades,setActividades] = useState([])
    const [countPacientes,setCountPacientes] = useState(0)
    const [countAuscultaciones,setCountAuscultaciones] = useState(0)

    //Variable para controlar confeti inicial
    const [confeti,setConfeti] = useState(false) 

    //Variable para controlar el estado del modal de tareas
    const [modalTareas,setModalTareas] = useState(false)

    //Recuperar cookies del navegador
    const cookies = new Cookies();

    //Datos API
    const VITE_API_LINK = import.meta.env.VITE_API_LINK

    //Función para recuperar las actividades del API
    const revisionActividad = async () => {

        //Recuperar token en las cookies
        const tokenAcceso = cookies.get("token")

        //Trae los datos del API
        await axios.get(VITE_API_LINK + "actividades/mis-actividades",{
            headers: {
                Authorization: `Bearer ${tokenAcceso}`,
                ClientAuth : import.meta.env.VITE_APIKEY
            },
            timeout : 5000  
        }).then((response) => {

            //Agrega los datos de las actividades
            setActividades(response.data)

            //¿Hay una respuesta valida?
            if(response.data.length == 0 || response.data == undefined){

                //Pone valores en 0
                setCountAuscultaciones(0)
                setCountPacientes(0)
            } else {

                //Trae datos dentro del array de respuesta
                const response_data = response.data[0]

                if(response_data.count_pacientes == 0){ //Cuenta si el array trae datos de este count
                    setCountPacientes(0)
                }else{

                    //Agrega los valores al estado
                    setCountPacientes(response_data.count_pacientes)
                }
    
                if(response_data.count_auscultaciones == 0){ //Cuenta si el array trae datos de este count
                    setCountAuscultaciones(0)
                }else{

                    //Agrega los valores al estado
                    setCountAuscultaciones(response_data.count_auscultaciones)
                }
            }

        }).catch((error) =>{
            console.log(error)
        })  
    }

    
    //Al renderizar la página ejecuta estas funciones
    useEffect(() => {
        revisionActividad()

        //Cambia el estado para la visualización del confetti
        setConfeti(true)

        //En 5seg quita el confeti
        setTimeout(() => {
            setConfeti(false)
        }, 5500);
    }, [])

    //Handle para controlar el modal de tareas (abierto-cerrado)
    const handleModalTareas = () => {
        setModalTareas(!modalTareas)
    }
    

    return (
        <div>

            {confeti == true ? (<Confetti width={width} height={height} numberOfPieces={250} recycle={false}/>) : null}

            {modalTareas == true ? (
                <Tareas abierto={modalTareas} handleModal={handleModalTareas}/>
            ) : (
                null
            )}

            <div className='row'>
                <div className='col-md-6 mb-2'>
                    <div className='col-md-12'>
                        <div className='py-3 pb-4 rounded shadow-lg position-relative'>
                            <div className='d-flex justify-content-center'>
                                <Lottie animationData={HeartLogin} style={{width : '100px', height : '100px'}}/>
                                <h4 className='fw-bold mt-5 me-4'>
                                    Hola, {`${user.Nombre_Usuario}`}
                                </h4>
                            </div>
                            <p className='fs-5 mt-3'>¡Nos alegra verte de nuevo en <span className='dark-red-handbook-text'>FonoDX</span>!</p>
                        </div>
                    </div>
                    <div className='col-md-12'>
                        <div className='col-sm-12 col-md-12 col-lg-12'>
                            <div className='p-5 mt-4 rounded shadow-lg h-100 ' role='button' onClick={() => handleModalTareas()}>
                                <div>
                                    <FaQuestionCircle className=' mx-auto d-block fs-1 dark-red-handbook-text'/>
                                    <h4 className='fw-bold mt-3 text-center'> Tareas</h4>
                                </div>

                                <p className='fs-5 mt-5'> ¿Tienes alguna tarea en mente?.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div className='col-md-6 mb-2'>
                    <div className='py-3 pb-4 rounded shadow-lg position-relative h-100'>
                        <img src='https://img.freepik.com/free-icon/user_318-159711.jpg' width={100} height={100} className='img-fluid'/>

                        <div className='px-5 mt-1 text-start'>
                            <label className='fw-bold'>Nombre:</label>
                            <input type='text' className='form-control p-3 mb-4' disabled value={user.Nombre_Completo}/>

                            <label className='fw-bold'>Cargo:</label>
                            <input type='text' className='form-control p-3 text-uppercase mb-4' disabled value={user.Cargo}/>

                            <label className='fw-bold'>Area:</label>
                            <input type='text' className='form-control p-3 text-uppercase' disabled value={user.Area}/>
                        </div>
                    </div>
                </div>
            </div>



                <br/>
                
                {actividades != undefined ? (

                    <div className='row'>
                        <div className='col-md-12'>
                            <div className='row'>

                                {countPacientes != null ? (
                                    <div className='col-md-6 mb-2'>
                                        <div className='py-5 mt-4 rounded shadow-lg px-5 dark-grey-handbook  h-100'>
                                            <div className='row'>
                                                <div className='col-md-12'>
                                                    <p className='dark-red-handbook-text  display-1 fw-bold'>
                                                        {countPacientes}
                                                    </p>
                                                </div>
                                                <div className='col-md-12'>
                                                    {parseInt(countPacientes) > 1 ? (
                                                        <p className='text-white fs-5'>
                                                            registros de pacientes.
                                                        </p>
                                                    ) : (
                                                        null
                                                    )}

                                            
                                                    {parseInt(countPacientes) < 1  ? (
                                                        <p className='text-white fs-5'>
                                                            pacientes nuevos.
                                                        </p>
                                                    ) : (
                                                        null
                                                    )}
                                            
                                                    {parseInt(countPacientes) == 1 ? (
                                                        <p className='text-white fs-5'>
                                                            paciente nuevo.
                                                        </p>
                                                    ) : (
                                                        null
                                                    )}
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                ) : (
                                    null
                                )}
                                

                                {countAuscultaciones != null ? (
                                    <div className='col-md-6 mb-2'>
                                        <div className='py-5 mt-4 rounded shadow-lg px-5 dark-grey-handbook  h-100'>
                                            <div className='row'>
                                                <div className='col-md-12'>
                                                    <p className='dark-red-handbook-text display-1 fw-bold' >
                                                        {countAuscultaciones}
                                                    </p>
                                                </div>
                                                <div className='col-md-12'>
                                                    {parseInt(countAuscultaciones) > 1 ? (
                                                        <p className='text-white fs-5'>
                                                            auscultaciones cardiacas nuevas.
                                                        </p>
                                                    ) : (
                                                        null
                                                    )}

                                                    {parseInt(countAuscultaciones) < 1  ? (
                                                        <p className='text-white fs-5'>
                                                            auscultaciones nuevas.
                                                        </p>
                                                    ) : (
                                                        null
                                                    )}

                                                    {parseInt(countAuscultaciones) == 1 ?  (
                                                        <p className='text-white fs-5'>
                                                            auscultacion nueva.
                                                        </p>
                                                    ) : (
                                                        null
                                                    )}
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                ) : (
                                    null
                                )}
                                
                            </div>
                        </div>
                    </div>
                ):(
                    null
                )}
        </div>
    )
}

export default CardGeneral