import axios from 'axios'
import { useEffect, useState } from 'react'
import { useNavigate, useOutletContext } from 'react-router-dom'
import Cookies from 'universal-cookie'

//Estas seran las opciones que se visualizaran en la barra lateral en el aplicativo
const DashboardComplete = ({rolUsuario}) => {

    //Función para redirección
    const navigate = useNavigate()

    //Variable permisos usuario
    const [permisos,setPermisos] = useState([])

    //Recuperar cookies del navegador
    const cookies = new Cookies()

    //Datos API
    const VITE_API_LINK = import.meta.env.VITE_API_LINK
    
    //Ejecuta traerPermisos apenas carge este componente
    useEffect(() => {
        traerPermisos()
    }, [])
    
    //Función para recuperar permisos del API
    const traerPermisos = async () => {

        //Recupera el token
        const tokenAcceso = cookies.get("token")

        //Trae los permisos del usuario
        axios.get(VITE_API_LINK + `roles/permisos/${rolUsuario}`,{
                headers: {
                    Authorization: `Bearer ${tokenAcceso}`,
                    ClientAuth : import.meta.env.VITE_APIKEY
                },
                timeout : 10000
        }).then((response) => {

            //Coloca los permisos del usuario en el state
            setPermisos(response.data)
        }).catch((error) => {
            console.log(error)
        })

    }    


    return (
        <div>
            <div className='rounded-pill dark-red-handbook-text shadow p-4 w-100 text-center mt-3 sidebar-btn-handbook' onClick={() => navigate("/index/dashboard")}>
                <a className='fs-6 a-sidebar'>Resumen General</a>
            </div>

            {permisos.some((permiso) => permiso.resource == "invitaciones") ? (
                <div className='rounded-pill shadow p-4 w-100 text-center mt-3 sidebar-btn-handbook' onClick={() => navigate("/index/admin/invitaciones")}>
                    <a className='fs-6 a-sidebar'>Invitaciones</a>
                </div>
            ) : (
                null
            )}

            {permisos.some((permiso) => permiso.resource == "permisos") ? (
                <div className='rounded-pill shadow p-4 w-100 text-center mt-3 sidebar-btn-handbook' onClick={() => navigate("/index/admin/permisos")}>
                    <a className='fs-6 a-sidebar'>Permisos y privilegios</a>
                </div>
            ): (
                null
            )}

            {permisos.some((permiso) => permiso.resource == "usuarios") ? (
                <div className='rounded-pill shadow p-4 w-100 text-center mt-3 sidebar-btn-handbook' onClick={() => navigate("/index/admin/usuarios")}>
                    <a className='fs-6 a-sidebar'>Usuarios</a>
                </div>
            ): (
                null
            )}



            {permisos.some((permiso) => permiso.resource == "pacientes") ? (
                <>
                    <div className='rounded-pill shadow p-4 w-100 text-center mt-3 sidebar-btn-handbook' onClick={() => navigate("/index/medico/mis-pacientes")}>
                        <a className='fs-6 a-sidebar'>Mis pacientes</a>
                    </div>

                    <div className='rounded-pill shadow p-4 w-100 text-center mt-3 sidebar-btn-handbook' onClick={() => navigate("/index/medico/registro-pacientes")}>
                        <a className='fs-6 a-sidebar'>Registrar paciente</a>
                    </div>
                </>
            ) : (
                null
            )}


            {permisos.some((permiso) => permiso.resource == "auscultacion") ? (
                <>
                    <div className='rounded-pill shadow p-4 w-100 text-center mt-3 sidebar-btn-handbook' onClick={() => navigate("/index/medico/mis-favoritas")}>
                        <a className='fs-6 a-sidebar'>Mis señales de referencia</a>
                    </div>

                    <div className='rounded-pill shadow p-4 w-100 text-center mt-3 sidebar-btn-handbook' onClick={() => navigate("/index/medico/registro-auscultacion")}>
                        <a className='fs-6 a-sidebar'>Registrar auscultación</a>
                    </div>
                </>
            ) : (
                null
            )}



            {permisos.some((permiso) => permiso.resource == "logs") ? (
                <>
                    <div className='rounded-pill shadow p-4 w-100 text-center mt-3 sidebar-btn-handbook' onClick={() => navigate("/index/admin/errores")}>
                        <a className='fs-6 a-sidebar'>Registro de errores</a>
                    </div>
                </>
            ) : (
                null
            )}

            {permisos.some((permiso) => permiso.resource == "mis-auscultaciones") ? (
                <>
                    <div className='rounded-pill shadow p-4 w-100 text-center mt-3 sidebar-btn-handbook' onClick={() => navigate("/index/paciente/mis-auscultaciones")}>
                        <a className='fs-6 a-sidebar'>Mis auscultaciones</a>
                    </div>
                </>
            ) : (
                null
            )}


        </div>
    )
}

export default DashboardComplete