import axios from 'axios'
import {useState,useEffect} from 'react'
import { FaAddressBook, FaAngleDown, FaCheck, FaPencilAlt, FaPlusCircle, FaRedo, FaTimes, FaTrash } from 'react-icons/fa'
import Modal from 'react-responsive-modal'
import { toast } from 'react-toastify'
import Cookies from 'universal-cookie'
import {AgregarTarea} from './tareas/TareasComponent.js'
import SpinnerHeart from '../SpinnerHeart.jsx'
import SweetAlert from 'react-bootstrap-sweetalert'

const Tareas = ({abierto,handleModal}) => {

    const [tareas,setTareas] = useState([])

    const [cargando,setCargando] = useState(false)
    const [idEnUso, setIdEnUso] = useState("")
    const [idTarea,setIdTarea] = useState("")
    const [alertaEliminar,setAlertaEliminar] = useState(false)
    const [agregandoTarea,setAgregandoTarea] = useState(false)
    const [filtrados,setFiltrados] = useState(false)
    const cookies = new Cookies()
    const VITE_API_LINK = import.meta.env.VITE_API_LINK

    useEffect(() => {
        traerMisTareas()
    }, [])
    

    const traerMisTareas = async() => {
        try {
            const tokenAcceso = cookies.get("token")
            setCargando(true)

            await axios.get(VITE_API_LINK + "tareas/mis-tareas", {
                headers: {
                    Authorization: `Bearer ${tokenAcceso}`,
                    ClientAuth : import.meta.env.VITE_APIKEY
                },
      
                timeout : 3000
            }).then((response) =>
                setTareas(response.data)
            ).catch((error) => {
                toast.error(error.response.data.Error)
            }).finally(() => {
                setCargando(false)
            })
        } catch (error) {
            console.log(error)
        }
    }

    const filtrarTareas = async(parametro) => {
        try {
            
            const copiaTareas = tareas.filter((tarea) => tarea.estado == parametro)
            setTareas(copiaTareas)
            setFiltrados(true)

        } catch (error) {
            console.log(error)
        }
    }


    const completarTarea = async(id) => {
        try {
            const tokenAcceso = cookies.get("token")
            await axios.get(VITE_API_LINK + `tareas/completa/${id}`, {
                headers: {
                    Authorization: `Bearer ${tokenAcceso}`,
                    ClientAuth : import.meta.env.VITE_APIKEY
                },
      
                timeout : 3000
            }).then((response) => {
                toast.info(response.data.Message)
                traerMisTareas()
            }).catch((error) => {
                toast.error(error.response.data.Error)
            })
        } catch (error) {
            console.log(error)
        }
    }


    const tareaIncompleta = async(id) => {
        try {
            const tokenAcceso = cookies.get("token")
            await axios.get(VITE_API_LINK + `tareas/incompleta/${id}`, {
                headers: {
                    Authorization: `Bearer ${tokenAcceso}`,
                    ClientAuth : import.meta.env.VITE_APIKEY
                },
      
                timeout : 3000
            }).then((response) => {
                toast.info(response.data.Message)
                traerMisTareas()
            }).catch((error) => {
                toast.error(error.response.data.Error)
            })
        } catch (error) {
            console.log(error)
        }
    }

    const eliminarTarea = async() => {
        try {
            const tokenAcceso = cookies.get("token")
            await axios.delete(VITE_API_LINK + `tareas/eliminar/${idTarea}`, {
                headers: {
                    Authorization: `Bearer ${tokenAcceso}`,
                    ClientAuth : import.meta.env.VITE_APIKEY
                },
      
                timeout : 3000
            }).then((response) => {
                toast.info(response.data.Message)
                const filtroEliminado = tareas.filter((filtro) => filtro.id !== idTarea);

                handleEliminar()
                setTareas(filtroEliminado)

            }).catch((error) => {
                toast.error(error.response.data.Error)
            })
        } catch (error) {
            console.log(error)
        }
    }

    const handleAgregar = () => {
        setAgregandoTarea(!agregandoTarea)
    }

    const handleEditar = (id) => {
        if(agregandoTarea == true){
            setAgregandoTarea(false)
            setIdEnUso("")
        }else{
            setAgregandoTarea(true)
            setIdEnUso(id)
        }
    }

    const handleCompletar = (id) => {
        completarTarea(id)
    }

    const handleIncompleta = (id) => {
        tareaIncompleta(id)
    }

    const handleEliminar = (id) => {
        if(alertaEliminar == true){
            setAlertaEliminar(false)
            setIdTarea("")
        }else{
            setAlertaEliminar(true)
            setIdTarea(id)
        }
    }

    const quitarFiltro = () => {
        setFiltrados(false)
        traerMisTareas()
    }

    return (
        <>
        {cargando == true ? (
            <SpinnerHeart width={500} height={500}/>
        ) : (
            <Modal open={abierto}
                onClose={handleModal}
                classNames={{
                    modal : 'rounded w-75 mid-grey-handbook'
                }}
                center> 

                {agregandoTarea == true ? (
                    <AgregarTarea handleAcciones={idEnUso == "" ? handleAgregar : handleEditar} 
                                  traerTareas={traerMisTareas} 
                                  editando={idEnUso == "" ? false : true}
                                  idAUsar={idEnUso}/>
                ) : (
                    <div>

                        {alertaEliminar == true ? (
                                <SweetAlert warning
                                            showCancel
                                            confirmBtnText="Si"
                                            confirmBtnBsStyle="success"
                                            cancelBtnText="No"
                                            cancelBtnBsStyle='danger'
                                            title="¿Deseas eliminar esta tarea?"
                                            onConfirm={eliminarTarea}
                                            onCancel={handleEliminar}
                                            focusCancelBtn>
                                    No podras recuperarla.
                                </SweetAlert>
                        ) : (
                            null
                        )}

                        <br/>
                        <div className='row gx-0 dark-grey-handbook rounded-top'>
                            <div className='col-md-1'>
                                <div className=' w-100 p-4 text-center dark-red-handbook-text fw-bold' role='button' onClick={() => handleAgregar()} title='Agregar tareas'>
                                    <FaPlusCircle className='fs-5'/>
                                </div>
                            </div>
                            <div className='col-md-11'>
                                <div className=' w-100 p-4 text-center dark-red-handbook-text  fw-bold '>
                                    TAREAS
                                </div>
                            </div>
                        </div>
                        <div className='row gx-0'>
                            {filtrados == true ? (
                                    <div className='col-md-12'>
                                        <button className='dark-red-handbook w-100 btn no-rounded text-white fw-bold p-2' title='Ver tareas incompletas' onClick={() => quitarFiltro()}>
                                            <FaRedo/>
                                            <span className='px-2'>
                                                Quitar filtro
                                            </span>
                                        </button>
                                    </div>
                            ) : (
                                <>
                                    <div className='col-md-6'>
                                        <button className='dark-red-handbook w-100 btn no-rounded text-white fw-bold p-2' title='Ver tareas incompletas' onClick={() => filtrarTareas(false)}>
                                            Ver incompletas
                                        </button>
                                    </div>
                                    <div className='col-md-6'>
                                        <button className='dark-red-handbook w-100 btn no-rounded text-white fw-bold p-2' title='Ver tareas completas' onClick={() => filtrarTareas(true)}>
                                            Ver completas
                                        </button>
                                    </div>
                                </>
                            )}
                        </div>
                        <div className='row'>
                            {tareas.length > 0 ? (
                                tareas.map((tarea) => (
                                    <div className='col-md-12'>
                                        <div className="card no-rounded">
                                            <div className="row g-0">
                                                <div className="col-md-7">
                                                    <div className="card-body text-center ">
                                                        <p className="card-title fw-bold">
                                                            {tarea.homework}
                                                        </p>
                                                        <p className='card-text'>
                                                            {tarea.description}
                                                        </p>
                                                        <small>
                                                            Fecha de creación: <br/>
                                                            <span className=' dark-grey-handbook rounded fw-bold p-1'>
                                                                {tarea.createdAt.split('T')[0]}
                                                            </span>
                                                        </small>
                                                    </div>
                                                </div>
                                                <div className='col-md-3'>
                                                    <div className='d-flex gray-bg align-items-center justify-content-center h-100'>
                                                        <small className='text-uppercase fw-bold text-center'>
                                                            {tarea.status == false ? (
                                                                "incompleta"
                                                            ) : (
                                                                "completada"
                                                            )}
                                                        </small>
                                                    </div>
                                                </div>
                                                <div className='col-md-2'>
                                                    <div className='row h-100'>
                                                        <div className='col-md-12'>
                                                            <div className='btn-gray dark-letter h-100 w-100 d-flex align-items-center justify-content-center' type="button" 
                                                                onClick={() => handleEditar(tarea.id)} title='Editar tarea'>
                                                                <span>
                                                                    <FaPencilAlt/>
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div className='col-md-12'>
                                                        {tarea.status == false ? (
                                                            <div className='btn-gray dark-letter h-100 w-100 d-flex align-items-center justify-content-center'  type="button" 
                                                                    onClick={() => handleCompletar(tarea.id)} title='Completar tarea'>
                                                                <span>
                                                                    <FaCheck/>
                                                                </span>
                                                            </div>
                                                        ) : (
                                                            <div className='btn-gray dark-letter h-100 w-100 d-flex align-items-center justify-content-center'  type="button" 
                                                                    onClick={() => handleIncompleta(tarea.id)} title='Cambiar a incompleta'>
                                                                <span>
                                                                    <FaTimes/>
                                                                </span>
                                                            </div>
                                                        )}
                                                        </div>
                                                        <div className='col-md-12'>
                                                            <div className='btn-gray dark-letter h-100 w-100 d-flex align-items-center justify-content-center' type="button" 
                                                                 onClick={() => handleEliminar(tarea.id)} title='Eliminar tarea'>
                                                                <span>
                                                                    <FaTrash/>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                ))
                            ) : (
                                <div className='col-md-12'>
                                    <div className='mid-grey-handbook  p-3 text-center'>
                                        Actualmente no hay tareas registradas.
                                    </div>
                                </div>
                            )}
                        </div>
                    </div>
                )}
        </Modal>
        )}
        </>
    )
}

export default Tareas