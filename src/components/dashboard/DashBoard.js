export {default as CardGeneral} from './medico/CardGeneral'
export {default as Tareas} from './Tareas'
export {default as DashboardComplete} from './DashboardComplete'
export {default as CardPaciente} from './paciente/CardPaciente'
export {default as CardAdmin} from './administrador/CardAdmin'
export {default as CardInvestigador} from './investigador/CardInvestigador'