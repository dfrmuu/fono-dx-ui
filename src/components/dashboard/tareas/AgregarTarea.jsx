import axios from 'axios'
import { ErrorMessage, Field, Form, Formik } from 'formik'
import {useState,useEffect} from 'react'
import { FaBackward, FaSave } from 'react-icons/fa'
import { toast } from 'react-toastify'
import Cookies from 'universal-cookie'
import * as Yup from 'yup'

const AgregarTarea = ({handleAcciones, editando,idAUsar,traerTareas}) => {

    const [tarea,setTarea] = useState("")
    const [descripcion,setDescripcion] = useState("")

    const [tareaAPI,setTareaAPI] = useState([])
    const cookies = new Cookies()
    const VITE_API_LINK = import.meta.env.VITE_API_LINK

    const tareaScheme = Yup.object().shape({
        homework : Yup.string()
                   .required("La tarea no puede quedar vacia")
                   .min("3", "Más de 3 caracteres"),
    
        description : Yup.string()
                   .required("La tarea no puede quedar vacia")
                   .min("3", "Más de 3 caracteres")
    })

    useEffect(() => {
        if(editando != null){
            traerTarea()
        }
    }, [])

    useEffect(() => {
        setTarea(tareaAPI.homework)
        setDescripcion(tareaAPI.description)
    }, [tareaAPI])
    
    
    const traerTarea = async() => {
        try {
            const tokenAcceso = cookies.get("token")

            await axios.get(VITE_API_LINK + `tareas/ver/${idAUsar}`,{
                headers: {
                    Authorization: `Bearer ${tokenAcceso}`,
                    ClientAuth : import.meta.env.VITE_APIKEY
                },
      
                timeout : 3000
            }).then((response) => {
                setTareaAPI(response.data)
            }).catch((error) => {
                console.log(error)
                toast.error(error.response.data.Error)
            })
        } catch (error) {
            console.log(error)
        }
    }

    const agregarTareas = async(values) => {
        try {
            const tokenAcceso = cookies.get("token")
            const {homework,description} = values

            await axios.post(VITE_API_LINK + "tareas/crear",{
                homework : homework,
                description : description
            },{
                headers: {
                    Authorization: `Bearer ${tokenAcceso}`,
                    ClientAuth : import.meta.env.VITE_APIKEY
                },
      
                timeout : 3000
            }).then((response) => {
                toast.info(response.data.Message)
                traerTareas()
                handleAcciones()
            }).catch((error) => {
                console.log(error)
                toast.error(error.response.data.Error)
            })
        } catch (error) {
            console.log(error)
        }
    }

    const editarTareas = async(values) => {
        try {
            const tokenAcceso = cookies.get("token")
            const {homework,description} = values

            await axios.put(VITE_API_LINK + "tareas/editar",{
                id : idAUsar,
                homework : homework,
                description : description
            },{
                headers: {
                    Authorization: `Bearer ${tokenAcceso}`,
                    ClientAuth : import.meta.env.VITE_APIKEY
                },
      
                timeout : 3000
            }).then((response) => {
                toast.info(response.data.Message)
                traerTareas()
                handleAcciones()
            }).catch((error) => {
                console.log(error)
                toast.error(error.response.data.Error)
            })
        } catch (error) {
            console.log(error)
        }
    }


    const handleTarea = (e) => {
        setTarea(e.target.value)
    }

    const handleDescripcion = (e) => {
        setDescripcion(e.target.value)
    }

    return (
        <div className='rounded p-2'>
            <Formik validationSchema={tareaScheme}
                    enableReinitialize={true}
                    initialValues={{
                        homework : tarea,
                        description : descripcion
                    }}
                    onSubmit={async(values) => {
                        {editando == true ? (
                            editarTareas(values)
                        ) : (
                            agregarTareas(values)
                        )}
                    }}>
                <Form>
                    <h2 className='fw-bold text-center mt-4'>
                        {editando == true ? (
                            "EDITANDO TAREA"
                        ) : (
                            "AGREGAR TAREA"
                        )}
                    </h2>
                    <br/>
                    <div className='row'>
                        <div className='col-md-12'>
                            <label className='fw-bold mb-2'>Nombre de la tarea:</label>
                            <Field className="form-control p-3" placeholder="Escribe la tarea.." name="homework" id="homework" onKeyUp={handleTarea}/>
                            <ErrorMessage name='homework' component='div' className='dark-red-handbook mt-2 mb-2 p-2 text-white rounded text-center fw-bold'/>
                        </div>
                    </div>
                    <br/>
                    <div className='row'>
                        <div className='col-md-12'>
                            <label className='fw-bold mb-2'>Descripción:</label>
                            <Field className="form-control p-3" placeholder="Escribe la descripción.." name="description" id="description" onKeyUp={handleDescripcion}/>
                            <ErrorMessage name='description' component='div' className='dark-red-handbook mt-2 mb-2 p-2 text-white rounded text-center fw-bold'/>
                        </div>
                    </div>
                    <br/>
                    <div className='row'>
                        <div className='col-md-12'>
                            <button className='dark-red-handbook text-white btn p-3 w-100 mb-3' type='submit'>
                                <FaSave/>
                                <span className='px-2'>Guardar</span>
                            </button>
                        </div>
                    </div>
                    <div className='row'>
                        <div className='col-md-12'>
                            <button className='dark-red-handbook text-white btn p-3 w-100' onClick={() => handleAcciones()}>
                                <FaBackward/>
                                <span className='px-2'>Ir atras</span>
                            </button>
                        </div>
                    </div>
                </Form>
            </Formik>
        </div>
    )
}

export default AgregarTarea