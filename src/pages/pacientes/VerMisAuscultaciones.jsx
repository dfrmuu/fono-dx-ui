import { useEffect, useState } from 'react'
import { FaAngleDoubleDown, FaAngleLeft, FaAngleRight, FaHandsWash, FaSearch } from 'react-icons/fa'
import { Auscultacion,ModalSenales } from '../../components/dashboard/paciente/mis-auscultaciones/ComponentesMisAuscultaciones'
import { useOutletContext } from 'react-router-dom'
import { ErrorMessage, Field, Form, Formik } from 'formik'
import Cookies from 'universal-cookie'
import SpinnerHeart from '../../components/SpinnerHeart'
import axios from 'axios'
import * as Yup from 'yup'
import { toast } from 'react-toastify'

//Página para recuperar las auscultaciones del paciente
const VerMisAuscultaciones = () => {

    //Variable para el spinner de carga
    const [cargando,setCargando] = useState(false)

    //Variable para almacenar las auscultaciones
    const [auscultaciones,setAuscultaciones] = useState([])

    //Variable para almacenar las auscultaciones (paginación)
    const [currentAuscultaciones,setCurrentAuscultaciones] = useState([])

    //Controla la apertura del modal
    const [modalSenales,setModalSenal] = useState(false)
    const [fechaAuscultaciones,setFechaAuscultaciones] = useState("")

    //Variables para realizar filtros
    const [fecha,setFecha] = useState("")

    //Variable para almacena las fechas disponibles de auscultación (filtros)
    const [fechas,setFechas] = useState([])

    //Recupera las cookies del navegador
    const cookies = new Cookies()

    //Datos del API
    const VITE_API_LINK = import.meta.env.VITE_API_LINK

    //Recupera fechas disponibles y consulta las auscultaciones del paciente
    useEffect(() => {
        consultaFechasAuscultaciones()
        consultarSeñales()
    }, [])
    
    //Estructura valida para consulta
    const SearchScheme = Yup.object().shape({
        fecha : Yup.date()
                   .required("La fecha es requerida para la consulta.")
                   
    })

    //Recupera las auscultaciones del paciente
    const consultarSeñales = async () => {

        setCargando(true)
        const tokenAcceso = cookies.get("token")

        await axios.post(VITE_API_LINK + `senales/paciente`,{}, {
            headers: {
                Authorization: `Bearer ${tokenAcceso}`,
                ClientAuth : import.meta.env.VITE_APIKEY,
                Resource: 'mis-auscultaciones',
                Action : 'listar'
            },
            timeout : 10000
        })
        .then((response) =>{
            setAuscultaciones(response.data)
            setCurrentAuscultaciones(response.data)
        }).catch((error) => {
            toast.error(error.response.data.Error)
            setAuscultaciones([])
            setCurrentAuscultaciones([])
        }).finally(() => {
            setTimeout(() => {
                setCargando(false)
            }, 500);
        })

    }

    //Consulta las fechas de auscultación disponibles en la base de datos
    const consultaFechasAuscultaciones = async() => {
    
        const tokenAcceso = cookies.get("token")

        await axios.post(VITE_API_LINK + `senales/fechas`, {} , {
            headers: {
                Authorization: `Bearer ${tokenAcceso}`,
                ClientAuth : import.meta.env.VITE_APIKEY,
                Resource: 'mis-auscultaciones',
                Action : 'listar'
            },
            timeout : 10000
        })
        .then((response) =>{
            setFechas(response.data)
        }).catch((error) => {
            setFechas([])
            toast.error(error.response.data.Error)
        })
    }

    //Filtra las señales a partir de los valores correspondientes
    const filtrarDatos = async() => {

        const tokenAcceso = cookies.get("token")


        await axios.post(VITE_API_LINK + "senales/filtrar",{
            fecha : fecha
        },{
            headers: {
                Authorization: `Bearer ${tokenAcceso}`,
                ClientAuth : import.meta.env.VITE_APIKEY,
                Resource : 'mis-auscultaciones',
                Action : 'listar'
            },
            timeout : 10000
        })
        .then((response) => {
            setCurrentAuscultaciones(response.data)
        }).catch((error) => {
            toast.error(error.response.data.Error)
        })
    }

    //Handle para la digitación de datos
    const handleFecha = (e) => {
        setFecha(e.target.value)
    }


    //Controla la apertura del modal 
    const handleModalSenal = (fecha) => {
        if(modalSenales == true){
            setFechaAuscultaciones("")
            setModalSenal(false)
        }else{
            setFechaAuscultaciones(fecha)
            setModalSenal(true)
        }
    }

    //Limpia la busqueda (filtros)
    const limpiarBusqueda = () => {
        setFecha("")
        consultarSeñales()
    }

    return (
        <div className='p-5 mid-grey-handbook rounded shadow'>

            <h3 className='text-center fw-bold'> Mis auscultaciones </h3>
            <p className='fs-5 text-center mt-4'> Aquí podra visualizar sus auscultaciones, y si ya tienen resultados.</p>

            <br/>
            <hr/>

            {modalSenales == true ? (
                <ModalSenales abierto={modalSenales} fecha={fechaAuscultaciones} handleModal={handleModalSenal}/>
            ) : (
                null
            )}



            <Formik validationSchema={SearchScheme}
                    onSubmit = {async() => {
                            filtrarDatos()
                        }
                    }

                    enableReinitialize={true}
                                            
                    initialValues = {{
                        fecha : fecha
                    }}>

                    <Form>
                        <div className='row mt-2'>
                            <div className='col-lg-2 col-md-6'>
                                <p className='fw-bold mt-3'>
                                    Fechas para filtrar:
                                </p>
                            </div>
                            <div className='col-lg-6 col-md-6'>
                                <Field as="select" id="fecha" name="fecha" className="form-control p-3 mt-1 shadow-sm text-center " onChange={handleFecha}>
                                    <option value="">Seleccione..</option>
                                    {fechas.map((fecha) => (
                                        <option>{fecha.createdAt}</option>
                                    ))}
                                </Field>
                                <ErrorMessage name='fecha' component='div' className='dark-red-handbook p-2 text-white rounded text-center fw-bold'/>
                            </div>
                            <div className='col-lg-2 col-md-6'>
                                <button className='dark-red-handbook text-white p-3 mt-1 w-100 btn' type='submit'>
                                    <FaSearch className='text-white'/>                      
                                    <span className='px-2'>Buscar </span>
                                </button>
                            </div>
                            <div className='col-lg-2 col-md-6'>
                                <button className='dark-red-handbook text-white p-3 mt-1 w-100 btn' type='button' onClick={() => limpiarBusqueda()}>
                                    <FaHandsWash className='text-white'/> 
                                    <span className='px-2'>Limpiar busqueda </span>
                                </button>
                            </div>
                        </div>
                    </Form>
            </Formik>


            {cargando == true ? (
                <SpinnerHeart height={500} width={500}/>
            ) : (
                auscultaciones.length > 0 ? (
                    <>
                    <div className='mt-5 table-responsive px-2'>
                        <table className="table table-striped text-center rounded-1 border overflow-hidden" id='data-table'>
                            <thead className='dark-grey-handbook text-white'>
                                <tr className='align-middle rounded-start'>
                                    <th className='tall-cell' scope="col"></th>
                                    <th className='tall-cell' scope="col">Paciente</th>
                                    <th className='tall-cell' scope="col">Fecha</th>
                                </tr>
                            </thead>                                
                            <tbody>
                                {currentAuscultaciones.map((auscultacion) => (
                                    <Auscultacion auscultacion={auscultacion}
                                                  handleVerSenales = {handleModalSenal}/>
                                ))}
                            </tbody>                        
                        </table> 
                    </div>

                    <br/>

                    {auscultaciones.length >= 10 ? (
                        <div className='p-3'>
                            <hr/>
                            <div className='row'> 
                                <div className='col-md-4'>
                                    <div className='d-flex justify-content-center'>
                                        <button className={`btn rounded-circle fw-bold light-grey-handbook-btn shadow mb-2`} type='button'
                                                onClick={() => handlePageChange(currentPage - 1)} disabled={currentPage === 1}>
                                            <FaAngleLeft/>
                                        </button>
                                    </div>
                                </div>
                                <div className='col-md-4'>
                                    <div className='d-flex justify-content-center align-middle'>
                                        <p className={`fw-bold dark-red-handbook-text mt-2`}>Página {currentPage}</p>
                                    </div>
                                </div>
                                <div className='col-md-4'>
                                    <div className='d-flex justify-content-center'>
                                        <button className={` btn rounded-circle fw-bold light-grey-handbook-btn shadow mb-2`} type='button'
                                                onClick={() => handlePageChange(currentPage + 1)} disabled={indexOfLastItem >= auscultaciones.length}>
                                            <FaAngleRight/>
                                        </button>
                                    </div>
                                </div>  
                            </div>
                        </div>
                    ) : (
                        null
                    )}  

                    </>
                ) : (
                    <div className='row'>
                            <div className='col-md-12'>
                                <div className='gray-bg p-3 mt-5 fw-bold rounded text-center'>
                                    Actualmente no hay datos disponibles.
                                </div>
                            </div>
                    </div>
                )
            )}

        </div>
    )
}

export default VerMisAuscultaciones