import { useState, useEffect} from 'react'
import { useNavigate } from 'react-router-dom'
import { Form, Field, Formik, ErrorMessage} from 'formik'
import { toast,ToastContainer} from 'react-toastify'
import Lottie from 'lottie-react'
import HeartLogin from '../../assets/lottie-anim/HeartLogin.json'
import * as Yup from 'yup'
import axios from 'axios'
import Cookies from 'universal-cookie'

//Página para el inicio de sesión del usuario
const Login = () => {

  //Función para redireccionar
  const navigate = useNavigate()

  //Recupera las cookies del navegador
  const cookies = new Cookies();

  //Trae los datos del API
  const VITE_API_LINK = import.meta.env.VITE_API_LINK

  //Trae el dispositivo que se utiliza actualmente
  const DEVICE = "web"

  //Ejecuta esta función cuando se renderiza la página
  useEffect(() => {
      //¿El token existe?
      if(cookies.get('token')){
        navigate("/index/dashboard")
      }
  }, [])
  


  //Controla el envio del formulario de inicio de sesión
  const handleSubmit = (values) => {

      //Recupera los valores del formulario
      const {email,password} = values;

        //Envia los datos al API
        axios.post(VITE_API_LINK + "auth/login", { //Inicio de sesión
            email : email,
            password : password,
            device : DEVICE
        },  {
          headers: {
            ClientAuth: import.meta.env.VITE_APIKEY,
          },
          withCredentials: true, // Opción para incluir cookies en la solicitud (si es necesario)
        }).then((response) => {
            //¿Respuesta correcta?
            if(response.data){
              //Redirecciona al dashboard
              navigate("/index/dashboard")
            }
        }).catch((error) =>{
            console.log(error)
            toast.error(error.response.data.Error)
        })
  }

  //Estructura correcta para los datos del formulario
  const LoginScheme = Yup.object().shape({
      email : Yup.string()
                   .required('El campo de correo es requerido')
                   .max(40,'No máximo de 40 caracteres').min(5,'Debe tener más de 5 caracteres'),

      password : Yup.string()
                      .required('La contraseña es requerida')
                      .min(6,'La contraseña no cumple con lo minimo establecido (6 caracteres)').max(30,'Demasiados caracteres (30)')
  })

  return (
    <section className='vh-100 dark-grey-handbook'>
        <ToastContainer position='top-right' autoClose={3000}/>
        <div className="container h-100 dark-grey-handbook">
          <div className="row d-flex justify-content-center align-items-center h-100">
            <div className="col col-xl-6">
              <div className="card shadow-lg no-border" >
                <div className="row g-0">
                  <div className="col-md-12 col-lg-12 d-flex align-items-center">
                    <div className="card-body mid-grey-handbook rounded p-4 p-lg-5 text-black">

                    <div className="position-absolute top-1 ms-3 mt-1 start-2 translate-middle">
                      <Lottie animationData={HeartLogin} 
                              className='rounded-circle shadow bg-white img-fluid' style={{height: '80px', width : '80px'}}/>
                    </div>
                      <Formik 
                          initialValues={{ email: "", password: ""}} 
                          onSubmit = {async (values) => {
                              handleSubmit(values)
                          }}
                          validationSchema={LoginScheme}>

                          <Form>
                            <div className="text-center mb-3 pb-1">
                              <p className="h1 dark-red-handbook-text fw-bold mb-0">FonoDX</p>
                            </div>

                            <h5 className="fw-bold mb-3 pb-3 text-center">Ingresa en tú cuenta</h5>

                            <div className="form-outline mb-4">
                              <label className="form-label fw-bold" htmlFor="email">Correo:</label>
                              <Field type="email" id="email" name='email' className="form-control p-3" placeholder='Ingrese su Correo' />
                              <ErrorMessage name='email' component='div' className='dark-grey-handbook mt-2 mb-2 p-2 text-white rounded text-center fw-bold'/>
                            </div>

                            <div className="form-outline mb-4">
                              <label className="form-label fw-bold" htmlFor="password">Contraseña:</label>
                              <Field type="password" id="password" name='password' className="form-control p-3" placeholder='Ingrese su contraseña' />
                              <ErrorMessage name='password' component='div' className='dark-grey-handbook mt-2 mb-2 p-2 text-white rounded text-center fw-bold'/>
                              <label className='dark-red-handbook-text mt-2' onClick={() => navigate("/recover")}>¿Olvido su contraseña?</label>
                            </div>

                            <div className="pt-1 mb-4">
                              <button className="btn dark-red-handbook text-white shadow btn-lg w-100 p-3" type="submit">Inicia sesión</button>
                            </div>

                            <div className='row'>
                                <div className='col-md-12'>
                                     <a className="btn dark-red-handbook text-white btn-lg w-100 shadow p-3" onClick={() => navigate("/register")}>¡Registrarme en el sistema!</a>
                                </div>
                            </div>

                          </Form>
                      </Formik>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    </section>
  )
}

export default Login