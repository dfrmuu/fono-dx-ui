import * as Yup from 'yup'
import axios from 'axios'
import { useState, useEffect} from 'react'
import Cookies from 'universal-cookie'
import { useNavigate } from 'react-router-dom'
import { ToastContainer, toast } from 'react-toastify'
import { FaBackward, FaRedo, FaSave } from 'react-icons/fa'
import { Form, Field, Formik, ErrorMessage} from 'formik'

//Página para recuperar las credenciales del usuario
const Recover = () => {

    //Función para redirección
    const navigate = useNavigate()

    //Generación de token
    const [email,setEmail] = useState("")
    const [tokenGenerado,setTokenGenerado] = useState(false)

    //Comprobación token
    const [token,setToken] = useState("")
    const [tokenCorrecto,setTokenCorrecto] = useState(false)

    //Cambio de clave
    const [nuevaClave,setNuevaClave] = useState("")

    //Estructura formulario para recuperar clave
    const RecoverScheme = Yup.object().shape({
        email : Yup.string()
                   .email("Debe ser un correo valido")
                   .required('El correo es requerido')
                   .max(30,'No máximo de 30 caracteres').min(10,'Debe tener más de 10 caracteres'),
  
    })

    //Datos API
    const VITE_API_LINK = import.meta.env.VITE_API_LINK
    const cookies = new Cookies()

    //Envia los datos para generar el token de recuperación
    const generarTokenRecuperar = async () => {
        try {      
            axios.post(VITE_API_LINK + "auth/recuperar", {
                email : email
            },{
                headers : {
                    ClientAuth : import.meta.env.VITE_APIKEY,
                }
            }).then((response) => {
                toast.info(response.data.Message)
                //Cambia el estado de si se genero o no el token
                setTokenGenerado(true)
            }).catch((error) =>{
                toast.error(error.response.data.Error)
            })
        } catch (error) {
            console.log(error)
        }
    }

    //Envia el token para su validación
    const revisarToken = async () => {
        try {      
            axios.post(VITE_API_LINK + "auth/validar-token", {
                token : token
            },{
                headers : {
                    ClientAuth : import.meta.env.VITE_APIKEY,
                }
            }).then((response) => {
                if(response.data.existe == true){
                    //¿El token es valido?
                    setTokenCorrecto(true)
                }
            }).catch((error) =>{
                toast.error(error.response.data.Error)
            })
        } catch (error) {
            console.log(error)
        }
    }

    //Cambia la clave
    const cambiarClave = async () => {
        try {      
            
            //Valida algunas politicas de la clave
            if(nuevaClave == ""){
                toast.error("La nueva contraseña no puede estar vacia")
                return
            }

            if(nuevaClave.length < 6){
                toast.error("No puede ser menor a 6 digitos")
                return
            }

            if(email == ""){
                toast.error("Ocurrio un error")
                return
            }

            //Envia los datos para el cambio de clave
            axios.post(VITE_API_LINK + "auth/cambiarclave", {
                clave : nuevaClave,
                email: email
            },{
                headers : {
                    ClientAuth : import.meta.env.VITE_APIKEY,
                }
            }).then((response) => {
                toast.info(response.data.Message)
            }).catch((error) =>{
                toast.error(error.response.data.Error)
            }).finally(() => {
                //Después de 1,5seg redirecciona al login
                setTimeout(() => {
                    navigate("/login")
                }, 1500);
            })
        } catch (error) {
            console.log(error)
        }
    }

    //Reinicia los valores del formulario
    const reiniciar = () => {
        setEmail("")
        setToken("")
        setNuevaClave("")
        setTokenCorrecto(false)
        setTokenGenerado(false)
    }

    //Handle para la digitación de datos del formulario
    const handleEmail = (e) => {
        setEmail(e.target.value)
    }

    const handleToken = (e) => {
        setToken(e.target.value)
    }

    const handleNuevaClave = (e) => {
        setNuevaClave(e.target.value)
    }

    return (
        <section className='vh-100 dark-grey-handbook'>
            <ToastContainer position='top-right' autoClose={3000}/>
            <div className="container py-5 h-100">
                <div className="row d-flex justify-content-center align-items-center h-100">
                    <div className="col col-xl-6">
                    <div className="card shadow-lg no-border mid-grey-handbook" >
                        <div className="row g-0">
                        <div className="col-md-12 col-lg-12 d-flex align-items-center">
                            <div className="card-body p-4 p-lg-5 text-black">

                            <div className=" mb-3 pb-1  text-center">
                                <p className="h1 fw-bold mb-0 dark-red-handbook-text">FonoDX</p>
                                <h5 className="fw-bold mb-3 mt-2 pb-3">Recupera tú contraseña</h5>
                            </div>

                            {tokenGenerado == false && tokenCorrecto == false ? (
                                <Formik onSubmit={async() => generarTokenRecuperar()}
                                    initialValues={{ email: email}} 
                                    validationSchema={RecoverScheme}
                                    enableReinitialize={true}>

                                    <Form>

                                        <div className="form-outline mb-4">
                                            <label className="form-label fw-bold" htmlFor="email">Correo:</label>
                                            <Field type="email" id="email" name='email' className="form-control p-3" placeholder='Ingrese su correo' onKeyUp={handleEmail}/>
                                            <ErrorMessage name='email' component='div' className='dark-red-handbook mt-2 mb-2 p-2 text-white rounded text-center fw-bold'/>
                                            <label className='dark-red-handbook-text mt-2 mb-2 p-2' onClick={() => navigate("/register")}>¿No tiene usuario?, registrese.</label>
                                        </div>


                                        <div className="pt-1 mb-4">
                                            <button className="btn dark-red-handbook  text-white shadow btn-lg mt-3 w-100 p-3" type="submit">
                                                <FaRedo/>
                                                <span className='px-2'>Recuperar</span>
                                            </button>
                                        </div>

                                        <div className='row'>
                                            <div className='col-md-12'>
                                                <button className="dark-red-handbook btn btn-lg w-100 text-white shadow p-3" onClick={() => navigate("/login")}>
                                                    <FaBackward/>
                                                    <span className='px-2'>Volver al login</span>
                                                </button>
                                            </div>
                                        </div>
                                    </Form>
                                </Formik>
                            ) : (
                                null
                            )} 

                            {tokenGenerado == true && tokenCorrecto == false ? (
                                <div>
                                    <div className="form-outline mb-4">
                                        <label className="form-label fw-bold" htmlFor="token">Token:</label>
                                        <input type="text" id="token" name='token' className="form-control p-3 text-center fs-4" placeholder='Ingrese el token generado' onKeyUp={handleToken}/>
                                    </div>


                                    <div className="pt-1 mb-4">
                                        <button className="btn dark-red-handbook  text-white shadow btn-lg mt-3 w-100 p-3" type="button" onClick={() => revisarToken()}>
                                            <FaRedo/>
                                            <span className='px-2'>Revisar token</span>
                                        </button>
                                    </div>
                                    <div className='row'>
                                        <div className='col-md-12'>
                                            <button className="dark-red-handbook btn btn-lg w-100 text-white shadow p-3" onClick={() => reiniciar()}>
                                                <FaBackward/>
                                            <span className='px-2'>Reiniciar formulario</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            ) : (
                                null
                            )}


                            {tokenCorrecto == true ? (
                                <div>
                                    <div className="form-outline mb-4">
                                        <label className="form-label fw-bold" htmlFor="pass">Nueva contraseña:</label>
                                        <input type="password" id="pass" name='pass' className="form-control p-3" placeholder='Ingrese su nueva contraseña' onKeyUp={handleNuevaClave}/>
                                    </div>


                                    <div className="pt-1 mb-4">
                                        <button className="btn dark-red-handbook  text-white shadow btn-lg mt-3 w-100 p-3" type="button" onClick={() => cambiarClave()}>
                                            <FaSave/>
                                            <span className='px-2'>Cambiar contraseña</span>
                                        </button>
                                    </div>
                                    <div className='row'>
                                        <div className='col-md-12'>
                                            <button className="dark-red-handbook btn btn-lg w-100 text-white shadow p-3" onClick={() => reiniciar()}>
                                                <FaBackward/>
                                            <span className='px-2'>Reiniciar formulario</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            ) : (
                                null
                            )}

                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Recover