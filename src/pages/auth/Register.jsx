import { useState, useEffect} from 'react'
import { useNavigate } from 'react-router-dom'
import { Form, Field, Formik, ErrorMessage} from 'formik'
import * as Yup from 'yup'
import axios from 'axios'
import { toast, ToastContainer } from 'react-toastify'
import {RegistroMedico,RegistroAdministrativo,RegistroInvestigador} from '../../components/auth/Auth.js'

//Componente para que un usuario se registre a partir de una invitación que llega a su correo
const Register = () => {

  //Función para redireccionar
  const navigate = useNavigate()

  //Variable para el correo
  const [correo,setCorreo] = useState("")

  //Variable para recuperar el correo del usuario
  const [correoUsuario,setCorreoUsuario] = useState("")

  //Variable para controlar el tipo de formulario que se le asigno a ese correo en la invitación
  const [tipoFormulario,setTipoFormulario] = useState("")

  //Valida si la consulta esta hecha
  const [consultaHecha,setConsultaHecha] = useState(false)

  //Recupera la invitación
  const [invitacion,setInvitacion] = useState("")

  //Datos API
  const VITE_API_LINK = import.meta.env.VITE_API_LINK

  //Handle para el control de la digitación del correo
  const handleCorreoChange = (e) => {
      setCorreo(e.target.value)
  }
 
  //Estructura de la consulta de correo
  const SearchScheme = Yup.object().shape({
      correo :    Yup.string()
                      .email('Ingrese un correo electrónico válido')
                      .required('Ingrese un correo electrónico'),
  })


  //Control del envio del formulario
  const handleSubmit = (values) => {
    
    //Recupera el correo de los valores del formulario
    const {correo} = values;

    //Envia el correo
    axios.post(VITE_API_LINK + "auth/revision-correo", {
      correo,
    }, {
        headers: {
            ClientAuth : import.meta.env.VITE_APIKEY 
        }
    }).then((response) => {
        //Agrega los datos al estado
        setCorreoUsuario(response.data.Correo)
        setTipoFormulario(response.data.TipoUsuario)
        setInvitacion(response.data.Invitacion)
        setConsultaHecha(true)
    }).catch((error) => {
       toast.error(error.response.data.Error)
    })
  }

  //Limpia el estado del correo
  const limpiarBusqueda = () => {
       setCorreo("")
  }

  return (
        <section className='vh-100 dark-grey-handbook'>
        <ToastContainer position='top-right' autoClose={3000}/>
        <div className="container-fluid h-100 dark-grey-handbook">
            <div className="row d-flex align-items-center justify-content-center h-100">
                <div className="col col-xl-6">
                <div className="card shadow-lg no-border" >
                    <div className="row g-0">
                        <div className="col-md-12 col-lg-12 d-flex align-items-center">
                            <div className="card-body mid-grey-handbook rounded p-4 p-lg-5 text-black">

                                {consultaHecha == false ? (
                                    <Formik enableReinitialize={true}
                                        validationSchema={SearchScheme} 
                                        initialValues={{correo : correo}}
                                        onSubmit ={ async(values) => {handleSubmit(values)}}>
                                                
                                        <Form>

                                            <div className="mb-3 pb-1">
                                                <p className="h1 fw-bold mb-0 dark-letter text-center">FonoDX</p>
                                            </div>

                                            <h5 className="fw-bold mb-3 pb-3 text-center">¡Registrate!</h5>

                                            <div className="form-outline mb-4">
                                            <label className="form-label fw-bold" htmlFor="Correo"> Consulta tú correo:</label>
                                                <Field type="text" className='form-control shadow-sm p-3' id='correo' name='correo' placeholder='Ingrese su correo institucional'
                                                            onKeyUp = {handleCorreoChange}/>
                                                <ErrorMessage name='correo' component='div' className='bg-danger p-2 text-white rounded text-center fw-bold'/>
                                            </div>

                                            <div className='row'>
                                                <div className='col-lg-6 col-md-12'>
                                                    <button className='btn dark-red-handbook btn-lg text-white p-3 mt-1 shadow w-100' type='submit'> 
                                                        <i className='text-white px-1 fa fa-search'></i>
                                                        Buscar 
                                                    </button>
                                                </div>
                                                <div className='col-lg-6 col-md-12'>
                                                    <button className='btn dark-red-handbook btn-lg text-white p-3 mb-3 shadow mt-1 w-100' onClick={() => limpiarBusqueda()} type='button'> 
                                                        <i className='text-white px-1 fa fa-hands'></i>
                                                        Limpiar 
                                                    </button>
                                                </div>
                                            </div>

                                        </Form>

                                    </Formik>
                                ) : (
                                    null
                                )}

                                {tipoFormulario == 2 ? (
                                    <RegistroMedico correoUsuario={correoUsuario} invitacion={invitacion}/> 
                                ) : null}

                                {tipoFormulario == 3 ? (
                                    <RegistroInvestigador correoUsuario={correoUsuario} invitacion={invitacion}/>
                                ) : (
                                    null
                                )} 

                                {tipoFormulario == 4 ? (
                                    <RegistroAdministrativo correoUsuario={correoUsuario} invitacion={invitacion}/>
                                ) : (
                                    null
                                )} 


                                <div className='row'>
                                    <div className='col-md-12'>
                                        <label className="dark-red-handbook-text w-100 mt-2 text-center" onClick={() => navigate("/login")}>Volver al login</label>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </section>
  )
}

export default Register