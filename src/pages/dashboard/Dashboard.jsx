import {useState} from 'react'
import {CardGeneral,CardAdmin,CardPaciente, CardInvestigador} from '../../components/dashboard/DashBoard'
import {useOutletContext } from 'react-router-dom'

//Componente para renderizado de dashboard de los roles
const Dashboard = () => {

  const [user, rol] = useOutletContext();


  return (
    <div>
      <div className='pt-5 pb-5 px-5 mid-grey-handbook rounded shadow vh-90'>

            <div className='text-center'>

              {rol == "1" ? //Paciente
                (
                  <div>
                    <CardPaciente/>
                  </div>
                ) : (
                  null
                )
              } 

    
              {rol == "2" ? //Medico
                (
                  <div>
                    <CardGeneral/>
                  </div>
                ) : (
                  null
                )
              }


              {rol == "3" ? //Investigador
                (
                  <div>
                    <CardInvestigador/>
                  </div>
                ) : (
                  null
                )
              }

              {rol == "4" ? //Administrador
                (
                  <div>
                      <CardAdmin/>
                  </div>
                ) : (
                  null
                )
              }


            </div>
        </div>
    </div>
  )
}

export default Dashboard