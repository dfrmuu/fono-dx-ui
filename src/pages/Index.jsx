import logo from '../assets/Logo_FonoDX.png'
import logoprincipal from '../assets/logo-principal.png'
import estetoscopio from '../assets/estetoscopiorojo.png'
import logoblanco from '../assets/logo-blanco.png'
import optimizar from '../assets/optimizar.gif'
import identificar from '../assets/identificar.gif'
import medir from '../assets/medir.gif'
import robot from '../assets/robot_shaio.gif'
import libros from '../assets/libros.gif'
import shaio_ai from '../assets/shaio_ai_rojo.gif'
import {FaArrowAltCircleLeft, FaArrowAltCircleRight, FaHome, FaUser} from 'react-icons/fa'
import { useState } from 'react'
import { useNavigate } from 'react-router-dom'

//Página principal del aplicativo (presentación del proyecto) 
const Index = () => {

    //Función para redireccionar
    const navigate = useNavigate()

    return (
        <div className='dark-red-handbook h-100 overflow-hidden'>
            <nav className="navbar navbar-expand-lg navbar-dark p-2 fw-bold">
                <div className="container-fluid">
                    <a className="navbar-brand" href="#">
                        <div className='rounded-bottom logo' >
                            <img className='img-fluid ms-3 drop-shadow-img' src={logoblanco} width={65} height={35}/>
                        </div>
                    </a>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarNav">
                        <ul className="navbar-nav mx-auto">
                            <li className="nav-item">
                                <a className="nav-link active" aria-current="page" href="#inicio">
                                    Inicio
                                </a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="#informacion">
                                    Información
                                </a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="#objetivos">
                                    Objetivos
                                </a>
                            </li>
                        </ul>
                        <button className='btn  text-white fw-bold' onClick={() => navigate("/login")}>
                            <FaUser/>
                            <span className='px-2'>Iniciar sesión</span>
                        </button>
                    </div>
                </div>
            </nav> 

            <div className='heartbeat-bg'>   {/*INDEX*/ }
                <div className="container"> 
                    <main className='vh-100 d-flex align-items-center'>
                        <div class="row justify-content-center">
                            <div class="col-lg-6 col-md-12 text-center">
                                <div className='text-white mt-5'>
                                    <h1 className='fw-bold' id='#inicio'>
                                        FONODX
                                    </h1>
                                    <p className='mt-5 fs-5'>
                                        Acceso al tratamiento y/o monitoreo de la población colombiana que padece de enfermedades cardiovasculares.
                                    </p>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-12">
                                <p className='text-center'>
                                    <img src={estetoscopio} alt="Imagen" className='drop-shadow-img float-img'/>
                                </p>
                            </div>
                        </div>
                    </main>
                </div>
            </div>


            <section> {/*INFORMACIÓN GENERAL*/ }
                <div className="custom-shape-divider-bottom-1690217194"  id='informacion'> 
                    <svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1200 120" preserveAspectRatio="none">
                        <path d="M321.39,56.44c58-10.79,114.16-30.13,172-41.86,82.39-16.72,168.19-17.73,250.45-.39C823.78,31,906.67,72,985.66,92.83c70.05,18.48,146.53,26.09,214.34,3V0H0V27.35A600.21,600.21,0,0,0,321.39,56.44Z" class="shape-fill"></path>
                    </svg>
                </div>
  
                <div className='light-grey-handbook px-5 py-5'>

                    <h2 className='text-center dark-red-handbook-text mt-5 fw-bold'> ACERCA DE </h2>

                    <div className='row mt-5 mb-5'>
                        <div className='col-md-4'>
                            <div className='p-5 rounded shadow  h-100'>
                                <div className='d-flex justify-content-center'>
                                    <img src={shaio_ai} className='rounded-circle shadow img-fluid' width={200} height={200}/>
                                </div>

                                <br/>

                                <div className='text-center mt-3'>
                                    <h4 className='fw-bold dark-red-handbook-text'> 
                                        Análisis por inteligencia artificial
                                    </h4>

                                    <p className='mt-5'>
                                        Los datos de fonocardiografía son analizados con los más actuales algoritmos de inteligencia artificial!
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div className='col-md-4'>

                            <div className='p-5 rounded shadow  h-100'>

                                <div className='d-flex justify-content-center'>
                                    <img src={robot} className='rounded-circle mt-3 shadow img-fluid' width={186} height={186}/>
                                </div> 

                                <br/>

                                <div className='text-center mt-3'>
                                    <h4 className='fw-bold dark-red-handbook-text'> 
                                        Plataforma by Clínica Shaio
                                    </h4>

                                    <p className='mt-5'>
                                        Basada en uno de los ejes misionales de la clinica Shaio, ¡la investigación!.
                                    </p>
                                </div>  

                            </div>
                        </div>
                        <div className='col-md-4'>
                            <div className='p-5 rounded shadow h-100'>

                                <div className='d-flex justify-content-center'>
                                    <img src={libros} className='rounded-circle mt-3 shadow img-fluid' width={186} height={186}/>
                                </div>   

                                <br/>

                                <div className='text-center mt-3'>
                                    <h4 className='fw-bold dark-red-handbook-text'> 
                                        Si es Shaio, es investigación.
                                    </h4>

                                    <p className='mt-5'>
                                        Lanzamiento desde la dirección de investigaciones de la Clinica.
                                    </p>
                                </div>  

                            </div>
                        </div>
                    </div>
                </div>

             </section>

            <div className='light-grey-handbook'>
                <div class="custom-shape-divider-bottom-1698419758"  id='objetivos'>
                    <svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1200 120" preserveAspectRatio="none">
                            <path d="M985.66,92.83C906.67,72,823.78,31,743.84,14.19c-82.26-17.34-168.06-16.33-250.45.39-57.84,11.73-114,31.07-172,41.86A600.21,600.21,0,0,1,0,27.35V120H1200V95.8C1132.19,118.92,1055.71,111.31,985.66,92.83Z" class="shape-fill"></path>
                    </svg>
                </div>    
            </div>

            <section className='mid-grey-handbook py-5'> {/*OBJETIVO MISIONAL*/ }
  
                 <h2 className='text-center dark-red-handbook-text fw-bold'> OBJETIVOS DEL PROYECTO </h2>
        
                 <div className='px-5'>
                    <div className='row mt-5'>
                        <div className='col-md-4'>
                            <div className='p-5 rounded shadow  h-100'>
                                <div className='d-flex justify-content-center'>
                                    <img src={optimizar} className='rounded-circle shadow img-fluid' width={200} height={200}/>
                                </div>

                                <br/>

                                <div className='text-center mt-3'>
                                    <h4 className='fw-bold dark-red-handbook-text'> 
                                        Optimizar
                                    </h4>

                                    <p className='mt-5'>
                                    Mejora de las capacidades funcionales del sistema de adquisición y análisis de sonidos
                                    valvulares cardiacos.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div className='col-md-4'>
                            <div className='p-5 rounded shadow  h-100'>
                                <div className='d-flex justify-content-center'>
                                    <img src={identificar} className='rounded-circle shadow img-fluid' width={200} height={200}/>
                                </div>

                                <br/>

                                <div className='text-center mt-3'>
                                    <h4 className='fw-bold dark-red-handbook-text'> 
                                        Identificar
                                    </h4>

                                    <p className='mt-5'>
                                        Revisar las características de los sonidos cardiacos que permitan alimentar un algoritmo de inteligencia artificial 
                                        para su agrupamiento de acuerdo a patrones de comportamiento.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div className='col-md-4'>
                            <div className='p-5 rounded shadow  h-100'>
                                <div className='d-flex justify-content-center'>
                                    <img src={medir} className='rounded-circle shadow img-fluid' width={200} height={200}/>
                                </div>

                                <br/>

                                <div className='text-center mt-3'>
                                    <h4 className='fw-bold dark-red-handbook-text'> 
                                        Medir
                                    </h4>

                                    <p className='mt-5'>
                                        Observar la fiabilidad del sistema en su capacidad de identificar tendencias de comportamiento de sonidos cardiacos provenientes
                                        de pacientes con y sin patologías valvulares
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                 </div>
            </section>


             <footer className='mid-grey-handbook'>
                    <div className="custom-shape-divider-bottom-1690224878">
                        <svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1200 120" preserveAspectRatio="none">
                            <path d="M321.39,56.44c58-10.79,114.16-30.13,172-41.86,82.39-16.72,168.19-17.73,250.45-.39C823.78,31,906.67,72,985.66,92.83c70.05,18.48,146.53,26.09,214.34,3V0H0V27.35A600.21,600.21,0,0,0,321.39,56.44Z" class="shape-fill"></path>
                        </svg>
                    </div>
                      <div className='row pt-5 dark-grey-handbook shadow'>
                            <div className="col-md-4">
                                <div className='px-5 pt-4'>
                                    <p className='fw-bold text-center'>Contacto</p>
                                        <ul>
                                            <li>Este apartado esta aún en construcción.</li>
                                        </ul> 
                                </div>
                            </div>
                            <div className="col-md-4">
                                <div className='px-5 pt-4'>
                                    <p className='fw-bold text-center'>Información</p>
                                        Tal como lo busca FonoDX, se planea facilitar el acceso al tratamiento y vigilancia de la población 
                                        colombiana afectada por enfermedades cardiovasculares.
                                </div>
                                </div>  
                                <div className="col-md-4">
                                    <div className='px-5 pt-4'>
                                        <p className='text-end'>
                                            <img src={logoblanco} className='img-fluid drop-shadow-img' width="90px" height="90px"/>
                                        </p>
                                    </div>
                                </div>                         
                             </div>   

                             <div className="p-2 dark-grey-handbook fw-bold shadow-lg">
                                <div className="row">
                                    <div className="col-md-12">
                                        <p className='text-center mt-4'>
                                            Todos los derechos reservados (2023)
                                        </p>
                                </div>
                            </div>
                    </div>
             </footer>
        </div>
    )
}

export default Index