export {default as Invitaciones} from './Invitaciones'
export {default as TablaPermisos} from './permisos/TablaPermisos'
export {default as TablaUsuarios} from './usuarios/TablaUsuarios'
export {default as TablaErrores} from './errores/TablaErrores'