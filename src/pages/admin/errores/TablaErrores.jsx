import {useState, useEffect} from 'react'
import { toast } from 'react-toastify'
import { FaAngleLeft, FaAngleRight, FaSearch, FaWrench } from 'react-icons/fa'
import { ErrorMessage, Formik,Form,Field} from 'formik'
import * as Yup from 'yup'
import SpinnerHeart from '../../../components/SpinnerHeart'
import Cookies from 'universal-cookie'
import axios from 'axios'
import { DateTime } from 'luxon'

//Página para renderizar la tabla de errores del sistema
const TablaErrores = () => {

    //Variable para spinner de carga
    const [cargando,setCargando] = useState(false)

    //Variable para almacenar los errores
    const [errores,setErrores] = useState([])

    //Variable para almacenar el top de errores
    const [topErrores,setTopErrores] = useState([])
    //Variable para paginar los errores
    const [currentErrores,setCurrentErrores] = useState([])

    //Almacena los diferentes recursos del sistema
    const [recursos,setRecursos] = useState([])

    //Variables para aginación
    const [currentPage, setCurrentPage] = useState(1);
    const itemsPerPage = 10;

    //Variables para filtros
    const [criterio,setCriterio] = useState("")
    const [dato,setDato] = useState("") 


    //API
    const VITE_API_LINK = import.meta.env.VITE_API_LINK
    const cookies = new Cookies()


    //Apenas carga el componente trae los errores y los recursos
    useEffect(() => {
        traerErrores()
        traerRecursos()
    }, [])
    
    //Estructura para busqueda
    const SearchScheme = Yup.object().shape({
        criterio : Yup.string()
                      .required("El criterio de busqueda es requerido")
                      .oneOf(["resource"],"El criterio de busqueda es requerido"),
        
        dato : Yup.string()
                  .required("El dato de busqueda es requerido"),
    })


    // Calcula los índices y filtra los equipos para la página actual
    const indexOfLastItem = currentPage * itemsPerPage;


    // Función para cambiar de página
    const handlePageChange = (pageNumber) => {
        if (pageNumber >= 1 && pageNumber <= Math.ceil(errores.length / itemsPerPage)) {
          setCurrentPage(pageNumber);
  
          // Calcular los índices y actualizar currentMarcas para reflejar la página actual
          const newIndexOfLastItem = pageNumber * itemsPerPage;
          const newIndexOfFirstItem = newIndexOfLastItem - itemsPerPage;
          setCurrentErrores(errores.slice(newIndexOfFirstItem, newIndexOfLastItem));
        }
    };

    //Recupera la lista de errores del api
    const traerErrores = async () => {

        setCargando(true)
        const tokenAcceso = cookies.get("token")

        await axios.get(VITE_API_LINK + "logs/errores",{
            headers: {
                Authorization: `Bearer ${tokenAcceso}`,
                ClientAuth : import.meta.env.VITE_APIKEY,
                Resource : 'logs',
                Action : 'listar'
            },
            timeout : 20000
        })
        .then((response) => {
            setErrores(response.data.listaLogs)
            setTopErrores(response.data.topErrores)
            setCurrentErrores(response.data.listaLogs)
        }).catch((error) => {
            console.log(error)
            setErrores([])
            setCurrentErrores([])
            toast.error(error.response.data.Error)
        }).finally(() => {
            setTimeout(() => {
                setCargando(false) 
            }, 200);
        })
    } 

    //Recupera la lista de recursos desde el API
    const traerRecursos = async () => {

        setCargando(true)
        const tokenAcceso = cookies.get("token")

        await axios.get(VITE_API_LINK + "logs/errores/recursos",{
            headers: {
                Authorization: `Bearer ${tokenAcceso}`,
                ClientAuth : import.meta.env.VITE_APIKEY,
                Resource : 'logs',
                Action : 'listar'
            },
            timeout : 10000
        })
        .then((response) => {
            setRecursos(response.data)
        }).catch((error) => {
            setRecursos([])
            toast.error(error.response.data.Error)
        }).finally(() => {
            setTimeout(() => {
                setCargando(false) 
            }, 200);
        })
    } 

    //Filtrar datos log de error
    const filtrarDatos = async () => {

        setCargando(true)

        const tokenAcceso = cookies.get("token")

        await axios.post(VITE_API_LINK + "logs/errores/filtrar",{
            dato : dato,
            criterio : criterio
        },{
            headers: {
                Authorization: `Bearer ${tokenAcceso}`,
                ClientAuth : import.meta.env.VITE_APIKEY,
                Resource : 'usuarios',
                Action : 'listar'
            },
            timeout : 10000
        })
        .then((response) => {
            setCurrentErrores(response.data)
        }).catch((error) => {
            toast.error(error.response.data.Error)
        }).finally(() => {
            setTimeout(() => {
                setCargando(false)
            }, 500);
        })
    } 

    //Limpia la busqueda
    const limpiarBusqueda = () => {
        traerErrores()
    }

    //Handle para la digitación de los valores
    const handleCriterioChange = (e) => {
        setDato("")
        setCriterio(e.target.value)
    }

    const handleDatoChange = (e) => {
        setDato(e.target.value)
    }


    return (
        <div className='p-5 mid-grey-handbook rounded shadow'>


            <h3 className='text-center fw-bold'> Registro de errores del sistema </h3>
            <p className='fs-5 text-center mt-4'> Aquí podra ver que errores son frecuentes en el aplicativo.</p>

            <br/>
            <hr/>

            {cargando == true ? (
                <SpinnerHeart height={500} width={500}/>
            ) : (
                errores.length > 0 ? (
                    <>

                    {topErrores.length > 0 ? (
                        <div className='row px-2'>
                            <label className='fw-bold mb-2'>Recursos con más errores:</label>
                            <br/>
                            {topErrores.length > 0 ? (
                                topErrores.map((error) => (
                                    <div className='col-lg-3 col-md-6'>
                                        <div className='p-3 text-center shadow light-grey-handbook mt-1 mb-1 rounded fw-bold'>
                                            <span className='text-uppercase'>{error.resource}</span><br/>
                                            <span className='dark-red-handbook-text'>( {error.cantidad} )</span><span> errores</span>
                                        </div>
                                    </div>
                                ))
                            ) : (
                                null
                            )}
                        </div>
                    ) : (
                        null
                    )}

                    <hr/>

                    <Formik enableReinitialize={true}
                            initialValues = {{criterio : criterio, dato : dato }}
                            onSubmit ={async() => filtrarDatos()}
                            validationSchema={SearchScheme}>
                            <Form>
                                <div className='row px-2'>
                                        <label className='fw-bold'>Filtrar datos:</label>
                                        <div className='col-md-6 col-lg-3'>

                                            <Field as='select' className='form-select text-center shadow-sm p-3 mt-1' id='criterio' name='criterio' value={criterio}
                                                   onChange = {handleCriterioChange}>
                                                    <option value=''> -- Filtrar por -- </option>
                                                    <option value='resource'> Recurso </option>
                                            </Field>
              

                                            <ErrorMessage name='criterio' component='div' className='dark-grey-handbook mt-2 mb-2 p-2 text-white rounded text-center fw-bold'/>
                                        </div>
                                        <div className='col-md-6 col-lg-5'>
                                            {criterio == 'resource' ? (
                                                <>
                                                <Field as='select' className='form-select text-center shadow-sm p-3 mt-1' 
                                                        id='dato' name='dato' onChange = {handleDatoChange} value={dato}>
                                                        <option value=''> -- Filtrar por -- </option>
                                                        {recursos.map((recurso) => (
                                                            <option value={recurso.resource}> {recurso.resource} </option>
                                                        ))}
                                                </Field>
                                                <ErrorMessage name='dato' component='div' className='dark-grey-handbook mt-2 mb-2 p-2 text-white rounded text-center fw-bold'/>
                                                </>
                                            ) : (
                                                null
                                            )}


                                            {criterio == '' ? (
                                                <div className='p-3 mt-1 shadow text-center rounded dark-grey-handbook text-white'>
                                                    Seleccione un valor del desplegable
                                                </div>
                                            ) : (
                                                null
                                            )}
                                        </div>
                                        <div className='col-md-6 col-lg-2'>
                                            <button className='btn dark-red-handbook w-100 text-white mt-1 p-3' type='submit'>
                                                <FaSearch className='px-1'/>
                                                Buscar
                                            </button>
                                        </div>

                                        <div className='col-md-6 col-lg-2'>
                                            <button className='btn dark-red-handbook w-100 text-white p-3 mt-1' type='button' onClick={() => limpiarBusqueda()}>
                                            <i className="fa-solid fa-hand-sparkles px-1"></i>
                                                Limpiar busqueda
                                            </button>
                                        </div>
                                </div>
                        </Form>
                    </Formik>

                    <br/>

                    <div className='mt-5 table-responsive px-2'>
                        <table className="table table-striped text-center rounded-1 border overflow-hidden" id='data-table'>
                            <thead className='dark-grey-handbook text-white'>
                                <tr className='align-middle rounded-start'>
                                    <th className='tall-cell' scope="col">Recurso</th>
                                    <th className='tall-cell' scope="col">Acción</th>
                                    <th className='tall-cell' scope="col">Información</th>
                                    <th className='tall-cell' scope="col">Fecha creación</th>
                                    <th className='tall-cell' scope="col">Repeticiones</th>
                                </tr>
                            </thead>                                
                            <tbody>
                                {currentErrores.map((error) => (
                                    <tr className='light-grey-handbook' key={error.id}>
                                        <td className='align-middle tall-cell fw-bold text-uppercase'>
                                            {error.resource}
                                        </td>
                                        <td className='align-middle tall-cell'>
                                            {error.action}
                                        </td>
                                        <td className='align-middle tall-cell'>
                                            {error.info}
                                        </td>
                                        <td className='align-middle tall-cell fw-bold'>
                                            {DateTime.fromISO(error.createdAt).setZone('UTC').setZone('America/Bogota').toFormat('yyyy-MM-dd -  HH:mm')}
                                        </td>
                                        <td className='align-middle tall-cell'>
                                            <span className='dark-grey-handbook rounded-circle px-3 py-2 text-white shadow fs-5'>
                                              {error.cantidad}
                                            </span>
                                        </td>
                                    </tr>
                                ))}
                            </tbody>                        
                        </table> 
                    </div>

                    <br/>

                    {errores.length >= 10 ? (
                        <div className='p-3'>
                            <hr/>
                            <div className='row'> 
                                <div className='col-md-4'>
                                    <div className='d-flex justify-content-center'>
                                        <button className={`btn rounded-circle fw-bold light-grey-handbook-btn shadow mb-2`} type='button'
                                                onClick={() => handlePageChange(currentPage - 1)} disabled={currentPage === 1}>
                                            <FaAngleLeft/>
                                        </button>
                                    </div>
                                </div>
                                <div className='col-md-4'>
                                    <div className='d-flex justify-content-center align-middle'>
                                        <p className={`fw-bold dark-red-handbook-text mt-2`}>Página {currentPage}</p>
                                    </div>
                                </div>
                                <div className='col-md-4'>
                                    <div className='d-flex justify-content-center'>
                                        <button className={` btn rounded-circle fw-bold light-grey-handbook-btn shadow mb-2`} type='button'
                                                onClick={() => handlePageChange(currentPage + 1)} disabled={indexOfLastItem >= errores.length}>
                                            <FaAngleRight/>
                                        </button>
                                    </div>
                                </div>  
                            </div>
                        </div>
                    ) : (
                        null
                    )}  

                    </>
                ) : (
                    <div className='row'>
                            <div className='col-md-12'>
                                <div className='gray-bg p-3 mt-5 fw-bold rounded text-center'>
                                    Actualmente no hay logs de error.
                                </div>
                            </div>
                    </div>
                )
            )}
        </div>
    )
}

export default TablaErrores