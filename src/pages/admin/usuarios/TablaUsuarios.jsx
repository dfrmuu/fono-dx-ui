import { useState } from 'react';
import { useNavigate, useOutletContext } from 'react-router-dom'
import { useEffect } from 'react';
import { toast } from 'react-toastify';
import { 
    CrearUsuario, 
    EditarUsuario, 
    RegistrosInicio, 
    Usuario,
} 
from '../../../components/dashboard/administrador/usuarios/ComponentesUsuario.js';
import axios from 'axios';
import Cookies from 'universal-cookie'
import * as Yup from 'yup'
import SpinnerHeart from '../../../components/SpinnerHeart';
import { ErrorMessage, Field, Form, Formik } from 'formik';
import SweetAlert from 'react-bootstrap-sweetalert';
import { FaAngleLeft, FaAngleRight, FaPlus, FaUser, FaUserPlus } from 'react-icons/fa';

//Página para visualizar los usuarios del sistema
const TablaUsuarios = () => {

    //Variable para redireccionar
    const navigate = useNavigate()

    //Recupera datos del usuario
    const [user,rol,permisos] = useOutletContext()

    //Variable para almacenar los usuarios
    const [usuarios,setUsuarios] = useState([])

    //Variable para almacenar los usuarios (paginación)
    const [currentUsuarios,setCurrentUsuarios] = useState([])

    //Variable spinner de carga
    const [cargando,setCargando] = useState(false)

    //Control modales (crear,editar,eliminar,desactivar,activar)
    const [modalCrear,setModalCrear] = useState(false)
    const [modalEditar,setModalEditar] = useState(false)
    const [alertaEliminar,setAlertaEliminar] = useState(false)

    const [alertaDesactivar,setAlertaDesactivar] = useState(false)
    const [id,setId] = useState("")
    const [alertaActivar,setAlertaActivar] = useState(false)

    //Variable para revisar logs de inicio
    const [idLogInicio,setIdLogInicio] = useState("")

    //Modal para visualización de registros de inicio de sesión
    const [modalRegistrosInicio,setModalRegistrosInicio] = useState(false)

    //Variables para filtros
    const [criterio,setCriterio] = useState("")
    const [dato,setDato] = useState("") 

    //Variables para paginación
    const [currentPage, setCurrentPage] = useState(1);
    const itemsPerPage = 10;

    // Calcula los índices y filtra los equipos para la página actual
    const indexOfLastItem = currentPage * itemsPerPage;

    //Datos API
    const VITE_API_LINK = import.meta.env.VITE_API_LINK
    const cookies = new Cookies()

    //Recupera los usuarios
    useEffect(() => {
        traerUsuarios()
    }, [])
    

    // Función para cambiar de página
    const handlePageChange = (pageNumber) => {
        if (pageNumber >= 1 && pageNumber <= Math.ceil(usuarios.length / itemsPerPage)) {
          setCurrentPage(pageNumber);
  
          // Calcular los índices y actualizar currentMarcas para reflejar la página actual
          const newIndexOfLastItem = pageNumber * itemsPerPage;
          const newIndexOfFirstItem = newIndexOfLastItem - itemsPerPage;
          setCurrentUsuarios(usuarios.slice(newIndexOfFirstItem, newIndexOfLastItem));
        }
    };

    //Esctructura para consulta de usuario
    const SearchScheme = Yup.object().shape({
        criterio : Yup.string()
                      .required("El criterio de busqueda es requerido")
                      .oneOf(["name","ci","status", "RoleID","phone"],"El criterio de busqueda es requerido"),
        
        search : Yup.string()
                  .required("El dato de busqueda es requerido"),
    })

    //Recupera los usuarios
    const traerUsuarios = async () => {

        setCargando(true)
        const tokenAcceso = cookies.get("token")

        await axios.get(VITE_API_LINK + "usuarios",{
            headers: {
                Authorization: `Bearer ${tokenAcceso}`,
                ClientAuth : import.meta.env.VITE_APIKEY,
                Resource : 'usuarios',
                Action : 'listar'
            },
            timeout : 10000
        })
        .then((response) => {
            setUsuarios(response.data)
            setCurrentUsuarios(response.data)
        }).catch((error) => {
            console.log(error)
            setUsuarios([])
            setCurrentUsuarios([])
            toast.error(error.response.data.Error)
        }).finally(() => {
            setTimeout(() => {
                setCargando(false) 
            }, 200);
        })
    } 

    //Filtra usuarios en el API
    const filtrarUsuarios = async() => {
        setCargando(true)

        const tokenAcceso = cookies.get("token")

        await axios.post(VITE_API_LINK + "usuarios/filtrar",{
            dato : dato,
            criterio : criterio
        },{
            headers: {
                Authorization: `Bearer ${tokenAcceso}`,
                ClientAuth : import.meta.env.VITE_APIKEY,
                Resource : 'usuarios',
                Action : 'listar'
            },
            timeout : 10000
        })
        .then((response) => {
            setCurrentUsuarios(response.data)
        }).catch((error) => {
            toast.error(error.response.data.Error)
        }).finally(() => {
            setTimeout(() => {
                setCargando(false)
            }, 500);
        })
    }

    //Desactiva un usuario del sistema
    const desactivarUsuario = async () => {
        const tokenAcceso = cookies.get("token")

        try {
            await axios.get(VITE_API_LINK + `usuarios/desactivar/${id}`,{
                headers: {
                    Authorization: `Bearer ${tokenAcceso}`,
                    ClientAuth : import.meta.env.VITE_APIKEY,
                    Resource : 'usuarios',
                    Action : 'editar'
                },
                timeout : 10000
            }).then((response) => {
                toast.info(response.data.Message)
                traerUsuarios()
            }).catch((error) => {
                toast.error(error.response.data.Error)
            })
        } catch (error) {
            console.log(error)
        }
    }

    //Elimina un usuario del sistema
    const eliminarUsuario = async () => {
        const tokenAcceso = cookies.get("token")

        try {
            await axios.delete(VITE_API_LINK + `usuarios/eliminar/${id}`,{
                headers: {
                    Authorization: `Bearer ${tokenAcceso}`,
                    ClientAuth : import.meta.env.VITE_APIKEY,
                    Resource : 'usuarios',
                    Action : 'eliminar'
                },
                timeout : 10000
            })
            .then((response) => {
                toast.info(response.data.Message)
                traerUsuarios()
                handleEliminar()
            }).catch((error) => {
                toast.error(error.response.data.Error)
            })
        } catch (error) {
            console.log(error)
        }
    }

    //Activa un usuario del sistema
    const activarUsuario = async () => {
        const tokenAcceso = cookies.get("token")

        try {
            await axios.get(VITE_API_LINK + `usuarios/activar/${id}`,{
                headers: {
                    Authorization: `Bearer ${tokenAcceso}`,
                    ClientAuth : import.meta.env.VITE_APIKEY,
                    Resource : 'usuarios',
                    Action : 'editar'
                },
                timeout : 10000
            })
            .then((response) => {
                toast.info(response.data.Message)
                traerUsuarios()
            }).catch((error) => {
                toast.error(error.response.data.Error)
            })
        } catch (error) {
            console.log(error)
        }
    }

    //Handle para los datos de filtros
    const handleCriterioChange = (e) => {
        setDato("")
        setCriterio(e.target.value)
    }

    const handleDatoChange = (e) => {
        setDato(e.target.value)
    }

    //Handle para la visualización de modales
    const handleCrear = () => {
        setModalCrear(!modalCrear)
    }

    const handleDesactivar = (id) => {
        if(alertaDesactivar == true){
            setAlertaDesactivar(false)
            setId("")
        }else{
            setAlertaDesactivar(true)
            setId(id)
        }
    }

    const handleActivar = (id) => {
        if(alertaActivar == true){
            setAlertaActivar(false)
            setId("")
        }else{
            setAlertaActivar(true)
            setId(id)
        }
    }

    const handleRegistrosInicio = (id) => {
        if(modalRegistrosInicio == true){
            setModalRegistrosInicio(false)
            setIdLogInicio("")
        }else{
            setModalRegistrosInicio(true)
            setIdLogInicio(id)
        }
    }


    const handleEditar = (id) => {
        if(modalEditar == true){
            setModalEditar(false)
            setId("")
        }else{
            setModalEditar(true)
            setId(id)
        }
    }

    const handleEliminar = (id) => {
        if(alertaEliminar == true){
            setAlertaEliminar(false)
            setId("")
        }else{
            setAlertaEliminar(true)
            setId(id)
        }
    }

    //Limpia la busqueda de un usuario
    const limpiarBusqueda = () => {
        traerUsuarios()
        setDato("")
        setCriterio("")
    }

    return (
        <div className='p-5 mid-grey-handbook rounded shadow'>


            <h3 className='text-center fw-bold'> Usuarios del sistema </h3>
            <p className='fs-5 text-center mt-4'> Aquí podra gestionar los usuarios que participan en el sistema.</p>



            {modalCrear == true ? (
                <CrearUsuario abierto={modalCrear} handleModal={handleCrear} traerUsuario={traerUsuarios}/>
            ) : (
                null
            )}

            {modalEditar == true ? (
                <EditarUsuario abierto={modalEditar} handleModal={handleEditar} id={id} traerUsuarios={traerUsuarios}/>
            ) : (
                null
            )}            

            {alertaDesactivar == true ? (
                <SweetAlert warning
                            showCancel
                            confirmBtnText="Si"
                            confirmBtnBsStyle="success"
                            cancelBtnText="No"
                            cancelBtnBsStyle='danger'
                            title="¿Deseas desactivar este usuario?"
                            onConfirm={desactivarUsuario}
                            onCancel={handleDesactivar}
                            focusCancelBtn>
                </SweetAlert>
            ) : (
                null
            )}

            {alertaEliminar == true ? (
                <SweetAlert warning
                            showCancel
                            confirmBtnText="Si"
                            confirmBtnBsStyle="success"
                            cancelBtnText="No"
                            cancelBtnBsStyle='danger'
                            title="¿Deseas eliminar este usuario?"
                            onConfirm={eliminarUsuario}
                            onCancel={handleEliminar}
                            focusCancelBtn>
                </SweetAlert>
            ) : (
                null
            )}

            {modalRegistrosInicio == true ? (
                <RegistrosInicio abierto={modalRegistrosInicio} handleModal={handleRegistrosInicio} id={idLogInicio}/>
            ) : (
                null
            )}
            

            {alertaActivar == true ? (
                <SweetAlert warning
                            showCancel
                            confirmBtnText="Si"
                            confirmBtnBsStyle="success"
                            cancelBtnText="No"
                            cancelBtnBsStyle='danger'
                            title="¿Deseas activar este usuario?"
                            onConfirm={activarUsuario}
                            onCancel={handleActivar}
                            focusCancelBtn>
                </SweetAlert>
            ) : (
                null
            )}
    
            <div className="position-fixed bottom-0 end-0 m-5 z-3">
                {permisos.some(permiso => permiso.resource === "usuarios" && permiso.action === "crear") ? (
                    <div title='Agregar usuario'>
                        <button className='btn rounded-circle dark-red-handbook shadow-lg red-border w-100 text-white p-3' onClick={() => handleCrear()}>
                            <FaUserPlus className='fs-1'/>
                        </button>
                    </div>
                )  : (
                    null
                 )}
            </div>

            <Formik enableReinitialize={true}
                    initialValues = {{criterio : criterio, search : dato }}
                    onSubmit ={async(values) => filtrarUsuarios()}
                    validationSchema={SearchScheme}>
                    <Form>
                        <div className='row px-2'>
                                <label className='fw-bold'>Filtrar datos:</label>
                                <div className='col-md-6 col-lg-3'>

                                    {criterio == "RoleID" ? (
                                        <>
                                            <Field as='select' className='form-select text-center shadow-sm p-3 mt-1' id='search' name='search' value={dato}
                                                    onChange = {handleDatoChange}>
                                                    <option value=''> -- Filtrar por -- </option>
                                                    <option value='1'> Paciente </option>
                                                    <option value='2'> Médico </option>
                                                    <option value='3'> Investigador </option>
                                                    <option value='4'> Administrador </option>
                                            </Field>
                                        </>
                                    ) : (
                                        null
                                    )}

                                    {criterio == "status" ? (
                                        <>
                                            <Field as='select' className='form-select text-center shadow-sm p-3 mt-1' id='search' name='search' value={dato}
                                                    onChange = {handleDatoChange}>
                                                    <option value=''> -- Filtrar por -- </option>
                                                    <option value='activo'> Activo </option>
                                                    <option value='inactivo'> Inactivo </option>
                                            </Field>
                                        </>
                                    ) : (
                                        null
                                    )}

                                    {criterio == "name" || criterio == "ci" || criterio == "" || criterio == "phone" ? (
                                        <>
                                            <Field type="text" id='search' name='search' className='w-100 form-control shadow-sm mt-1 p-3' 
                                                placeholder="Ingrese los criterios a buscar" onKeyUp={handleDatoChange}/>
                                        </>
                                    ) : (
                                        null
                                    )}

                                    <ErrorMessage name='search' component='div' className='dark-grey-handbook mt-2 mb-2 p-2 text-white rounded text-center fw-bold'/>
                                </div>
                                <div className='col-md-6 col-lg-5'>
                                    <Field as='select' className='form-select text-center shadow-sm p-3 mt-1' 
                                            id='criterio' name='criterio' value={criterio}
                                            onChange = {handleCriterioChange}>
                                            <option value=''> -- Filtrar por -- </option>
                                            <option value='name'> Nombre </option>
                                            <option value='ci'> Cedula </option>
                                            <option value='phone'> Número de teléfono </option>
                                            <option value='status'> Estado del usuario </option>
                                            <option value='RoleID'> Rol </option>
                                        </Field>
                                        <ErrorMessage name='criterio' component='div' className='dark-grey-handbook mt-2 mb-2 p-2 text-white rounded text-center fw-bold'/>
                                </div>
                                <div className='col-md-6 col-lg-2'>
                                    <button className='btn dark-red-handbook w-100 text-white mt-1 p-3' type='submit'>
                                    <i className='fa fa-search px-1'></i>
                                        Buscar
                                    </button>
                                </div>

                                <div className='col-md-6 col-lg-2'>
                                    <button className='btn dark-red-handbook w-100 text-white p-3 mt-1' type='button' onClick={() => limpiarBusqueda()}>
                                    <i className="fa-solid fa-hand-sparkles px-1"></i>
                                        Limpiar busqueda
                                    </button>
                                </div>
                        </div>
                </Form>
            </Formik>

            <br/>
            <hr/>

            {cargando == true ? (
                <SpinnerHeart height={500} width={500}/>
            ) : (
                usuarios.length > 0 ? (
                    <>
                    <div className='mt-5 table-responsive px-2'>
                        <table className="table table-striped text-center rounded-1 border overflow-hidden" id='data-table'>
                            <thead className='dark-grey-handbook text-white'>
                                <tr className='align-middle rounded-start'>
                                    <th className='tall-cell' scope="col">Estado</th>
                                    <th className='tall-cell' scope="col">Documento</th>
                                    <th className='tall-cell' scope="col">Nombre</th>
                                    <th className='tall-cell' scope="col">Correo</th>
                                    <th className='tall-cell' scope="col">Número de teléfono</th>
                                    <th className='tall-cell' scope="col">Nombre de usuario</th>
                                    <th className='tall-cell' scope="col">Rol</th>
                                    <th className='tall-cell' scope="col">Acciones</th>
                                </tr>
                            </thead>                                
                            <tbody>
                                {currentUsuarios.map((usuario) => (
                                    <Usuario usuario={usuario}
                                             key={usuario.id}
                                             handleActivar={handleActivar}
                                             handleEditar={handleEditar}
                                             handleEliminar={handleEliminar}
                                             handleDesactivar={handleDesactivar}
                                             handleRegistrosInicio={handleRegistrosInicio}/>
                                ))}
                            </tbody>                        
                        </table> 
                    </div>

                    <br/>

                    {usuarios.length >= 10 ? (
                        <div className='p-3'>
                            <hr/>
                            <div className='row'> 
                                <div className='col-md-4'>
                                    <div className='d-flex justify-content-center'>
                                        <button className={`btn rounded-circle fw-bold light-grey-handbook-btn shadow mb-2`} type='button'
                                                onClick={() => handlePageChange(currentPage - 1)} disabled={currentPage === 1}>
                                            <FaAngleLeft/>
                                        </button>
                                    </div>
                                </div>
                                <div className='col-md-4'>
                                    <div className='d-flex justify-content-center align-middle'>
                                        <p className={`fw-bold dark-red-handbook-text mt-2`}>Página {currentPage}</p>
                                    </div>
                                </div>
                                <div className='col-md-4'>
                                    <div className='d-flex justify-content-center'>
                                        <button className={` btn rounded-circle fw-bold light-grey-handbook-btn shadow mb-2`} type='button'
                                                onClick={() => handlePageChange(currentPage + 1)} disabled={indexOfLastItem >= usuarios.length}>
                                            <FaAngleRight/>
                                        </button>
                                    </div>
                                </div>  
                            </div>
                        </div>
                    ) : (
                        null
                    )}  

                    </>
                ) : (
                    <div className='row'>
                            <div className='col-md-12'>
                                <div className='gray-bg p-3 mt-5 fw-bold rounded text-center'>
                                    Actualmente no hay usuarios registrados.
                                </div>
                            </div>
                    </div>
                )
            )}
        </div>
    )
}

export default TablaUsuarios