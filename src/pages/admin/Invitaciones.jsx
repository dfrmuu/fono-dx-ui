import {useState,useEffect} from 'react'
import {TablaInvitaciones} from '../../components/dashboard/administrador/tabla/Tabla.js'

//Página para renderizar las invitaciones del sistema
const Invitaciones = () => {
  return (
    <div className='p-5 mid-grey-handbook rounded shadow'>

        <h3 className='text-center fw-bold'> Invitaciones del sistema </h3>
        <p className='fs-5 text-center mt-4'> Aquí podra gestionar las invitaciones que se han hecho en el sistema.</p>

    
        <TablaInvitaciones/>
    </div>
  )
}

export default Invitaciones