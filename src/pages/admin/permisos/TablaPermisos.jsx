import axios from 'axios'
import { useEffect, useState } from 'react'
import Cookies from 'universal-cookie'
import SpinnerHeart from '../../../components/SpinnerHeart'
import { ModalPermisos, Rol } from '../../../components/dashboard/administrador/permisos/ComponentesPermisos.js'
import { toast } from 'react-toastify'

//Página para gestionar los permisos de los roles del sistema 
const TablaPermisos = () => {

    //Variable para almacenamiento de roles
    const [roles,setRoles] = useState([])

    //Variable para spinner de carga
    const [cargando,setCargando] = useState(false)

    //Variable para controlar la visualización del modal de permisos
    const [modalPermisos,setModalPermisos] = useState(false)

    //Almacena la id del rol que se va a visualizar
    const [idRol,setIdRol] = useState("")

    //Datos del API
    const VITE_API_LINK = import.meta.env.VITE_API_LINK
    const cookies = new Cookies()

    //Recupera los roles
    useEffect(() => {
        traerRoles()
    }, [])
    
    //Recupera los roles desde el API
    const traerRoles = async() => {
        try {

            setCargando(true)
            const tokenAcceso = cookies.get("token")

            await axios.get(VITE_API_LINK + "roles",{
                headers: {
                    Authorization: `Bearer ${tokenAcceso}`,
                    ClientAuth : import.meta.env.VITE_APIKEY,
                    Resource : 'roles',
                    Action : 'listar' 
                },
                timeout : 3000
            })
            .then((response) => {
                setRoles(response.data)
            }).catch((error) => {
                setRoles([])
                toast.error(error.response.data.Error)
            }).finally(() => {
                setTimeout(() => {
                    setCargando(false) 
                }, 200);
            })

        } catch (error) {
            console.log(error)
        }
    }

    //Controla la apertura del modal de permisos
    const handleModalPermisos = (id) =>{
        console.log(id)
        if(modalPermisos == true){
            setModalPermisos(false)
            setIdRol("")
        }else{
            setModalPermisos(true)
            setIdRol(id)
        }
    }


    return (
        <div className='p-5 mid-grey-handbook rounded shadow'>

            <h3 className='text-center fw-bold'> Permisos y privilegios </h3>
            <p className='fs-5 text-center mt-4'> Aquí podra gestionar los permisos de los roles en el sistema.</p>


            {modalPermisos == true ? (
                <ModalPermisos abierto={modalPermisos} handleModal={handleModalPermisos} idRol={idRol}/>
            ) : (
                null
            )}

            <br/>
            <hr/>

            {cargando == true ? (
                <SpinnerHeart height={500} width={500}/>
            ) : (
                roles.length > 0 ? (
                    <div className='mt-3 table-responsive'>
                        <table className="table table-borderless  text-center rounded-1 border overflow-hidden" id='data-table'>
                            <thead className='dark-grey-handbook text-white'>
                                <tr className='align-middle'>
                                    <th scope="col" className='tall-cell'>Rol</th>
                                    <th scope="col" className='tall-cell'>Fecha creación</th>
                                    <th scope="col" className='tall-cell'>Acciones</th> 
                                </tr>
                            </thead>                                
                            <tbody>
                                {roles.map((rol) => (
                                    <Rol datosRol={rol} 
                                         key={rol.id}
                                         verPermisosRol={handleModalPermisos}/>
                                ))}
                            </tbody>                         
                        </table> 
                    </div>
                ) : (
                    <div className='row'>
                            <div className='col-md-12'>
                                <div className='gray-bg p-3 mt-5 fw-bold rounded text-center'>
                                    Actualmente no hay roles.
                                </div>
                            </div>
                    </div>
                )
            )}
        </div>
    )
}

export default TablaPermisos