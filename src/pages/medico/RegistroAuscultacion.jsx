import { useState, useRef, useEffect} from 'react';
import { Form, Field, Formik, ErrorMessage} from 'formik'
import { toast, ToastContainer } from 'react-toastify';
import { BeatLoader } from 'react-spinners';
import { DateTime } from 'luxon'
import { v4 as uuidv4 } from 'uuid';
import * as Yup from 'yup'
import axios from 'axios'
import Cookies from 'universal-cookie';
import * as d3 from 'd3';
import { FaHeadset, FaHeart, FaMicrophoneSlash, FaRedo } from 'react-icons/fa';


//Componente para realizar el registro de una auscultación
const RegistroAuscultacion = () => {


    //Referencias objetos en el dom para renderizado de resultados
    const canvasRef = useRef(null);
    const canvasGrp = useRef(null);
    const audioContextRef = useRef(null); 
    const analyserNodeRef = useRef(null);

    //Canal para la entrada de audio
    const [stream, setStream] = useState(null);

    //Variable para almacenar blob de audio
    const [audioBlob, setAudioBlob] = useState(null);

    //Variable para almacenar blob para previsualización
    const [audioPreview, setAudioPreview] = useState(null)

    //Datos formulario
    const [focoSeleccionado,setFocoSeleccionado] = useState("")
    const [dateTime, setDateTime] = useState(DateTime.local().toFormat('yyyy-MM-dd HH:mm'));

    //Busqueda paciente
    const [selectedOption, setSelectedOption] = useState('seleccione')
    const [dato,setDato] = useState("")
    const [usuario,setUsuario] = useState([])

    //Estados de la grabación
    const [testDone,setTestDone] = useState(false);
    const [isRecording, setIsRecording] = useState(false); //¿Esta grabando audio?

    //API
    const cookies = new Cookies()
    const VITE_API_LINK = import.meta.env.VITE_API_LINK


    useEffect(() => { //¿Revision del blob?
        if(testDone == true){
            drawGraph()
        }
    }, [audioBlob])
    

    //Handle para la digitación de datos
    const handleCriterioChange = (e) => {
        setSelectedOption(e.target.value);
    }

    const handleDatoChange = (e) => {
        setDato(e.target.value);
    }

    const handleFocoChange = (e) => {
        setFocoSeleccionado(e.target.value)
    }

    //Handle para control de estados de grabación
    const handleStartRecording = () => {
        startRecording()
    }

    const handleStopRecording = () => {
        stopRecording()
    }

    //Reinicia la grabación
    const handleRestartRecording = () => {
        setAudioBlob(null)
        setAudioPreview(null)
        setIsRecording(false)
        const canvas = canvasGrp.current;    //Limpiar canvas
        const canvasContext = canvas.getContext('2d');
        canvasContext.clearRect(0, 0, canvas.width, canvas.height);
    }

    //Inicia la grabación
    const startRecording = () => {
  
        setAudioPreview(null)

        //Esto lo que hace es recuperar los datos del microfono, del elemento para capturar la auscultación
        navigator.mediaDevices
            .getUserMedia({ audio: true, video: false })
            .then(stream => {
                setStream(stream);
                audioContextRef.current = new (window.AudioContext || window.webkitAudioContext)();

                setIsRecording(true)

                const canvas = canvasGrp.current;    //Limpiar canvas
                const canvasContext = canvas.getContext('2d');
                canvasContext.clearRect(0, 0, canvas.width, canvas.height);

                if(testDone == true){
                    const mediaRecorder = new MediaRecorder(stream);
                    const chunks = [];


                    mediaRecorder.start();

                    mediaRecorder.ondataavailable =  (e) => { 
                        chunks.push(e.data)
                    };

                    mediaRecorder.onstop = (e)=> {
                        const blob = new Blob(chunks, { type: 'audio/ogg; codecs=opus' });
                        setAudioBlob(blob);
                    };
    
                }

                const source = audioContextRef.current.createMediaStreamSource(stream);
                analyserNodeRef.current = audioContextRef.current.createAnalyser();
                source.connect(analyserNodeRef.current);
            }).catch( error => {
                    switch (error.name) {
                        case "NotAllowedError":
                            toast.error('Debes activar el uso de microfono en este sitio web mediante la configuración de el navegador.')
                        break;
            
                        case "NotFoundError":
                            toast.error('Al parecer no tienes un microfono conectado.')
                        break;
                    }
                }
            ).finally(() => {
                //Dibuja una grafica intensidad tiempo del audio
                drawTimeIntensityGraph();
            })
    };
  
    //Detiene la grabación
    const stopRecording = async () => {
        if(stream != null){
            setIsRecording(false)
            setTestDone(true)
            await stream.getTracks().forEach(track => track.stop());
            setStream(null);
        } else {
            console.log('No stream!');
        }
    };
  
    //Dibuja la grafica intensidad tiempo
    const drawTimeIntensityGraph = () => {
        try{
            const canvas = canvasRef.current;
            const canvasContext = canvas.getContext('2d');
            const timeData = new Float32Array(analyserNodeRef.current.frequencyBinCount);
    
            analyserNodeRef.current.fftSize = 2048;
            analyserNodeRef.current.getFloatTimeDomainData(timeData);

            canvasContext.clearRect(0, 0, canvas.width, canvas.height);
            canvasContext.beginPath();
            for (let i = 0; i < timeData.length; i++) {
                const x = (i / timeData.length) * canvas.width;
                const y = (timeData[i] + 1) * canvas.height / 2;
                if (i === 0) canvasContext.moveTo(x, y);
                else canvasContext.lineTo(x, y);
            }
    
            canvasContext.stroke();
            requestAnimationFrame(drawTimeIntensityGraph);
        }catch(error){
            console.log(error)
        }
    };

    //Dibuja grafica de la señal (solo intensidad)
    const drawGraph = async () => {
        const canvas = canvasGrp.current;
        const canvasContext = canvas.getContext('2d');


        if(audioBlob != null){
            const audioBuffer = await audioBlob.arrayBuffer();
            audioContextRef.current.decodeAudioData(audioBuffer).then(buffer => {
                const audioData = [];


                for (let i = 0; i < buffer.length; i++) {
                  audioData.push(buffer.getChannelData(0)[i]);
                }

                const xScale = d3
                                .scaleLinear()
                                .domain([0, audioData.length -1])
                                .range([0, canvas.width]);

                const yScale = d3
                                .scaleLinear()
                                .domain([-1, 1])
                                .range([canvas.height, 4]);  //.range([canvas.height, 0]);

                // Dibuja la señal ECG en el canvas
                canvasContext.beginPath();

                for (let i = 0; i < audioData.length; i++) {
                    const x = xScale(i);
                    const y = yScale(audioData[i]);
                

                    if (i === 0) {
                        canvasContext.moveTo(x, y);
                    } else {
                        canvasContext.lineTo(x, y);
                    }
                }

                canvasContext.stroke()

                // Add labels to the X and Y axis
                canvasContext.font = "8px arial";
                canvasContext.textAlign = "center";
                canvasContext.textBaseline = "middle";
                
                for (let i = -1; i <= 1; i += 0.5) {
                    const y = yScale(i);
                    canvasContext.fillText(i, 7, y);

                }

                for (let i = 0; i <= buffer.duration; i++) {
                    const x = xScale(i * (buffer.duration / buffer.duration));
                    canvasContext.fillText(i, x, canvas.width - 1);
                }
  

                setAudioPreview(audioBlob)
            }).catch(error => {console.log(error)});

        }
    };

    //Consulta los datos del paciente en el API
    const consultarPaciente = async (values) => {
        const tokenAcceso = cookies.get("token")
        const {criterio,dato} = values;
        const origen = "registro-auscultacion"

        axios.post( VITE_API_LINK + "paciente/buscar",{criterio,dato,origen},
        {
            headers: {
                Authorization: `Bearer ${tokenAcceso}`,
                ClientAuth : import.meta.env.VITE_APIKEY,
                Resource : 'pacientes',
                Action : 'listar'
            },
            timeout : 10000
        }).then((response) => {

            console.log(response)

            if(response.data.status == "completo"){
                setUsuario(response.data)
            } 

        }).catch((error) => {
            toast.error(error.response.data.Error)
        })
    }

    //Limpia la consulta de pacient
    const limpiarBusqueda = (setFieldValue) => {
        setUsuario([])
        setFocoSeleccionado("")
        setFieldValue('dato',"")
        const canvas = canvasGrp.current;    //Limpiar canvas
        const canvasContext = canvas.getContext('2d');
        canvasContext.clearRect(0, 0, canvas.width, canvas.height);
        setAudioPreview(null)
        setSelectedOption('seleccione')
        setDato("")
    }

    //Estructura consulta de paciente
    const SearchScheme = Yup.object().shape({
        criterio : Yup.string()
                      .required("El criterio de busqueda es requerido")
                      .oneOf(["documento","correo"],"El criterio de busqueda es requerido"),
        
        dato : Yup.string()
                  .required("El dato de busqueda es requerido"),
    })

    //Estructura para el envio de la auscultación
    const SeñalScheme = Yup.object().shape({

        nombre_completo : Yup.string()
                             .required("El nombre es requerido"),

        foco : Yup.string()
                  .required("El foco es requerido")
                  .oneOf(["FA","FP","FT","FM"],"El foco aortico es requerido"),

        fecha_creacion : Yup.string()    
    })

    //Envia los datos
    const handleSubmit = async(values) => {
        if(audioBlob != null){

            const {foco} = values
            const PatientID = usuario.PacientID
            const nombreAudio = uuidv4();
            // const nombreArchivo = foco + "_" + fecha_creacion
            // const quitarEspacio = nombreArchivo.replace(" ","_")
            // const nombreFinal = quitarEspacio.replace(":","-")
            const tokenAcceso = cookies.get("token")


            const archivo = new File([audioBlob], nombreAudio, { type: 'audio/mp3' });    

            const formData = new FormData();
            formData.append('PatientID',PatientID)
            formData.append('aortic_focus', foco)
            formData.append('auscultacion', archivo);


            axios.post(VITE_API_LINK + "senal/registrar", formData ,
            {
                headers: {
                    Authorization: `Bearer ${tokenAcceso}`,
                    'Content-Type': 'multipart/form-data',
                    ClientAuth : import.meta.env.VITE_APIKEY,
                    Resource : 'auscultacion',
                    Action : 'crear'
                },
                timeout : 20000
            }).then((response) => {
                toast.info(response.data.Message)
                actividadRegistroAuscultacion()
            }).catch((error) => {
                console.log(error)
                toast.error(error.response.data.Error)
            })
            
        }else{
            toast.error("Debe realizar la auscultación!")
        }
    }

    //Registra la actividad de la auscultación
    const actividadRegistroAuscultacion= async () => {

        const fecha_creacion = DateTime.local().toFormat('yyyy-MM-dd')
        const actividad = "Auscultacion"
        const tokenAcceso = cookies.get("token")


        axios.post(VITE_API_LINK + "actividad/crear",{
            actividad,
            fecha_creacion
        },{
            headers: {
                Authorization: `Bearer ${tokenAcceso}`,
                ClientAuth : import.meta.env.VITE_APIKEY,
            },
            timeout : 10000  
        }).then((response) => {

        }).catch((error) =>{
            console.log(error)
        })  

    }

    return (
        <div className='p-5 mid-grey-handbook rounded shadow'>

            <h3 className='text-center fw-bold'> Registro de auscultacion</h3>
            <p className='fs-5 text-center mt-4'>En este formulario se podra asignar una auscultacion a un paciente.</p>


            <Formik enableReinitialize={true}
                    validationSchema={SearchScheme} 
                    initialValues={{criterio : selectedOption, dato : dato}}
                    onSubmit ={ async(values) => {consultarPaciente(values)}}>
                                
                    {(props) => (

                        <Form>
                            <div className='row'>
                                <h5 className='fw-bold mb-3 mt-3'> Busqueda paciente:</h5>
                                <div className="col-lg-4 col-md-6">
                                    <Field as='select' className='form-select w-100 shadow dark-red-handbook text-white p-3 mt-2' name="critero" id="criterio"
                                          value={selectedOption} onChange={handleCriterioChange}>
                                        <option value="seleccione">Seleccione un criterio de busqueda:</option>
                                        <option value="documento">Número de documento</option>
                                        <option value="correo">Correo electronico</option>
                                    </Field>
                                    <ErrorMessage name='criterio' component='div' className='dark-grey-handbook mb-2 mt-2 p-2 text-white rounded text-center fw-bold'/>
                                </div>
                                <div className="col-lg-4 col-md-6">
                                    <Field type="text" className='form-control shadow-sm p-3 mt-2' id='dato' name='dato' placeholder='Ingrese datos de busqueda'
                                            onKeyUp = {handleDatoChange}/>
                                    <ErrorMessage name='dato' component='div' className='dark-grey-handbook mb-2 mt-2 p-2 text-white rounded text-center fw-bold'/>
                                </div>      
                                <div className="col-lg-2 col-md-6">
                                    <button className='btn dark-red-handbook  text-white shadow-sm w-100 p-3  mt-2 ' type='submit'> 
                                    <i className='text-white px-1 fa fa-search'></i>
                                        Buscar 
                                    </button>
                                </div> 
                                <div className="col-lg-2 col-md-6">
                                    <button className='btn dark-red-handbook  text-white shadow-sm w-100 p-3 mt-2' onClick={() => limpiarBusqueda(props.setFieldValue)} type='button'> 
                                    <i className='text-white px-1 fa fa-hands'></i>
                                        Limpiar busqueda 
                                    </button>
                                </div>                
                            </div>
                        </Form>

                    )}
            </Formik>

            <br/>
            <hr/>

            <Formik enableReinitialize={true}
                    validationSchema={SeñalScheme} 
                    initialValues={{ nombre_completo : usuario ? usuario.name + " " + usuario.last_name : "" , foco : focoSeleccionado, fecha_creacion : dateTime}}
                    onSubmit ={ async(values) => {handleSubmit(values)}}>

                <Form className='mt-3 light-grey-handbook p-3 shadow rounded'>
                    <br/>
                    <div className='row'>
                        <div className="col-md-12">
                            <label className='form-label fw-bold mt-1'> Nombre completo del paciente:</label>
                            <Field type="text" className='form-control shadow-sm p-3' id="nombre_completo" name="nombre_completo"
                                   placeholder='Ingrese el nombre del paciente' value={usuario ? usuario.nombre_completo : ""} disabled/>
                            <ErrorMessage name='nombre_completo' component='div' className='bg-danger p-2 text-white rounded text-center fw-bold'/>
                        </div>
                    </div>

                    <br/>

                    <div className='row'>
                        <div className="col-md-6">
                            <label className='form-label fw-bold mt-1'> Foco:</label>
                            <Field as="select" className='shadow-sm w-100 form-select p-3' id="foco" name="foco" value={focoSeleccionado} onChange={handleFocoChange}>
                                <option value="">Seleccione el foco</option>
                                <option value="FA"> FA - FOCO AORTICO</option>
                                <option value="FP"> FP - FOCO PULMONAR </option>
                                <option value="FT"> FT - FOCO TRICUSPIDAL</option>
                                <option value="FM"> FM - FOCO MITRAL</option>
                            </Field>
                            <ErrorMessage name='foco' component='div' className='dark-red-handbook mt-2 mb-2  p-2 text-white rounded text-center fw-bold'/>
                        </div>  

                        <div className="col-md-6">
                            <label className='form-label fw-bold mt-1' > Fecha de creación:</label>
                            <Field type="datetime-local" id="fecha_creacion" name="fecha_creacion" className='form-control shadow-sm p-3' value={dateTime} disabled/>
                            <ErrorMessage name='fecha_creacion' component='div' className='dark-red-handbook mt-2 mb-2 p-2 text-white rounded text-center fw-bold'/>
                        </div>
                    </div>

                    <br/>
                    <hr/>

                    {testDone == true ? (
                        <div className='row'>
                            <label className='form-label fw-bold mt-1'> Realizar auscultación:</label>
                            <div className='col-md-12'>
                                {isRecording == true ? (
                                    <>
                                        <div className='row'>
                                            <div className='col-md-6'>
                                                <button type="button" className='btn dark-red-handbook w-100 text-white shadow-sm p-3' onClick={handleStopRecording}>
                                                    <FaMicrophoneSlash/>
                                                    <span className='px-2'>Detener grabación</span>
                                                    <BeatLoader loading={testDone} className="mx-2" color='white' size={5}/>
                                                </button>
                                            </div>
                                            <div className='col-md-6'>
                                                <button type='button' className='btn dark-red-handbook w-100 h-100 p-3 text-white shadow-sm' onClick={handleRestartRecording}>
                                                    <FaRedo/>
                                                    <span className='px-2'>Reiniciar grabación</span>
                                                </button>   
                                            </div>
                                        </div>
                                    </>
                                ) : (
                                    <>  
                                        <div className='row'>
                                            <div className='col-md-6'>
                                                <button type='button' className='btn dark-red-handbook w-100 h-100 p-3 text-white shadow-sm' onClick={handleStartRecording}>
                                                    <FaHeadset/>
                                                    <span className='px-2'>Iniciar grabación</span>
                                                </button>
                                            </div>
                                            <div className='col-md-6'>
                                                <button type='button' className='btn dark-red-handbook w-100 h-100 p-3 text-white shadow-sm' onClick={handleRestartRecording}>
                                                    <FaRedo/>
                                                    <span className='px-2'>Reiniciar grabación</span>
                                                </button>                                                
                                            </div>
                                        </div>
                                    </>
                                )}
                            </div>                    
                        </div>
                    ):(
                        <div className='row'>
                            <label className='form-label fw-bold mt-1'> Probar microfono:</label>
                            <div className='col-md-12'>
                                {isRecording  ? (
                                    <>
                                        <button type="button" className='btn dark-red-handbook w-100 p-3 text-white shadow-sm'  onClick={handleStopRecording}>
                                            <i className='fa fa-microphone-slash px-2'></i>
                                                Detener prueba.
                                        </button>
                                    </>
                                ) : (
                                    <>  
                                        <button type='button' className='btn dark-red-handbook w-100 h-100 p-3 text-white shadow-sm' onClick={handleStartRecording}>
                                            <i className='fa fa-headset px-2'></i>
                                                Iniciar prueba.
                                        </button>
                                    </>
                                )}
                            </div>                    
                        </div>
                    )}

                    <br/>

                    <div className='row'>
                        <div className='col-md-1'>
                            <canvas ref={canvasRef} className="rounded shadow-lg w-100 h-100"/> 
                        </div>

                        <div className='col-md-9'>
                            <div className='alert dark-grey-handbook text-white w-100 h-100 text-center'>
                            {testDone == true ? (
                                <p>
                                    A partir de su auscultación se brindara un grafico y el audio para su revisión.
                                </p>
                            ): (
                                <p>
                                    Presione el boton "iniciar prueba" para la revisión autonoma de su equipo (microfono),
                                    si la linea no parece moverse, significa que su dispositivo no esta recibiendo audio.
                                </p>
                            )}


                                {audioPreview  ? 
                                    <div className='mt-4'>
                                        <audio className='shadow rounded-pill w-100' controls>
                                            <source src={URL.createObjectURL(audioBlob)}  type='audio/mpeg'/>
                                        </audio> 
                                    </div>
                                : null}
                            </div>
                        </div>

                        <div className='col-md-2'>
                            <canvas ref={canvasGrp} className="rounded shadow-lg w-100 h-100"/>
                        </div>
                    </div>

                    <div className='row'>
                        <div className='col-md-12'>
                            {audioPreview  ? (
                                <button className='btn dark-red-handbook w-100 text-white shadow-sm mt-4 p-3' type='submit'>
                                    <i className='text-white p-1 fa fa-save'></i>
                                    Registrar auscultacion 
                                </button>
                            ) : (
                                <button className='btn dark-red-handbook w-100 text-white shadow-sm mt-4 p-3' disabled>
                                    <i className='text-white p-1 fa fa-save'></i>
                                    Registrar auscultacion 
                                </button>                       
                            )}
                        </div>
                    </div>
                </Form>
            </Formik>
        </div>
    )
}

export default RegistroAuscultacion