import {useState} from 'react'
import { Form, Field, Formik, ErrorMessage} from 'formik'
import * as Yup from 'yup'
import axios from 'axios'
import { toast} from 'react-toastify'
import {PacienteExistente, PacienteIncompleto, PacienteInexistente} from '../../components/dashboard/medico/formularios-paciente/Formularios'
import { useOutletContext } from 'react-router-dom'
import Cookies from 'universal-cookie';

//Página para realizar el registro de un paciente
const RegistroPaciente = () => {

    //Variable para controlar el status del usuario
    const [statusUsuario,setStatusUsuario] = useState("")

    //Variables para el filtro de datos
    const [dato,setDato] = useState("")
    const [selectedOption, setSelectedOption] = useState("");

    //Variable para almacenar datos de un usuario
    const [usuario,setUsuario] = useState("")

    //Recupera las cookies del navegador
    const cookies = new Cookies()

    //Datos api
    const VITE_API_LINK = import.meta.env.VITE_API_LINK

    //Controla los cambios de formulario de filtro
    const handleCriterioChange = (event) => {
        setSelectedOption(event.target.value);
    }

    const handleDatoChange = (event) => {
        setDato(event.target.value);
    }

    //Limpia la busqueda de paciente realizada
    const limpiarBusqueda = (setFieldValue) => {
        setStatusUsuario("");
        setUsuario("")
        setFieldValue('dato',"")
        setSelectedOption('seleccione')
        setDato("")
    }

    //Consulta pacientes a partir de un criterio
    const consultarPaciente = async (values) => {

        //Trae el token en las cookies
        const tokenAcceso = cookies.get("token")

        //Recupera valores del formulario
        const {criterio,dato} = values;
        const origen = "registro-paciente"

        //Limpia el estado del usuario
        setStatusUsuario("")
    
        //Busca el paciente
        await axios.post(VITE_API_LINK + "paciente/buscar",{criterio,dato,origen},
        {
            headers: {
                Authorization: `Bearer ${tokenAcceso}`,
                ClientAuth : import.meta.env.VITE_APIKEY,
                Resource : 'pacientes',
                Action : 'listar'
            },

            timeout : 10000
        }).then((response) => {
            
            //¿Existe?
            if(response.data.status == "completo"){
                setStatusUsuario("completo")
                setUsuario(response.data)
            } 


        }).catch((error) => {

            //¿Incompleto?
            if(error.response.data.Status == "incompleto"){
                setStatusUsuario("incompleto")
                setUsuario(error.response.data.usuario)
            }

            //¿Invalido?
            if(error.response.data.Status == 'invalida'){
                toast.error(error.response.data.Error) 
            }

            //¿Inexistente?
            if(error.response.data.Status == 'inexistente'){
                toast.error(error.response.data.Error) 
                setStatusUsuario("inexistente")
            }
        })
    }

    //Estructura para los filtros
    const SearchScheme = Yup.object().shape({
        criterio : Yup.string()
                      .required("El criterio de busqueda es requerido")
                      .oneOf(["documento","correo"],"El criterio de busqueda es requerido"),
        
        dato : Yup.string()
                  .required("El dato de busqueda es requerido"),
    })



    return (
        <div className='p-5 mid-grey-handbook rounded shadow'>
            <h3 className='text-center fw-bold'> Registro de nuevo paciente</h3>
            <p className='fs-5 text-center mt-4'>En este formulario se podra realizar el registro de un paciente nuevo.</p>

            <br/>
            <hr/>

            <Formik enableReinitialize={true}
                    validationSchema={SearchScheme} 
                    initialValues={{criterio : selectedOption ? selectedOption : ""  , dato : dato}} 
                    onSubmit ={ async(values) => {consultarPaciente(values)}}>

            {(props) => (

                <Form className='mt-3 light-grey-handbook p-3 shadow rounded'>
                    <div className='row'>
                            <h5 className='fw-bold mb-3 mt-3' htmlFor="criterio"> ¿Este paciente ya esta registrado?:</h5>
                            <div className="col-lg-4 col-md-6">
                                <Field as="select" id="criterio" name="criterio" className='form-select w-100 shadow  mt-1 dark-red-handbook text-white p-3' value ={selectedOption} 
                                      onChange={handleCriterioChange}>
                                    <option value="seleccione">Seleccione un criterio de busqueda:</option>
                                    <option value="documento">Número de documento</option>
                                    <option value="correo">Correo electronico</option>
                                </Field>
                                <ErrorMessage name='criterio' component='div' className='dark-grey-handbook mb-2 mt-2 p-2 text-white rounded text-center fw-bold'/>
                            </div>
                            <div className="col-lg-4 col-md-6">
                                <Field type="text" className='form-control shadow-sm p-3 mt-1' id='dato' name='dato' placeholder='Ingrese datos de busqueda'
                                        onChange = {handleDatoChange}/>
                                <ErrorMessage name='dato' component='div' className='dark-grey-handbook mb-2 mt-2 p-2 text-white rounded text-center fw-bold'/>
                            </div>      
                            <div className="col-lg-2 col-md-6">
                                <button className='btn dark-red-handbook text-white shadow-sm w-100 mt-1 p-3' type='submit'> 
                                <i className='text-white px-1 fa fa-search'></i>
                                    Buscar 
                                </button>
                            </div>
                            <div className="col-lg-2 col-md-6">
                                <button className='btn dark-red-handbook text-white shadow-sm w-100 mt-1 p-3' onClick={() => limpiarBusqueda(props.setFieldValue)} type='button'> 
                                <i className='text-white px-1 fa fa-hands'></i>
                                    Limpiar busqueda 
                                </button>
                            </div>                
                    </div>
                </Form>

            )}

            </Formik>


            {statusUsuario == 'inexistente' &&  selectedOption == 'correo' ? (
                <PacienteInexistente dato={dato} tipo={"correo"}/>
            ) : null} 


            {statusUsuario == 'inexistente' &&  selectedOption == 'documento' ? (
                <PacienteInexistente dato={dato} tipo={"documento"}/>
            ) : null} 

            { statusUsuario == 'incompleto' &&  selectedOption == 'correo' ? (
                <PacienteIncompleto usuario = {usuario} dato={dato} tipo={"correo"}/>
            ): null}

            { statusUsuario == 'incompleto' &&  selectedOption == 'documento' ? (
                <PacienteIncompleto usuario ={usuario} dato={dato} tipo={"documento"}/>
            ): null}

            { statusUsuario == 'completo' ? (
                <PacienteExistente usuario={usuario}/>
            ): null}     
        </div>
    )
}

export default RegistroPaciente