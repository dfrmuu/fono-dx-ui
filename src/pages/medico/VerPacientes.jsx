import {useState} from 'react'
import { TablaPacientes } from '../../components/dashboard/medico/tabla/Tabla'

//Página para ver los pacientes pertenecientes al médico
const VerPacientes = () => {
    return (
        <div className='p-5 mid-grey-handbook rounded shadow'>

            <h3 className='text-center fw-bold'> Mis pacientes</h3>
            <p className='fs-5 text-center mt-4'> Aquí podra visualizar el listado de pacientes, sus señales asociadas y el diagnostico.</p>
            <TablaPacientes/>
        </div>
    )
}

export default VerPacientes