import {useState,useEffect} from 'react'
import { toast } from 'react-toastify';
import { useOutletContext } from 'react-router-dom';
import { BeatLoader } from 'react-spinners';
import { Form, Field, Formik, ErrorMessage} from 'formik'
import * as Yup from 'yup'
import axios from 'axios'
import Cookies from 'universal-cookie';
import Favorito from '../../components/dashboard/medico/favoritos/Favorito';
import SpinnerHeart from '../../components/SpinnerHeart';

//Este componente recupera las señales favoritas de el médico
const SeñalesFavoritas = () => {

    //Recupera las cookies en el navegador
    const cookies = new Cookies()

    //Variables para almacenar las señales de referencia / favoritas
    const [señalesFavoritas,setSeñalesFavoritas] = useState([])

    //Fechas / datos para filtros
    const [fechasFavoritos,setFechasFavoritos] = useState([])
    const [opcion,setOpcion] = useState("")
    const [dato,setDato] = useState("")

    //Spinner de carga
    const [cargando,setCargando] = useState(false)

    //Datos API
    const VITE_API_LINK = import.meta.env.VITE_API_LINK


    //Recupera los favoritos del médico
    useEffect(() => {
      misFavoritos()
      recuperarFechasFavoritos()
    }, [])
    

    //Handle para la digitación / cambio de valores
    const handleChangeOpcion = (e) => {
        setOpcion(e.target.value)
    }

    const handleDatoChange = (e) => {
        setDato(e.target.value)
    }

    //Trae los favoritos del médico
    const misFavoritos = async() => {

        setCargando(true)

        const tokenAcceso = cookies.get("token")
        
        await axios.get(VITE_API_LINK + "favoritos/mis-favoritos", {
            headers: {
                Authorization: `Bearer ${tokenAcceso}`,
                ClientAuth : import.meta.env.VITE_APIKEY
            },

            timeout : 10000
        }).then((response) =>{
            // console.log(response.data)
            setSeñalesFavoritas(response.data)
        }).catch((error) => {
            toast.error(error.response.data.Error)
        }).finally(() => {
          setTimeout(() => {
            setCargando(false)
          }, 1000);
        })
    }


    const recuperarFechasFavoritos = async() => {
        setCargando(true)

        const tokenAcceso = cookies.get("token")

        await axios.get(VITE_API_LINK + "favoritos/fechas", {
            headers: {
                Authorization: `Bearer ${tokenAcceso}`,
                ClientAuth : import.meta.env.VITE_APIKEY
            },

            timeout : 10000
        }).then((response) =>{
            console.log(response)
            setFechasFavoritos(response.data)
        }).catch((error) => {
            console.log(error)
            toast.error(error.response.data.Error)
        }).finally(() => {
          setTimeout(() => {
            setCargando(false)
          }, 1000);
        })
    }


    const filtrarDatosFavoritos = async() => {
        const tokenAcceso = cookies.get("token")

        await axios.post(VITE_API_LINK + "favoritos/filtrar",{
           criterio : opcion,
           dato : dato
        },{
            headers: {
                Authorization: `Bearer ${tokenAcceso}`,
                ClientAuth : import.meta.env.VITE_APIKEY
            },

            timeout : 10000
        }).then((response) =>{
          setSeñalesFavoritas(response.data)
        }).catch((error) => {
            toast.error(error.response.data.Error)
        }).finally(() => {
          setTimeout(() => {
            setCargando(false)
          }, 1000);
        })
    }

    //Estructura busqueda favorito.
    const searchScheme = Yup.object().shape({
        opcion : Yup.string()
                    .required("Debe escoger una opción")
                    .oneOf(["texto","fecha"], "Escoja un parametro de busqueda valido."),

        dato : Yup.string()
                   .required("Puede que no haya seleccionado una fecha, o el campo esta vacio")
    })


    //Limpia la busqueda, o filtro que se ha hecho
    const limpiarBusqueda = () => {
        misFavoritos()
        setDato("")
        setOpcion("")
    }


    return (
      <div className='p-5 mid-grey-handbook rounded shadow'>

        <h3 className='text-center fw-bold'> Señales de referencia</h3>
        <p className='fs-5 text-center mt-4'> Aquí podra visualizar las señales (auscultaciones) que usted considere que pueden ser referencia para futuros análisis.</p>

        <div className='mt-3'>
          <div className='row'>
            <Formik enableReinitialize={true}
                    initialValues={{
                          opcion : opcion,
                          dato : dato
                    }}
                    validationSchema={searchScheme}
                    onSubmit = {async() => filtrarDatosFavoritos()}>

                    <Form>
                    <div className='row'>
                        <label className='fw-bold'>Filtrar referencias por:</label>
                          <div className='col-md-6 col-lg-2  mt-2'>
                              <Field as="select" className='form-select p-3  w-100 shadow-sm dark-red-handbook text-white' id="opcion" name="opcion" value={opcion}
                                     onChange={handleChangeOpcion}>
                                  <option value="">Seleccione..</option>
                                  <option value="texto">Texto</option>
                                  <option value="fecha">Fecha</option>
                              </Field>
                              <ErrorMessage name='opcion' component='div' className='dark-grey-handbook  p-2 text-white mt-2 rounded text-center'/>
                          </div>

                          {opcion == "texto" ? (
                            <div className='col-md-6 col-lg-6  mt-2'>
                              <Field type="text" className='form-control p-3  w-100 shadow-sm' id="dato" name="dato" onKeyUp={handleDatoChange} placeholder = "Digite la información"/>
                              <ErrorMessage name='dato' component='div' className='dark-grey-handbook  p-2 text-white mt-2 rounded text-center'/>
                            </div>
                          ) : (
                            null
                          )}

                          {opcion == "fecha" ? (
                            <div className='col-md-6 col-lg-6  mt-2'>
                              <Field as="select" className='form-select p-3  w-100 shadow-sm' id="dato" name="dato" onChange={handleDatoChange}>
                                <option value="">Seleccione..</option>
                                {fechasFavoritos.map(fecha => (
                                    <option key={fecha.createdAt} value={fecha.createdAt}>
                                      {fecha.createdAt}
                                    </option>
                                ))}
                              </Field>
                              <ErrorMessage name='dato' component='div' className='dark-grey-handbook  p-2 text-white mt-2 rounded text-center'/>
                            </div>
                          ) : (
                            null
                          )}

                          {opcion == "" ? (
                            <div className='col-md-6 col-lg-6  mt-2'>
                              <div className='rounded p-3  w-100 shadow-sm text-center text-white dark-grey-handbook'>
                                Seleccione un filtro para iniciar.
                              </div>
                            </div>
                          ) : (
                            null
                          )}

                          <div className='col-md-6 col-lg-2  mt-2'>
                              <button className='dark-red-handbook w-100 p-3  btn text-white shadow-sm' type='submit'>
                                  <i className='fa fa-search text-white px-1'></i>
                                  Buscar
                              </button>
                          </div>
                          <div className='col-md-6 col-lg-2 mt-2'>
                              <button className='dark-red-handbook w-100 p-3 btn text-white shadow-sm' type='button' onClick={() => limpiarBusqueda()}>
                                  <i className='fa fa-hands text-white px-1'></i>
                                  Limpiar busqueda
                              </button>                          
                          </div>
                    </div>
                </Form>
            </Formik>
          </div>
        </div>

        <br/>
        <hr/>

        {cargando == true ? 
          (<SpinnerHeart width={500} height={500}/>) 
            : 
          (
            señalesFavoritas.length > 0 ? (
              <div className='overflow-auto scrollbar-fonodx mt-4 px-3 shadow-inside-edit'>
                  <div className='row'>
                    {señalesFavoritas.map((senal) => 
                        <Favorito senal={senal.favorites} key={senal.favorites} misFavoritos={misFavoritos}/>
                    )}
                  </div>
              </div>
            ) : (
              <div className='row'>
                      <div className='col-md-12'>
                          <div className='gray-bg p-3 mt-5 fw-bold rounded text-center'>
                              Actualmente no hay señales de referencia.
                          </div>
                      </div>
              </div>
            )
        )}

      </div>
    )
}

export default SeñalesFavoritas