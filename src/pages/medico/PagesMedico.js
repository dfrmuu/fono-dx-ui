export {default as VerPacientes} from './VerPacientes'
export {default as RegistroPaciente} from './RegistroPaciente'
export {default as SeñalesFavoritas} from './SeñalesFavoritas'
export {default as RegistroAuscultacion} from './RegistroAuscultacion'