import React from 'react'
import ReactDOM from 'react-dom/client'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap/dist/js/bootstrap.bundle.min'
import Popper from 'popper.js';
import{
  BrowserRouter,
  Route,
  Routes,
}  from 'react-router-dom'
import Layout from '../src/layout/Layout'
import Dashboard from './pages/dashboard/Dashboard'

import {
  VerPacientes,
  RegistroPaciente,
  RegistroAuscultacion,
  SeñalesFavoritas
} from './pages/medico/PagesMedico'

import {
  Login,
  Recover,
  Register,
} from './pages/auth/PagesAuth'

import {
  Invitaciones,
  TablaPermisos,
  TablaUsuarios,
  TablaErrores
} from './pages/admin/PagesAdmin'
import {
  VerMisAuscultaciones
} from './pages/pacientes/PagesPacientes'
import Index from './pages/Index';


//Rutas del aplicativo web

ReactDOM.createRoot(document.getElementById('root')).render(
  <BrowserRouter>
      <Routes >
          <Route path='/' index element={<Index/>}/>
          
          <Route path='/login' element={<Login/>}/>
          <Route path='/recover' element={<Recover/>}/>
          <Route path='/register' element={<Register/>}/>

          <Route path='/index' element={<Layout/>}>
            <Route index path='/index/dashboard' element={<Dashboard/>}/>
            {/*Rutas paciente*/}
            <Route path='/index/paciente/mis-auscultaciones' element={<VerMisAuscultaciones/>}/>

            {/* Rutas  del medico */}
            <Route path='/index/medico/mis-pacientes' element={<VerPacientes/>}/>
            <Route path='/index/medico/registro-pacientes' element={<RegistroPaciente/>}/>
            <Route path='/index/medico/registro-auscultacion' element={<RegistroAuscultacion/>}/>
            <Route path='/index/medico/mis-favoritas' element={<SeñalesFavoritas/>}/>
            {/* Rutas del administrativo */ }
            <Route path='/index/admin/invitaciones' element={<Invitaciones/>}/>
            <Route path='/index/admin/usuarios' element={<TablaUsuarios/>}/>
            <Route path='/index/admin/permisos' element={<TablaPermisos/>}/>
            <Route path='/index/admin/errores' element={<TablaErrores/>}/>

          </Route>
      </Routes>
  </BrowserRouter>
)
